<?php

/* * ***********************************************
 *
 * Snoopy - the PHP net client
 * Author: Monte Ohrt <monte@ohrt.com>
 * Copyright (c): 1999-2014, all rights reserved
 * Version: 2.0.0
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * You may contact the author of Snoopy by e-mail at:
 * monte@ohrt.com
 *
 * The latest version of Snoopy can be obtained from:
 * http://snoopy.sourceforge.net/
 * *********************************************** */

namespace net\snoopy;

/**
 * Http 页面采集类
 * 完善的浏览器模拟访问
 */
class Snoopy {
    /*     * ** Public variables *** */

    /* user definable vars */

    var $scheme = 'http'; // http or https
    var $scheme_is = true;
    var $host = "www.php.net"; // host name we are connecting to
    var $port = 80; // port we are connecting to
    var $proxy_host = ""; // proxy host to use
    var $proxy_port = ""; // proxy port to use
    var $proxy_user = ""; // proxy user to use
    var $proxy_pass = ""; // proxy password to use
    var $agent = "Snoopy v2.0.0+"; // agent we masquerade as
    var $referer = ""; // referer info to pass
    var $cookies = array(); // array of cookies to pass
    // $cookies["username"]="joe";
    var $rawheaders = array(); // array of raw headers to send
    // $rawheaders["Content-type"]="text/html";
    var $maxredirs = 5; // http redirection depth maximum. 0 = disallow
    var $lastredirectaddr = ""; // contains address of last redirected address
    var $offsiteok = true; // allows redirection off-site
    var $maxframes = 0; // frame content depth maximum. 0 = disallow
    var $expandlinks = true; // expand links to fully qualified URLs.
    // this only applies to fetchlinks()
    // submitlinks(), and submittext()
    var $passcookies = true; // pass set cookies back through redirects
    // NOTE: this currently does not respect
    // dates, domains or paths.
    var $user = ""; // user for http authentication
    var $pass = ""; // password for http authentication
    // http accept types
    var $accept = "image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, */*";
    var $results = ""; // where the content is put
    var $resultss = ""; // where the content is put
    var $error = ""; // error messages sent here
    var $response_code = ""; // response code returned from server
    var $headers = array(); // headers returned from server sent here
    var $maxlength = 500000; // max return data length (body)
    var $read_timeout = 0; // timeout on read operations, in seconds
    // supported only since PHP 4 Beta 4
    // set to 0 to disallow timeouts
    var $timed_out = false; // if a read operation timed out
    var $status = 0; // http request status
    var $temp_dir = "/tmp"; // temporary directory that the webserver
    // has permission to write to.
    // under Windows, this should be C:\temp
    //var $curl_path = false;
    // deprecated, snoopy no longer uses curl for https requests,
    // but instead requires the openssl extension.
    // send Accept-encoding: gzip?
    var $use_gzip = true;
    // file or directory with CA certificates to verify remote host with
    var $cafile;
    var $capath;
    //下面这段为补充说明
    //新加的支持http1.1协议
    var $httpversion = false; //默认是1.0
    //var $curl_path = "/usr/bin/curl";
    var $curl_path = '';
    var $curl_path_win = '\curl\curl.exe';
    var $curl_path_linux = "/usr/bin/curl";
    // Snoopy will use cURL for fetching
    // SSL content if a full system path to
    // the cURL binary is supplied here.
    // set to false if you do not have
    // cURL installed. See http://curl.haxx.se
    // for details on installing cURL.
    // Snoopy does *not* use the cURL
    // library functions built into php,
    // as these functions are not stable
    // as of this Snoopy release.
    var $curl_mode = 1;
    //1默认为php自带扩展curl,2第三方curl
    var $ssl = false;
    //证书密钥路径
    var $ssl_path = '';
    var $ssl_pemcret = ''; //需完整路径
    var $ssl_pemkey = ''; //完整路径
    //只看是否响应，不实际获取数据
    var $is_look=false;


    /*     * ** Private variables *** */
    var $_maxlinelen = 4096; // max line length (headers)
    var $_httpmethod = "GET"; // default http request method
    var $_httpversion = "HTTP/1.0"; // default http request version
    var $_submit_method = "POST"; // default submit method
    var $_submit_type = "application/x-www-form-urlencoded"; // default submit type
    var $_mime_boundary = ""; // MIME boundary for multipart/form-data submit type
    var $_redirectaddr = false; // will be set if page fetched is a redirect
    var $_redirectdepth = 0; // increments on an http redirect
    var $_frameurls = array(); // frame src urls
    var $_framedepth = 0; // increments on frame depth
    var $_isproxy = false; // set if using a proxy server
    var $_fp_timeout = 30; // timeout for socket connection

    function ver() {
        echo $this->agent . "\r\n";
    }

    /* ======================================================================*\
      Function:	fetch
      Purpose:	fetch the contents of a web page
      (and possibly other protocols in the
      future like ftp, nntp, gopher, etc.)
      Input:		$URI	the location of the page to fetch
      Output:		$this->results	the output text from the fetch
      \*====================================================================== */

    function fetch($URI) {

        $URI_PARTS = parse_url($URI);
        if (!empty($URI_PARTS["user"]))
            $this->user = $URI_PARTS["user"];
        if (!empty($URI_PARTS["pass"]))
            $this->pass = $URI_PARTS["pass"];
        if (empty($URI_PARTS["query"]))
            $URI_PARTS["query"] = '';
        if (empty($URI_PARTS["path"]))
            $URI_PARTS["path"] = '';

        $fp = null;
        //强制走https方式
        if ($this->scheme_is == FALSE) {
            $URI_PARTS["scheme"] = 'https';
        }

//        var_dump($URI_PARTS["scheme"]);exit;
        switch (strtolower($URI_PARTS["scheme"])) {
            case "https":
                if (!extension_loaded('openssl')) {
                    trigger_error("openssl extension required for HTTPS", E_USER_ERROR);
                    exit;
                }
//                $this->port = 443;
                if ($this->scheme_is == FALSE) {
                    $this->port = 443;
                } else {
                    $this->port = isset($URI_PARTS["port"]) ? $URI_PARTS["port"] : 80;
                }

//                var_dump($this->curl_mode);
//                exit;
                if ($this->curl_mode == 1) {
                    $this->request_by_curls($URI, $this->_httpmethod);
                    return $this;
                    break;
                } else {
                    switch (strtoupper(PHP_OS)) {
                        case 'CYGWIN':
                            $this->curl_path = $this->curl_path_linux; //LINUX
                            break;
                        case 'WINNT':
                            $this->curl_path = dirname(__FILE__) . $this->curl_path_win; //WINDOWS
                            break;
                        default:
                            $this->curl_path = $this->curl_path_linux; //LINUX
                            break;
                    }

//                    if (strpos(strtoupper(PHP_OS), 'WIN') === false) {
//                        $this->curl_path = $this->curl_path_linux; //LINUX
//                    } else {
//                        
//                    }

                    if (!$this->curl_path) {
                        trigger_error("curl_path is null", E_USER_ERROR);
                        exit;
                    }
                    if (function_exists("is_executable")) {
                        if (!is_executable($this->curl_path)) {
                            trigger_error("curl_path not find", E_USER_ERROR);
                            exit;
                        }
                    }

                    $this->host = $URI_PARTS["host"];

                    if (!empty($URI_PARTS["port"]))
                        $this->port = $URI_PARTS["port"];
                    if ($this->_isproxy) {
                        // using proxy, send entire URI
                        $this->_httpsrequest($URI, $URI, $this->_httpmethod);
                    } else {
                        $path = $URI_PARTS["path"] . ($URI_PARTS["query"] ? "?" . $URI_PARTS["query"] : "");
                        // no proxy, send only the path
                        $this->_httpsrequest($path, $URI, $this->_httpmethod);
                    }


                    if ($this->_redirectaddr) {
                        /* url was redirected, check if we've hit the max depth */
                        if ($this->maxredirs > $this->_redirectdepth) {
                            // only follow redirect if it's on this site, or offsiteok is true
                            if (preg_match("|^http://" . preg_quote($this->host) . "|i", $this->_redirectaddr) || $this->offsiteok) {
                                /* follow the redirect */
                                $this->_redirectdepth++;
                                $this->lastredirectaddr = $this->_redirectaddr;
                                $this->fetch($this->_redirectaddr);
                            }
                        }
                    }

                    if ($this->_framedepth < $this->maxframes && count($this->_frameurls) > 0) {
                        $frameurls = $this->_frameurls;
                        $this->_frameurls = array();

                        while (list(, $frameurl) = each($frameurls)) {
                            if ($this->_framedepth < $this->maxframes) {
                                $this->fetch($frameurl);
                                $this->_framedepth++;
                            } else
                                break;
                        }
                    }

                    return $this;
                    break;
                }

            case "http":
                $this->scheme = strtolower($URI_PARTS["scheme"]);
                $this->host = $URI_PARTS["host"];
                if (!empty($URI_PARTS["port"]))
                    $this->port = $URI_PARTS["port"];
                if ($this->_connect($fp)) {
                    if ($this->_isproxy) {
                        // using proxy, send entire URI
                        $this->_httprequest($URI, $fp, $URI, $this->_httpmethod);
                    } else {
                        $path = $URI_PARTS["path"] . ($URI_PARTS["query"] ? "?" . $URI_PARTS["query"] : "");
                        // no proxy, send only the path
                        $this->_httprequest($path, $fp, $URI, $this->_httpmethod);
                    }

                    $this->_disconnect($fp);

                    if ($this->_redirectaddr) {
                        /* url was redirected, check if we've hit the max depth */
                        if ($this->maxredirs > $this->_redirectdepth) {
                            // only follow redirect if it's on this site, or offsiteok is true
                            if (preg_match("|^https?://" . preg_quote($this->host) . "|i", $this->_redirectaddr) || $this->offsiteok) {
                                /* follow the redirect */
                                $this->_redirectdepth++;
                                $this->lastredirectaddr = $this->_redirectaddr;
                                $this->fetch($this->_redirectaddr);
                            }
                        }
                    }

                    if ($this->_framedepth < $this->maxframes && count($this->_frameurls) > 0) {
                        $frameurls = $this->_frameurls;
                        $this->_frameurls = array();

                        while (list(, $frameurl) = each($frameurls)) {
                            if ($this->_framedepth < $this->maxframes) {
                                $this->fetch($frameurl);
                                $this->_framedepth++;
                            } else
                                break;
                        }
                    }
                } else {
                    return false;
                }
                return $this;
                break;
            default:
                // not a valid protocol
                $this->error = 'Invalid protocol "' . $URI_PARTS["scheme"] . '"\n';
                return false;
                break;
        }
        return $this;
    }

    /* ======================================================================*\
      Function:	submit
      Purpose:	submit an http(s) form
      Input:		$URI	the location to post the data
      $formvars	the formvars to use.
      format: $formvars["var"] = "val";
      $formfiles  an array of files to submit
      format: $formfiles["var"] = "/dir/filename.ext";
      Output:		$this->results	the text output from the post
      \*====================================================================== */

    function submit($URI, $formvars = "", $formfiles = "", $Encode = 1) {
        
        unset($postdata);

        if (is_array($formvars)) {
            $postdata = $this->_prepare_post_body($formvars, $formfiles, $Encode);
        } else {
            $postdata = $formvars;
        }
     
//        var_dump($this->scheme_is);exit;


        $URI_PARTS = parse_url($URI);
        if (!empty($URI_PARTS["user"]))
            $this->user = $URI_PARTS["user"];
        if (!empty($URI_PARTS["pass"]))
            $this->pass = $URI_PARTS["pass"];
        if (empty($URI_PARTS["query"]))
            $URI_PARTS["query"] = '';
        if (empty($URI_PARTS["path"]))
            $URI_PARTS["path"] = '';
        //强制https流程，但实际走http协议

        if ($this->scheme_is == FALSE) {
            $URI_PARTS["scheme"] = 'https';
        }
        switch (strtolower($URI_PARTS["scheme"])) {
            case "https":
                if (!extension_loaded('openssl')) {
                    trigger_error("openssl extension required for HTTPS", E_USER_ERROR);
                    exit;
                }
                if ($this->scheme_is == FALSE) {
                    $this->port = 443;
                } else {
                    $this->port = $URI_PARTS["port"] ? $URI_PARTS["port"] : 80;
                }


                if ($this->curl_mode == 1) {
                    $this->request_by_curls($URI, $this->_submit_method, $formvars);
                    return $this;
                    break;
                } else {
                    //curl方式
                    switch (strtoupper(PHP_OS)) {
                        case 'CYGWIN':
                            $this->curl_path = $this->curl_path_linux; //LINUX
                            break;
                        case 'WINNT':
                            $this->curl_path = dirname(__FILE__) . $this->curl_path_win; //WINDOWS
                            break;
                        default:
                            $this->curl_path = $this->curl_path_linux; //LINUX
                            break;
                    }
//                    if (strpos(strtoupper(PHP_OS), 'WIN') === false) {
//                        $this->curl_path = $this->curl_path_linux; //LINUX
//                    } else {
//                        
//                    }
//                    if (strpos(strtoupper(PHP_OS), 'WIN') === false) {
//                        $this->curl_path = $this->curl_path_linux; //LINUX
//                    } else {
//                        $this->curl_path = dirname(__FILE__) . $this->curl_path_win; //WINDOWS
//                    }
                    if (!$this->curl_path) {
                        trigger_error("curl_path is null", E_USER_ERROR);
                        exit;
                    }
                    if (function_exists("is_executable")) {
                        if (!is_executable($this->curl_path)) {
                            trigger_error("curl_path not find", E_USER_ERROR);
                            exit;
                        }
                    }
                    //针对xml方式进行过滤
                    if (strpos($postdata, '?xml version="1.0"?>') > 0) {
                        $postdata = str_replace("\"", "'", $postdata);
                        $postdata = str_replace("\n", "", $postdata);
                        $postdata = str_replace("\r", "", $postdata);
                    }
                    $this->host = $URI_PARTS["host"];
                    if (!empty($URI_PARTS["port"]))
                        $this->port = $URI_PARTS["port"];
                    if ($this->_isproxy) {
                        // using proxy, send entire URI
                        $this->_httpsrequest($URI, $URI, $this->_submit_method, $this->_submit_type, $postdata);
                    } else {
                        $path = $URI_PARTS["path"] . ($URI_PARTS["query"] ? "?" . $URI_PARTS["query"] : "");
                        // no proxy, send only the path
                        $this->_httpsrequest($path, $URI, $this->_submit_method, $this->_submit_type, $postdata);
                    }

                    if ($this->_redirectaddr) {
                        /* url was redirected, check if we've hit the max depth */
                        if ($this->maxredirs > $this->_redirectdepth) {
                            if (!preg_match("|^" . $URI_PARTS["scheme"] . "://|", $this->_redirectaddr))
                                $this->_redirectaddr = $this->_expandlinks($this->_redirectaddr, $URI_PARTS["scheme"] . "://" . $URI_PARTS["host"]);

                            // only follow redirect if it's on this site, or offsiteok is true
                            if (preg_match("|^http://" . preg_quote($this->host) . "|i", $this->_redirectaddr) || $this->offsiteok) {
                                /* follow the redirect */
                                $this->_redirectdepth++;
                                $this->lastredirectaddr = $this->_redirectaddr;
                                if (strpos($this->_redirectaddr, "?") > 0)
                                    $this->fetch($this->_redirectaddr); // the redirect has changed the request method from post to get
                                else
                                    $this->submit($this->_redirectaddr, $formvars, $formfiles);
                            }
                        }
                    }

                    if ($this->_framedepth < $this->maxframes && count($this->_frameurls) > 0) {
                        $frameurls = $this->_frameurls;
                        $this->_frameurls = array();

                        while (list(, $frameurl) = each($frameurls)) {
                            if ($this->_framedepth < $this->maxframes) {
                                $this->fetch($frameurl);
                                $this->_framedepth++;
                            } else
                                break;
                        }
                    }
                    return $this;
                    break;
                }
            case "http":
                $this->scheme = strtolower($URI_PARTS["scheme"]);
                $this->host = $URI_PARTS["host"];
                if (!empty($URI_PARTS["port"]))
                    $this->port = $URI_PARTS["port"];
                if ($this->_connect($fp)) {
                    if ($this->_isproxy) {
                        // using proxy, send entire URI
                        $this->_httprequest($URI, $fp, $URI, $this->_submit_method, $this->_submit_type, $postdata);
                    } else {
                        $path = $URI_PARTS["path"] . ($URI_PARTS["query"] ? "?" . $URI_PARTS["query"] : "");
                        // no proxy, send only the path
                        $this->_httprequest($path, $fp, $URI, $this->_submit_method, $this->_submit_type, $postdata);
                    }
                    $this->_disconnect($fp);

                    if ($this->_redirectaddr) {
                        /* url was redirected, check if we've hit the max depth */
                        if ($this->maxredirs > $this->_redirectdepth) {
                            if (!preg_match("|^" . $URI_PARTS["scheme"] . "://|", $this->_redirectaddr))
                                $this->_redirectaddr = $this->_expandlinks($this->_redirectaddr, $URI_PARTS["scheme"] . "://" . $URI_PARTS["host"]);

                            // only follow redirect if it's on this site, or offsiteok is true
                            if (preg_match("|^https?://" . preg_quote($this->host) . "|i", $this->_redirectaddr) || $this->offsiteok) {
                                /* follow the redirect */
                                $this->_redirectdepth++;
                                $this->lastredirectaddr = $this->_redirectaddr;
                                if (strpos($this->_redirectaddr, "?") > 0)
                                    $this->fetch($this->_redirectaddr); // the redirect has changed the request method from post to get
                                else
                                    $this->submit($this->_redirectaddr, $formvars, $formfiles);
                            }
                        }
                    }

                    if ($this->_framedepth < $this->maxframes && count($this->_frameurls) > 0) {
                        $frameurls = $this->_frameurls;
                        $this->_frameurls = array();

                        while (list(, $frameurl) = each($frameurls)) {
                            if ($this->_framedepth < $this->maxframes) {
                                $this->fetch($frameurl);
                                $this->_framedepth++;
                            } else
                                break;
                        }
                    }
                } else {
                    return false;
                }
                return $this;
                break;
            default:
                // not a valid protocol
                $this->error = 'Invalid protocol "' . $URI_PARTS["scheme"] . '"\n';
                return false;
                break;
        }
        return $this;
    }

    /* ======================================================================*\
      Function:	fetchlinks
      Purpose:	fetch the links from a web page
      Input:		$URI	where you are fetching from
      Output:		$this->results	an array of the URLs
      \*====================================================================== */

    function fetchlinks($URI) {
        if ($this->fetch($URI) !== false) {
            if ($this->lastredirectaddr)
                $URI = $this->lastredirectaddr;
            if (is_array($this->results)) {
                for ($x = 0; $x < count($this->results); $x++)
                    $this->results[$x] = $this->_striplinks($this->results[$x]);
            } else
                $this->results = $this->_striplinks($this->results);

            if ($this->expandlinks)
                $this->results = $this->_expandlinks($this->results, $URI);
            return $this;
        } else
            return false;
    }

    /* ======================================================================*\
      Function:	fetchform
      Purpose:	fetch the form elements from a web page
      Input:		$URI	where you are fetching from
      Output:		$this->results	the resulting html form
      \*====================================================================== */

    function fetchform($URI) {

        if ($this->fetch($URI) !== false) {

            if (is_array($this->results)) {
                for ($x = 0; $x < count($this->results); $x++)
                    $this->results[$x] = $this->_stripform($this->results[$x]);
            } else
                $this->results = $this->_stripform($this->results);

            return $this;
        } else
            return false;
    }

    /* ======================================================================*\
      Function:	fetchtext
      Purpose:	fetch the text from a web page, stripping the links
      Input:		$URI	where you are fetching from
      Output:		$this->results	the text from the web page
      \*====================================================================== */

    function fetchtext($URI) {
        if ($this->fetch($URI) !== false) {
            if (is_array($this->results)) {
                for ($x = 0; $x < count($this->results); $x++)
                    $this->results[$x] = $this->_striptext($this->results[$x]);
            } else
                $this->results = $this->_striptext($this->results);
            return $this;
        } else
            return false;
    }

    /* ======================================================================*\
      Function:	submitlinks
      Purpose:	grab links from a form submission
      Input:		$URI	where you are submitting from
      Output:		$this->results	an array of the links from the post
      \*====================================================================== */

    function submitlinks($URI, $formvars = "", $formfiles = "") {
        if ($this->submit($URI, $formvars, $formfiles) !== false) {
            if ($this->lastredirectaddr)
                $URI = $this->lastredirectaddr;
            if (is_array($this->results)) {
                for ($x = 0; $x < count($this->results); $x++) {
                    $this->results[$x] = $this->_striplinks($this->results[$x]);
                    if ($this->expandlinks)
                        $this->results[$x] = $this->_expandlinks($this->results[$x], $URI);
                }
            } else {
                $this->results = $this->_striplinks($this->results);
                if ($this->expandlinks)
                    $this->results = $this->_expandlinks($this->results, $URI);
            }
            return $this;
        } else
            return false;
    }

    /* ======================================================================*\
      Function:	submittext
      Purpose:	grab text from a form submission
      Input:		$URI	where you are submitting from
      Output:		$this->results	the text from the web page
      \*====================================================================== */

    function submittext($URI, $formvars = "", $formfiles = "") {
        if ($this->submit($URI, $formvars, $formfiles) !== false) {
            if ($this->lastredirectaddr)
                $URI = $this->lastredirectaddr;
            if (is_array($this->results)) {
                for ($x = 0; $x < count($this->results); $x++) {
                    $this->results[$x] = $this->_striptext($this->results[$x]);
                    if ($this->expandlinks)
                        $this->results[$x] = $this->_expandlinks($this->results[$x], $URI);
                }
            } else {
                $this->results = $this->_striptext($this->results);
                if ($this->expandlinks)
                    $this->results = $this->_expandlinks($this->results, $URI);
            }
            return $this;
        } else
            return false;
    }

    /* ======================================================================*\
      Function:	set_submit_multipart
      Purpose:	Set the form submission content type to
      multipart/form-data
      \*====================================================================== */

    function set_submit_multipart() {
        $this->_submit_type = "multipart/form-data";
        return $this;
    }

    /* ======================================================================*\
      Function:	set_submit_normal
      Purpose:	Set the form submission content type to
      application/x-www-form-urlencoded
      \*====================================================================== */

    function set_submit_normal() {
        $this->_submit_type = "application/x-www-form-urlencoded";
        return $this;
    }

    /* ======================================================================*\
      Function:	set_submit_json
      Purpose:	Set the form submission content type to
      application/json
      \*====================================================================== */

    function set_submit_json() {
        $this->_submit_type = "application/json";
        return $this;
    }

    /* ======================================================================*\
      Private functions
      \*====================================================================== */


    /* ======================================================================*\
      Function:	_striplinks
      Purpose:	strip the hyperlinks from an html document
      Input:		$document	document to strip.
      Output:		$match		an array of the links
      \*====================================================================== */

    function _striplinks($document) {
        preg_match_all("'<\s*a\s.*?href\s*=\s*			# find <a href=
						([\"\'])?					# find single or double quote
						(?(1) (.*?)\\1 | ([^\s\>]+))		# if quote found, match up to next matching
													# quote, otherwise match up to next space
						'isx", $document, $links);


        // catenate the non-empty matches from the conditional subpattern

        while (list($key, $val) = each($links[2])) {
            if (!empty($val))
                $match[] = $val;
        }

        while (list($key, $val) = each($links[3])) {
            if (!empty($val))
                $match[] = $val;
        }

        // return the links
        return $match;
    }

    /* ======================================================================*\
      Function:	_stripform
      Purpose:	strip the form elements from an html document
      Input:		$document	document to strip.
      Output:		$match		an array of the links
      \*====================================================================== */

    function _stripform($document) {
        preg_match_all("'<\/?(FORM|INPUT|SELECT|TEXTAREA|(OPTION))[^<>]*>(?(2)(.*(?=<\/?(option|select)[^<>]*>[\r\n]*)|(?=[\r\n]*))|(?=[\r\n]*))'Usi", $document, $elements);

        // catenate the matches
        $match = implode("\r\n", $elements[0]);

        // return the links
        return $match;
    }

    /* ======================================================================*\
      Function:	_striptext
      Purpose:	strip the text from an html document
      Input:		$document	document to strip.
      Output:		$text		the resulting text
      \*====================================================================== */

    function _striptext($document) {

        // I didn't use preg eval (//e) since that is only available in PHP 4.0.
        // so, list your entities one by one here. I included some of the
        // more common ones.

        $search = array("'<script[^>]*?>.*?</script>'si", // strip out javascript
            "'<[\/\!]*?[^<>]*?>'si", // strip out html tags
            "'([\r\n])[\s]+'", // strip out white space
            "'&(quot|#34|#034|#x22);'i", // replace html entities
            "'&(amp|#38|#038|#x26);'i", // added hexadecimal values
            "'&(lt|#60|#060|#x3c);'i",
            "'&(gt|#62|#062|#x3e);'i",
            "'&(nbsp|#160|#xa0);'i",
            "'&(iexcl|#161);'i",
            "'&(cent|#162);'i",
            "'&(pound|#163);'i",
            "'&(copy|#169);'i",
            "'&(reg|#174);'i",
            "'&(deg|#176);'i",
            "'&(#39|#039|#x27);'",
            "'&(euro|#8364);'i", // europe
            "'&a(uml|UML);'", // german
            "'&o(uml|UML);'",
            "'&u(uml|UML);'",
            "'&A(uml|UML);'",
            "'&O(uml|UML);'",
            "'&U(uml|UML);'",
            "'&szlig;'i",
        );
        $replace = array("",
            "",
            "\\1",
            "\"",
            "&",
            "<",
            ">",
            " ",
            chr(161),
            chr(162),
            chr(163),
            chr(169),
            chr(174),
            chr(176),
            chr(39),
            chr(128),
            "ä",
            "ö",
            "ü",
            "Ä",
            "Ö",
            "Ü",
            "ß",
        );

        $text = preg_replace($search, $replace, $document);

        return $text;
    }

    /* ======================================================================*\
      Function:	_expandlinks
      Purpose:	expand each link into a fully qualified URL
      Input:		$links			the links to qualify
      $URI			the full URI to get the base from
      Output:		$expandedLinks	the expanded links
      \*====================================================================== */

    function _expandlinks($links, $URI) {

        preg_match("/^[^\?]+/", $URI, $match);

        $match = preg_replace("|/[^\/\.]+\.[^\/\.]+$|", "", $match[0]);
        $match = preg_replace("|/$|", "", $match);
        $match_part = parse_url($match);
        $match_root = $match_part["scheme"] . "://" . $match_part["host"];

        $search = array("|^http://" . preg_quote($this->host) . "|i",
            "|^(\/)|i",
            "|^(?!http://)(?!mailto:)|i",
            "|/\./|",
            "|/[^\/]+/\.\./|"
        );

        $replace = array("",
            $match_root . "/",
            $match . "/",
            "/",
            "/"
        );

        $expandedLinks = preg_replace($search, $replace, $links);

        return $expandedLinks;
    }

    /* ======================================================================*\
      Function:	_httprequest
      Purpose:	go get the http(s) data from the server
      Input:		$url		the url to fetch
      $fp			the current open file pointer
      $URI		the full URI
      $body		body contents to send if any (POST)
      Output:
      \*====================================================================== */

    function _httprequest($url, $fp, $URI, $http_method, $content_type = "", $body = "") {
//        var_dump($this->httpversion,$url, $fp, $URI, $http_method, $content_type , $body);exit;
        $cookie_headers = '';
        if ($this->passcookies && $this->_redirectaddr)
            $this->setcookies();

        $URI_PARTS = parse_url($URI);
        if (empty($url))
            $url = "/";
        if ($this->httpversion) {
            $headers = $http_method . " " . $url . " " . "HTTP/1.1" . "\r\n";
//            $headers = $http_method . " " . $url . " " . "HTTP/1.0" . "\r\n";
        } else {
            $headers = $http_method . " " . $url . " " . $this->_httpversion . "\r\n";
        }

        if (!empty($this->host) && !isset($this->rawheaders['Host'])) {
            $headers .= "Host: " . $this->host;
            if (!empty($this->port) && $this->port != '80')
                $headers .= ":" . $this->port;
            $headers .= "\r\n";
        }
        if (!empty($this->agent))
            $headers .= "User-Agent: " . $this->agent . "\r\n";
        if (!empty($this->accept))
            $headers .= "Accept: " . $this->accept . "\r\n";
        if ($this->use_gzip) {
            // make sure PHP was built with --with-zlib
            // and we can handle gzipp'ed data
            if (function_exists('gzinflate')) {
                $headers .= "Accept-encoding: gzip\r\n";
            } else {
                trigger_error(
                        "use_gzip is on, but PHP was built without zlib support." .
                        "  Requesting file(s) without gzip encoding.", E_USER_NOTICE);
            }
        }
        if (!empty($this->referer))
            $headers .= "Referer: " . $this->referer . "\r\n";
        if (!empty($this->cookies)) {
            if (!is_array($this->cookies))
                $this->cookies = (array) $this->cookies;

            reset($this->cookies);
            if (count($this->cookies) > 0) {
                $cookie_headers .= 'Cookie: ';
                foreach ($this->cookies as $cookieKey => $cookieVal) {
                    $cookie_headers .= $cookieKey . "=" . urlencode($cookieVal) . "; ";
                }
                $headers .= substr($cookie_headers, 0, -2) . "\r\n";
            }
        }
        if (!empty($this->rawheaders)) {
            if (!is_array($this->rawheaders))
                $this->rawheaders = (array) $this->rawheaders;
            while (list($headerKey, $headerVal) = each($this->rawheaders))
                $headers .= $headerKey . ": " . $headerVal . "\r\n";
        }
        if (!empty($content_type)) {
            $headers .= "Content-type: $content_type";
            if ($content_type == "multipart/form-data")
                $headers .= "; boundary=" . $this->_mime_boundary;
            $headers .= "\r\n";
        }

        if (!empty($body))
            $headers .= "Content-length: " . strlen($body) . "\r\n";
        if (!empty($this->user) || !empty($this->pass))
            $headers .= "Authorization: Basic " . base64_encode($this->user . ":" . $this->pass) . "\r\n";

        //add proxy auth headers
        if (!empty($this->proxy_user))
            $headers .= 'Proxy-Authorization: ' . 'Basic ' . base64_encode($this->proxy_user . ':' . $this->proxy_pass) . "\r\n";


        if ($this->httpversion) {
            //如果不加下面这一句,请求会阻塞很久
            $headers .= "Connection: Close\r\n\r\n";
//            $headers .= "\r\n";
        } else {
            $headers .= "\r\n";
        }
//        var_dump($headers);
        //
        // set the read timeout if needed
        if ($this->read_timeout > 0){
           socket_set_timeout($fp, $this->read_timeout); 
            stream_set_blocking($fp, true);   //重要，设置为非阻塞模式
//            stream_set_timeout($fp,$this->read_timeout);   //设置超时
        }
            
        $this->timed_out = false;

        fwrite($fp, $headers . $body, strlen($headers . $body));
        

        $this->_redirectaddr = false;
        $this->headers=array();
//        unset($this->headers);

        // content was returned gzip encoded?
        $is_gzipped = false;

        while ($currentHeader = fgets($fp, $this->_maxlinelen)) {
            if ($this->read_timeout > 0 && $this->_check_timeout($fp)) {
                $this->status = -100;
                return false;
            }

            if ($currentHeader == "\r\n")
                break;

            // if a header begins with Location: or URI:, set the redirect
            if (preg_match("/^(Location:|URI:)/i", $currentHeader)) {
                // get URL portion of the redirect
                preg_match("/^(Location:|URI:)[ ]+(.*)/i", chop($currentHeader), $matches);
                // look for :// in the Location header to see if hostname is included
                if (!preg_match("|\:\/\/|", $matches[2])) {
                    // no host in the path, so prepend
                    $this->_redirectaddr = $URI_PARTS["scheme"] . "://" . $this->host . ":" . $this->port;
                    // eliminate double slash
                    if (!preg_match("|^/|", $matches[2]))
                        $this->_redirectaddr .= "/" . $matches[2];
                    else
                        $this->_redirectaddr .= $matches[2];
                } else
                    $this->_redirectaddr = $matches[2];
            }

            if (preg_match("|^HTTP/|", $currentHeader)) {
                if (preg_match("|^HTTP/[^\s]*\s(.*?)\s|", $currentHeader, $status)) {
                    $this->status = $status[1];
                }
                $this->response_code = $currentHeader;
            }

            if (preg_match("/Content-Encoding: gzip/", $currentHeader)) {
                $is_gzipped = true;
            }

            $this->headers[] = $currentHeader;
        }

        $results = '';
        /*
         提示：feof() 函数对遍历长度未知的数据很有用。
　　  注意：如果服务器没有关闭由 fsockopen() 所打开的连接，feof() 会一直等待直到超时而返回 TRUE。默认的超时限制是 60 秒，可以使用 stream_set_timeout() 来改变这个值。
　　  注意：如果传递的文件指针无效可能会陷入无限循环中，因为 EOF 不会返回 TRUE。
         默认设置1秒,因此不推荐使用本方法.尽量使用curl.
         * */  
        //需要真正计时
        $start_time= time();
        while (!feof($fp)) {
            stream_set_timeout($fp, 1);
            $_data = fread($fp, $this->maxlength);
            $info = stream_get_meta_data($fp);
            if ($info['timed_out']) {
                break;
            }
            $results .= $_data;
        }
        $end_time= time();
//        var_dump($results);exit;

//        //此处会死循环
//        do {
//            $_data = fread($fp, $this->maxlength);
//
//            if (strlen($_data) == 0) {
//                break;
//            }
//            $results .= $_data;
//        } while (true);
       
        // gunzip
        if ($is_gzipped) {
            // per http://www.php.net/manual/en/function.gzencode.php
            $results = substr($results, 10);
            $results = gzinflate($results);
        }
        socket_set_timeout($fp, $this->read_timeout); 
        if ($this->read_timeout > 0 && ($end_time-$start_time)>$this->read_timeout) {
            $this->status = -100;
            return false;
        }
        
//        if ($this->read_timeout > 0 && $this->_check_timeout($fp)) {
//            $this->status = -100;
//            return false;
//        }
//        var_dump(111);exit;
        // check if there is a a redirect meta tag

        if (preg_match("'<meta[\s]*http-equiv[^>]*?content[\s]*=[\s]*[\"\']?\d+;[\s]*URL[\s]*=[\s]*([^\"\']*?)[\"\']?>'i", $results, $match)) {
            $this->_redirectaddr = $this->_expandlinks($match[1], $URI);
        }
        //强制重置返回值为空
//      unset($this->results);
        $this->results = '';
        // have we hit our frame depth and is there frame src to fetch?
        if (($this->_framedepth < $this->maxframes) && preg_match_all("'<frame\s+.*src[\s]*=[\'\"]?([^\'\"\>]+)'i", $results, $match)) {
            $this->results[] = $results;
            for ($x = 0; $x < count($match[1]); $x++)
                $this->_frameurls[] = $this->_expandlinks($match[1][$x], $URI_PARTS["scheme"] . "://" . $this->host);
        } // have we already fetched framed content?
        elseif (is_array($this->results))
            $this->results[] = $results;
        // no framed content
        else
            $this->results = $results;

//        var_dump($results."\r\n");
        //返回格式为json对象,实际为数组方式
        if (!empty($content_type)) {
            if ($content_type == "application/json") {
                if (!is_array($this->results)) {
                    $jsondata = json_decode($this->results, true);
                    if (!is_array($jsondata)) {
                        $jsondata = json_decode($jsondata, true);
                        if (is_array($jsondata)) {
                            $this->results = $jsondata;
                        }
                    } else {
                        $this->results = $jsondata;
                    }
                }
            }
        }
        return $this;
    }

    /* ======================================================================*\
      Function:	_httpsrequest
      Purpose:	go get the https data from the server using curl
      Input:		$url		the url to fetch
      $URI		the full URI
      $body		body contents to send if any (POST)
      Output:
      \*====================================================================== */

    function _httpsrequest($url, $URI, $http_method, $content_type = "", $body = "") {
//        var_dump($url, $URI, $http_method, $content_type, $body);exit;
        $cookie_headers = '';
        if ($this->passcookies && $this->_redirectaddr) {
            $this->setcookies();
        }
        $headers = array();
        $URI_PARTS = parse_url($URI);
        if (empty($url))
            $url = "/";
        // GET ... header not needed for curl
        //$headers[] = $http_method." ".$url." ".$this->_httpversion;
        if (!empty($this->agent))
            $headers[] = "User-Agent: " . $this->agent;
        if (!empty($this->host))
            if (!empty($this->port))
                $headers[] = "Host: " . $this->host . ":" . $this->port;
            else
                $headers[] = "Host: " . $this->host;
        if (!empty($this->accept))
            $headers[] = "Accept: " . $this->accept;
        if (!empty($this->referer))
            $headers[] = "Referer: " . $this->referer;
        if (!empty($this->cookies)) {
            if (!is_array($this->cookies))
                $this->cookies = (array) $this->cookies;
            reset($this->cookies);
            if (count($this->cookies) > 0) {
                $cookie_headers = 'Cookie: ';
                foreach ($this->cookies as $cookieKey => $cookieVal) {
                    $cookie_headers .= $cookieKey . "=" . urlencode($cookieVal) . "; ";
                }
                $headers[] = substr($cookie_headers, 0, -2);
            }
        }
        if (!empty($this->rawheaders)) {
            if (!is_array($this->rawheaders))
                $this->rawheaders = (array) $this->rawheaders;
            while (list($headerKey, $headerVal) = each($this->rawheaders))
                $headers[] = $headerKey . ": " . $headerVal;
        }
        if (!empty($content_type)) {
            if ($content_type == "multipart/form-data")
                $headers[] = "Content-type: $content_type; boundary=" . $this->_mime_boundary;
            else
                $headers[] = "Content-type: $content_type";
        }
        //var_dump($body);exit;
        if (!empty($body))
            $headers[] = "Content-length: " . strlen($body);
        if (!empty($this->user) || !empty($this->pass)) {
            $headers[] = "Authorization: Basic " . base64_encode($this->user . ":" . $this->pass);
        }
//        var_dump($headers);exit;
        $cmdline_params = '';
        for ($curr_header = 0; $curr_header < count($headers); $curr_header++) {
            $safer_header = strtr($headers[$curr_header], "\"", " ");
            $cmdline_params .= " -H \"" . $safer_header . "\"";
        }
//          var_dump($cmdline_params);exit;

        if (!empty($body))
            $cmdline_params .= " -d \"$body\"";
        if ($this->read_timeout > 0) {
            $cmdline_params .= " -m " . $this->read_timeout;
        }
//        $headerfile = tempnam($this->temp_dir, "sno");
        $headerfile = tempnam(sys_get_temp_dir(), "sno");


        if ($this->ssl) {
            if (!empty($this->ssl_pemkey)) {
                if (!file_exists($this->ssl_pemkey)) {
                    $this->error = '密钥不存在';
                    return $this;
                }
                $cmdline_params .= " --key-type \"" . 'PEM' . "\"";
                $cmdline_params .= " --key \"" . $this->ssl_pemkey . "\"";
            }

            if (!empty($this->ssl_pemcret)) {
                if (!file_exists($this->ssl_pemcret)) {
                    $this->error = '证书不存在';
                    return $this;
                }
                $cmdline_params .= " --cert-type \"" . 'PEM' . "\"";
                $cmdline_params .= " -E \"" . $this->ssl_pemcret . "\"";
            }
        }
        //-D 保存 head头到指定文件
        // -I 直接返回head头
        // -H 设置提交的head头
        // -k/--insecure 允许不使用证书到SSL站点
        if($this->is_look===TRUE){
            $tempstr = $this->curl_path . " -I -k -D \"$headerfile\"" . $cmdline_params . " \"" . $URI . "\" ";
        }else{
            $tempstr = $this->curl_path . " -k -D \"$headerfile\"" . $cmdline_params . " \"" . $URI . "\" ";
        }
//        $tempstr = $this->curl_path . " -k -D \"$headerfile\"" . $cmdline_params . " \"" . $URI . "\" ";
//        var_dump($tempstr);
//        exit;
        exec($tempstr, $results, $return);
//        var_dump($results);exit;
        if ($return) {
            $this->error = "Error: cURL could not retrieve the document, error $return.";
            return false;
        }
//        $results = implode("\r\n", $results);
        $results = implode("\r\n", $results);
        $result_headers = file("$headerfile");
        $this->_redirectaddr = false;
        $this->headers=array();
//        unset($this->headers);
        for ($currentHeader = 0; $currentHeader < count($result_headers); $currentHeader++) {
            // if a header begins with Location: or URI:, set the redirect
            if (preg_match("/^(Location: |URI: )/i", $result_headers[$currentHeader])) {
                // get URL portion of the redirect
                //preg_match("/^(Location: |URI:)\s+(.*)/", chop($result_headers[$currentHeader]), $matches);
                preg_match("/^(Location: |URI:)[ ]+(.*)/i", chop($result_headers[$currentHeader]), $matches);
                // look for :// in the Location header to see if hostname is included
                if (!preg_match("|\:\/\/|", $matches[2])) {
                    // no host in the path, so prepend
                    $this->_redirectaddr = $URI_PARTS["scheme"] . "://" . $this->host . ":" . $this->port;
                    // eliminate double slash
                    if (!preg_match("|^/|", $matches[2]))
                        $this->_redirectaddr .= "/" . $matches[2];
                    else
                        $this->_redirectaddr .= $matches[2];
                } else
                    $this->_redirectaddr = $matches[2];
            }
            if (preg_match("|^HTTP/|", $result_headers[$currentHeader])) {
                $this->response_code = $result_headers[$currentHeader];
                if (preg_match("|^HTTP/[^\s]*\s(.*?)\s|", $this->response_code, $match)) {
                    $this->status = $match[1];
                }
            }
            $this->headers[] = $result_headers[$currentHeader];
        }
        // check if there is a a redirect meta tag
        if (preg_match("'<meta[\s]*http-equiv[^>]*?content[\s]*=[\s]*[\"\']?\d+;[\s]*URL[\s]*=[\s]*([^\"\']*?)[\"\']?>'i", $results, $match)) {
            $this->_redirectaddr = $this->_expandlinks($match[1], $URI);
        }

        //强制重置返回值为空
        unset($this->results);
        $this->results='';
        // have we hit our frame depth and is there frame src to fetch?
        if (($this->_framedepth < $this->maxframes) && preg_match_all("'<frame\s+.*src[\s]*=[\'\"]?([^\'\"\>]+)'i", $results, $match)) {
            $this->results[] = $results;
            for ($x = 0; $x < count($match[1]); $x++)
                $this->_frameurls[] = $this->_expandlinks($match[1][$x], $URI_PARTS["scheme"] . "://" . $this->host);
        } // have we already fetched framed content?
        elseif (is_array($this->results))
            $this->results[] = $results;
        // no framed content
        else
            $this->results = $results;
        unlink("$headerfile");
        
//        var_dump($this->status);exit;


        if (empty($content_type)) {
            $this->_submit_type;
        }
        //返回格式为json对象,实际为数组方式
        if (!empty($this->_submit_type)) {
            if ($this->_submit_type == "application/json") {
                if (!is_array($this->results)) {
                    $jsondata = json_decode($this->results, true);
                    if (!is_array($jsondata)) {
                        $jsondata = json_decode($jsondata, true);
                        if (is_array($jsondata)) {
                            $this->results = $jsondata;
                        }
                    } else {
                        $this->results = $jsondata;
                    }
                }
            }
        }

        return $this;
    }

    /* ======================================================================*\
      Function:	setcookies()
      Purpose:	set cookies for a redirection
      \*====================================================================== */

    function setcookies() {
        for ($x = 0; $x < count($this->headers); $x++) {
            if (preg_match('/^set-cookie:[\s]+([^=]+)=([^;]+)/i', $this->headers[$x], $match))
                $this->cookies[$match[1]] = urldecode($match[2]);
        }
        return $this;
    }

    /* ======================================================================*\
      Function:	_check_timeout
      Purpose:	checks whether timeout has occurred
      Input:		$fp	file pointer
      \*====================================================================== */

    function _check_timeout($fp) {
        if ($this->read_timeout > 0) {
            $fp_status = socket_get_status($fp);
            if ($fp_status["timed_out"]) {
                $this->timed_out = true;
                return true;
            }
        }
        return false;
    }

    /* ======================================================================*\
      Function:	_connect
      Purpose:	make a socket connection
      Input:		$fp	file pointer
      \*====================================================================== */

    function _connect(&$fp) {
        if (!empty($this->proxy_host) && !empty($this->proxy_port)) {
            $this->_isproxy = true;

            $host = $this->proxy_host;
            $port = $this->proxy_port;

            if ($this->scheme == 'https') {
                trigger_error("HTTPS connections over proxy are currently not supported", E_USER_ERROR);
                exit;
            }
        } else {
            $host = $this->host;
            $port = $this->port;
        }

        $this->status = 0;

        $context_opts = array();

        if ($this->scheme == 'https') {
            // if cafile or capath is specified, enable certificate
            // verification (including name checks)
            if (isset($this->cafile) || isset($this->capath)) {
                $context_opts['ssl'] = array(
                    'verify_peer' => true,
                    'CN_match' => $this->host,
                    'disable_compression' => true,
                );

                if (isset($this->cafile))
                    $context_opts['ssl']['cafile'] = $this->cafile;
                if (isset($this->capath))
                    $context_opts['ssl']['capath'] = $this->capath;
            }

            $host = 'ssl://' . $host;
        }

        $context = stream_context_create($context_opts);

        if (version_compare(PHP_VERSION, '5.0.0', '>')) {
            if ($this->scheme == 'http') {
                $host = "tcp://" . $host;
            }
            $fp = stream_socket_client(
                    "$host:$port", $errno, $errmsg, $this->_fp_timeout, STREAM_CLIENT_CONNECT, $context);
        } else {
            $fp = fsockopen(
                    $host, $port, $errno, $errstr, $this->_fp_timeout, $context);
        }


        if ($fp) {
            // socket connection succeeded
            return true;
        } else {
            // socket connection failed
            $this->status = $errno;
            switch ($errno) {
                case -3:
                    $this->error = "socket creation failed (-3)";
                case -4:
                    $this->error = "dns lookup failure (-4)";
                case -5:
                    $this->error = "connection refused or timed out (-5)";
                default:
                    $this->error = "connection failed (" . $errno . ")";
            }
            return false;
        }
    }

    /* ======================================================================*\
      Function:	_disconnect
      Purpose:	disconnect a socket connection
      Input:		$fp	file pointer
      \*====================================================================== */

    function _disconnect($fp) {
        return (fclose($fp));
    }

    /* ======================================================================*\
      Function:	_prepare_post_body
      Purpose:	Prepare post body according to encoding type
      Input:		$formvars  - form variables
      $formfiles - form upload files
      Output:		post body
      \*====================================================================== */

    function _prepare_post_body($formvars, $formfiles, $Encode) {
        settype($formvars, "array");
        settype($formfiles, "array");
        $postdata = '';

        if (count($formvars) == 0 && count($formfiles) == 0)
            return;
        switch ($this->_submit_type) {
            case "application/json":
                //重要的地方，直接编码为array->json，字符串形式
                $postdata = json_encode($formvars);
                break;
            case "application/x-www-form-urlencoded":
                reset($formvars);
                while (list($key, $val) = each($formvars)) {
                    if (is_array($val) || is_object($val)) {
                        while (list($cur_key, $cur_val) = each($val)) {
                            if ($Encode == 1) {
                                $postdata .= urlencode($key) . "[]=" . urlencode($cur_val) . "&";
                            } else {
                                $postdata .= $key . "[]=" . $cur_val . "&";
                            }
                        }
                    } else {
                        if ($Encode == 1) {
                            $postdata .= urlencode($key) . "=" . urlencode($val) . "&";
                        } else {
                            $postdata .= $key . "=" . $val . "&";
                        }
                    }
                }
                break;

            case "multipart/form-data":
                $this->_mime_boundary = "Snoopy" . md5(uniqid(microtime()));

                reset($formvars);
                while (list($key, $val) = each($formvars)) {
                    if (is_array($val) || is_object($val)) {
                        while (list($cur_key, $cur_val) = each($val)) {
                            $postdata .= "--" . $this->_mime_boundary . "\r\n";
                            $postdata .= "Content-Disposition: form-data; name=\"$key\[\]\"\r\n\r\n";
                            $postdata .= "$cur_val\r\n";
                        }
                    } else {
                        $postdata .= "--" . $this->_mime_boundary . "\r\n";
                        $postdata .= "Content-Disposition: form-data; name=\"$key\"\r\n\r\n";
                        $postdata .= "$val\r\n";
                    }
                }

                reset($formfiles);
                while (list($field_name, $file_names) = each($formfiles)) {
                    settype($file_names, "array");
                    while (list(, $file_name) = each($file_names)) {
                        if (!is_readable($file_name))
                            continue;

                        $fp = fopen($file_name, "r");
                        $file_content = fread($fp, filesize($file_name));
                        fclose($fp);
                        $base_name = basename($file_name);

                        $postdata .= "--" . $this->_mime_boundary . "\r\n";
                        $postdata .= "Content-Disposition: form-data; name=\"$field_name\"; filename=\"$base_name\"\r\n\r\n";
                        $postdata .= "$file_content\r\n";
                    }
                }
                $postdata .= "--" . $this->_mime_boundary . "--\r\n";
                break;
        }

        return $postdata;
    }

    /* ======================================================================*\
      Function:	getResults
      Purpose:	return the results of a request
      Output:		string results
      \*====================================================================== */

    function getResults() {
        return $this->results;
    }

    /* ======================================================================*\
      下面为补充的函数
      \*====================================================================== */
    /*
     * Curl版本
     * 使用方法：
     * $post_string = "app=request&version=beta";
     * request_by_curl('http://facebook.cn/restServer.php',$post_string);
     */

    public function request_by_curl($remote_server, $post_string) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $remote_server);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

//        if($this->cookies) {
//curl_setopt($ch, CURLOPT_COOKIE, $this->cookies);
//}
        if (!empty($this->agent)) {
            curl_setopt($ch, CURLOPT_USERAGENT, $this->agent);
        } else {
            curl_setopt($ch, CURLOPT_USERAGENT, "curl_php");
        }
        $this->results = curl_exec($ch);
        curl_close($ch);
        return $this;
    }

    /**
     * 内置扩展curl https访问
     *
     * @param String
     * @return String
     */
    private function request_by_curls($url, $http_method, $body = "") {
        //var_dump($url, $http_method, $body);exit;
        $opts = array(
            CURLOPT_USERAGENT => $this->agent,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_RETURNTRANSFER => 1,
            //不验证证书下同
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false);

        /* 根据请求类型设置特定参数 */
        switch (strtoupper($http_method)) {
            case 'GET':
                $opts[CURLOPT_URL] = $url;
                break;
            case 'POST':
                $opts[CURLOPT_URL] = $url;
                $opts[CURLOPT_POST] = 1;
                $opts[CURLOPT_POSTFIELDS] = $body;
                break;
        }

        if ($this->ssl) {
            if (!file_exists($this->ssl_pemcret)) {
                $this->error = '证书不存在';
                return $this;
            }
            $opts[CURLOPT_SSLCERTTYPE] = 'PEM';
            $opts[CURLOPT_SSLCERT] = $this->ssl_pemcret;


            if (!file_exists($this->ssl_pemkey)) {
                $this->error = '密钥不存在';
                return $this;
            }
            $opts[CURLOPT_SSLKEYTYPE] = 'PEM';
            $opts[CURLOPT_SSLKEY] = $this->ssl_pemkey;
        }


        /* 初始化并执行curl请求 */
        $ch = curl_init();
        //批量初始参数
        curl_setopt_array($ch, $opts);

        $this->results = curl_exec($ch);
        $err = curl_errno($ch);
        $errmsg = curl_error($ch);
        curl_close($ch);

        if ($err > 0) {
            $this->error = $errmsg;
        }


        if (!empty($this->_submit_type)) {
            if ($this->_submit_type == "application/json") {
                if (!is_array($this->results)) {
                    $jsondata = json_decode($this->results, true);
                    if (!is_array($jsondata)) {
                        $jsondata = json_decode($jsondata, true);
                        if (is_array($jsondata)) {
                            $this->results = $jsondata;
                        }
                    } else {
                        $this->results = $jsondata;
                    }
                }
            }
        }

        return $this;
    }

}

?>
