<?php

namespace fsys;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Dir
 *
 * @author Administrator
 */
class DirBase {

    private $separate = '';

    public function __construct() {
        if (IS_WIN) {
            $this->separate = '\\';
        } else {
            $this->separate = "/";
        }
    }

    function test() {
        echo 'ok';
    }

    /**
     * 扫描目录下所有信息
     * @access public
     * @param string $path 目录路径
     * @return array
     */
    function searchDir($path, &$data) {
        if (is_dir($path)) {

            $dp = dir($path);
            while ($file = $dp->read()) {
                if ($file != '.' && $file != '..') {
                    $this->searchDir($path . $this->separate . $file, $data);
                }
            }
            $dp->close();
        }
        if (is_file($path)) {
            $data[] = $path;
        }
    }

    /**
     * 方法:searchDir的别名
     * @access public
     * @param string $dir 目录路径
     * @return array
     */
    function getDir($dir) {
        $data = array();
        $this->searchDir($dir, $data);
        return $data;
    }

    function listdir($dir) {
        if ($handle = opendir($dir)) {
            $output = array();
            while (false !== ($item = readdir($handle))) {
                if (is_dir($dir . $this->separate . $item) and $item != "." and $item != "..") {
                    $output[] = $dir . $this->separate . $item;
                    $output = array_merge($output, self::ListDescendantDirectories($dir . $this->separate . $item));
                }
            }
            closedir($handle);
            return $output;
        } else {
            return false;
        }
    }

    function ListDescendantDirectories($dir) {
        if ($handle = opendir($dir)) {
            $output = array();
            while (false !== ($item = readdir($handle))) {
                if (is_dir($dir . '/' . $item) and $item != "." and $item != "..") {
                    $output[] = $dir . '/' . $item;
                    $output = array_merge($output, self::ListDescendantDirectories($dir . '/' . $item));
                }
            }
            closedir($handle);
            return $output;
        } else {
            return false;
        }
    }

    function myscandir($dir, &$files) {
//        var_dump($dir);exit;
        foreach (scandir($dir) as $v) { //scandir() 遍历指定目录，只会遍历一级目录
            if (is_dir($dir . $this->separate . $v)) {
                if ($v == '.' || $v == '..') continue;
               $this-> myscandir($dir . $this->separate . $v,$files);
            }
//            echo $dir . $this->separate . $v . "\r\n";

            if (is_file($dir . $this->separate . $v)) {
                $files[] = $dir . $this->separate . $v;
            }
//                $files[] = $v;
        }
    }

    /**
     * 获取路径下所有文件
     * @access public
     * @param string $path 目录路径
     * @return array
     */
    function get_allfiles($path, &$files) {
        if (is_dir($path)) {
            $dp = dir($path);
            while ($file = $dp->read()) {
                if ($file != "." && $file != "..") {
                    $this->get_allfiles($path . $this->separate . $file, $files);
                }
            }
            $dp->close();
        }
        if (is_file($path)) {
            $files[] = $path;
        }
    }

    /**
     * 方法:get_allfiles的别名
     * @access public
     * @param string $dir 目录路径
     * @return array
     */
    function get_filenamesbydir($dir) {
        $files = array();
        $this->get_allfiles($dir, $files);
        return $files;
    }

    function my_dir($dir) {
        $files = array();
        if (@$handle = opendir($dir)) { //注意这里要加一个@，不然会有warning错误提示：）
            while (($file = readdir($handle)) !== false) {
                if ($file != ".." && $file != ".") { //排除根目录；
                    if (is_dir($dir . $this->separate . $file)) { //如果是子文件夹，就进行递归
                        $files[$file] = self::my_dir($dir . $this->separate . $file);
                    } else { //不然就将文件的名字存入数组；
                        $files[] = $file;
                    }
                }
            }
            closedir($handle);
            return $files;
        }
    }

    function scan_dir($dir) {
        $files = array();
        if (@$handle = opendir($dir)) { //注意这里要加一个@，不然会有warning错误提示：）
            while (($file = readdir($handle)) !== false) {
                if ($file != ".." && $file != ".") { //排除根目录；
                    if (is_dir($dir . $this->separate . $file)) { //如果是子文件夹，就进行递归
//                        $files[$file] = self:: scan_dir($dir . $this->separate . $file);
//                        $files[] = self:: scan_dir($dir . $this->separate . $file);
                        $path = self:: scan_dir($dir . $this->separate . $file);
                        foreach ($path as $value) {
//                            var_dump($value);
                            $files[] = $dir . $this->separate . $value;
                        }
                    } else { //不然就将文件的名字存入数组；
                        $files[] = $dir . $this->separate . $file;
                    }
                }
            }
            closedir($handle);
            return $files;
        }
    }

}
