# 链接库核心文件目录 #

----------
## 目录列表 ##
- cache 缓存驱动文件
- config 配置文件驱动文件
- console console行模块
- controller 控制器驱动文件
- db DB模块
- debug 调试模块
- exception 异常处理模块
- log 日志驱动
- model Model模块和DB模块相关
- paginator pr处理相关
- process 进度处理
- response 响应处理模块,各种处理响应方式
- session session处理模块
- template template模块,标签什么的
- view 视图模块

----------

- App.php 应用管理,首要执行文件
- Build.php 初始生成文件
- Cache.php 缓存控制文件
- Collection.php
- Config.php 配置文件读取
- Console.php 命令行控制
- Controller.php 控制器
- Cookie.php 
- Db.php
- Debug.php
- Env.php 系统配置变量
- Error.php 错误处理
- Exception.php 一次处理
- File.php 文件处理
- Hook.php 动作核心处理文件
- Lang.php 语言控制
- Loader.php 加载第三方类库控制
- Log.php 日志控制
- Model.php 
- Paginator.php 分页处理
- Process.php 进程管理,配合Console模块
- Request.php 请求控制
- Response.php 返回控制
- Route.php 路由
- Session.php
- Template.php
- Url.php 路由构造
- Validate.php 验证
- View.php 视图

## 模块说明 ##
系统底层框架为tp5.0.x按功能分为以下几个部分
按数据接收处理顺序分

- 系统支撑
	- 系统配置
	- 日志
	- 错误和调试
	- 基本架构
	- 扩展

- 系统功能
	- 请求
	 - 安全
	 - 验证
	- 路由
	- 控制器
	 - 视图
	 - 模板
	- 模型
 	 - 数据库
 	- 杂项
 	- 命令行
	
