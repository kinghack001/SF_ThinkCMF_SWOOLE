# 底层框架 #

----------
## 文件列表 ##
- .gitignore git配置文件
- .htaccess 伪静态配置文件
- .travis.yml 自动构建文件
- codecov.yml 单测覆盖率统计配置文件
- composer.json 版本依赖说明
- phpunit.xml 单元测试配置
- base.php 基础引导文件
- console.php shell操作基础文件
- convention.php 惯例配置文件
- helper.php 助手文件
- start.php 默认启动引导文件