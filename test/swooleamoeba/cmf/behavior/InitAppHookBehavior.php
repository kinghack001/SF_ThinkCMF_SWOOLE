<?php
// +---------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +---------------------------------------------------------------------
// | Copyright (c) 2013-2018 http://www.thinkcmf.com All rights reserved.
// +---------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +---------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +---------------------------------------------------------------------
namespace cmf\behavior;

use think\db\Query;
use think\Hook;
use think\Db;

class InitAppHookBehavior
{

    // 行为扩展的执行入口必须是run
    public function run(&$param)
    {
        if (!cmf_is_installed()) {
            return;
        }

        $app        = request()->module();
        $service = APP_NAME;
//        var_dump($service,$app);exit;
        $appHookPluginsCacheKey = "init_hook_plugins_app_{$service}_{$app}_hook_plugins";
//        $appHookPlugins         = cache($appHookPluginsCacheKey);
        $appHookPlugins='';
//        var_dump(empty($appHookPlugins), is_null($appHookPlugins),$appHookPlugins->toarray());exit;
        
        if (empty($appHookPlugins) || empty($appHookPlugins->toarray())) {
            // ->fetchSql(true)
            $appHooks = Db::name('hook')->where('service', $service)->where('app', $app)->whereOr(function (Query $query) {
                $query->where('app','eq', '')->where('service', 'eq', '');
            })->column('hook');
//            var_dump($appHooks);exit;

            $appHookPlugins = Db::name('hook_plugin')->field('hook,plugin')->where('status', 1)
                ->where('hook', 'in', $appHooks)
                ->order('list_order ASC')
                ->select();
//        var_dump(111,$appHookPlugins);exit;
            if (!empty($appHookPlugins)) {
                $appHookPlugins=$appHookPlugins->toArray();
            }else{
                $appHookPlugins='';
            }
            cache($appHookPluginsCacheKey, $appHookPlugins, null, 'init_hook_plugins');
        }
//        var_dump(111,$appHookPlugins);exit;
// exit;
        if (!empty($appHookPlugins)) {
            foreach ($appHookPlugins as $hookPlugin) {
//                var_dump($hookPlugin);exit;
//                       var_dump(cmf_get_plugin_class($hookPlugin['plugin']));exit;
                Hook::add($hookPlugin['hook'], cmf_get_plugin_class($hookPlugin['plugin']));
            }
        }
    }
}