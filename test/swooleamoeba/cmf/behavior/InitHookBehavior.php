<?php
// +---------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +---------------------------------------------------------------------
// | Copyright (c) 2013-2018 http://www.thinkcmf.com All rights reserved.
// +---------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +---------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +---------------------------------------------------------------------
namespace cmf\behavior;

use think\db\Query;
use think\Hook;
use think\Db;

// 系统级别的钩子
class InitHookBehavior
{

    // 行为扩展的执行入口必须是run
    public function run(&$param)
    {
        if (!cmf_is_installed()) {
            return;
        }

        $systemHookPlugins = cache('init_hook_plugins_system_hook_plugins');
//      $systemHookPlugins=[];
        if (empty($systemHookPlugins)) {
            // 系统钩子和前台模板钩子
            $systemHooks = Db::name('hook')->where('type', 1)->whereOr(function (Query $query) {
                $query->where('type', 3)->where('app', ['eq', ''], ['eq', 'cmf'], 'or');
            })->column('hook');
      		
// 查询启用的插件
            $systemHookPlugins = Db::name('hook_plugin')->field('hook,plugin')->where('status', 1)
                ->where('hook', 'in', $systemHooks)
                ->order('list_order ASC')
                ->select();
//            var_dump($systemHookPlugins);exit;
            if (!empty($systemHookPlugins)) {
                $systemHookPlugins=$systemHookPlugins->toArray();
            }else{
                $systemHookPlugins='';
            }
            
            cache('init_hook_plugins_system_hook_plugins', $systemHookPlugins, null, 'init_hook_plugins');
        }
//SystemInfo
        // 系统注册标签
        if (!empty($systemHookPlugins)) {
            foreach ($systemHookPlugins as $hookPlugin) {
//                var_dump($hookPlugin);exit;
                Hook::add($hookPlugin['hook'], cmf_get_plugin_class($hookPlugin['plugin']));
            }
        }

    }
}