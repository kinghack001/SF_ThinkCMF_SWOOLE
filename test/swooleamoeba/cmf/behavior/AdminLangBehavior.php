<?php

// +---------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +---------------------------------------------------------------------
// | Copyright (c) 2013-2018 http://www.thinkcmf.com All rights reserved.
// +---------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +---------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +---------------------------------------------------------------------

namespace cmf\behavior;

use think\Lang;
use think\Request;

class AdminLangBehavior {

    // 行为扩展的执行入口必须是run
    public function run() {
        $request = Request::instance();
        $langSet = $request->langset();

        // 要解决2种状态加载,注意语言包这种和view相关的,应该放在对应的应用层目录,而不放公共层和app层.
        // 但如果多个APP都会用到的.需要放到common层.可重复使用
        // 平台级内容,产品级内容,对应不同的目录
        // 加载应用后台菜单语言包,此处已经破坏了原来的默认语言载入逻辑.需要注意.
        $commons = cmf_scan_dir(APP_COMMON . '*', GLOB_ONLYDIR);

        foreach ($commons as $common) {
            Lang::load([
                APP_COMMON . $common . DS . 'lang' . DS . $langSet . DS . 'admin_menu' . EXT,
                APP_COMMON . $common . DS . 'lang' . DS . $langSet . DS . 'admin' . EXT,
                APP_COMMON . $common . DS . 'lang' . DS . $langSet . EXT,
            ]);
        }

        // 产品级后台语言包,放在应用目录中
        $apps = cmf_scan_dir(APP_PATH . '*', GLOB_ONLYDIR);
        foreach ($apps as $app) {
            Lang::load([
                APP_PATH . $app . DS . 'lang' . DS . $langSet . DS . 'admin_menu' . EXT,
                APP_PATH . $app . DS . 'lang' . DS . $langSet . DS . 'admin' . EXT,
                APP_PATH . $common . DS . 'lang' . DS . $langSet . EXT,
            ]);
        }

        // 加后台菜单动态语言包
        $defaultLangDir = config('DEFAULT_LANG');
        Lang::load([
            CMF_ROOT . "data/lang/" . $defaultLangDir . "/admin_menu.php"
        ]);
    }

}
