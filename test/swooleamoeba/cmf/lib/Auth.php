<?php
// +---------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +---------------------------------------------------------------------
// | Copyright (c) 2013-2014 http://www.thinkcmf.com All rights reserved.
// +---------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +---------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +---------------------------------------------------------------------
namespace cmf\lib;

use think\Db;

/**
 * ThinkCMF权限认证类
 */
class Auth
{

    //默认配置
    protected $_config = [];

    public function __construct()
    {
    }

    /**
     * 检查权限,基于url访问规则的认证.
     * @param $name string|array  需要验证的规则列表,支持逗号分隔的权限规则或索引数组
     * @param $uid  int           认证用户的id
     * @param $relation string    如果为 'or' 表示满足任一条规则即通过验证;如果为 'and'则表示需满足所有规则才能通过验证
     * @return boolean           通过验证返回true;失败返回false
     * 这有个问题,就是在页面级别.或者方法内,对不同级别,或者说是不同角色组,
     */
    public function check($uid,$name,$app='',$relation = 'or')
    {

        // 尽量name不能写重了.否则会判断错误.
        if (empty($uid)) {
            return false;
        }
        if ($uid == 1) {
            return true;
        }

        if (is_string($name)) {
            $name = strtolower($name);
            // 对app过滤问题
            if (strpos($name, ',') !== false) {
                $name = explode(',', $name);
            } else {
                $where=[];
                $where['name']=$name;
                if(!empty($app)){
                    $where['type']=$app.'_url';
                }

                // 是否存在
                $findAuthRuleCount = Db::name('auth_rule')->where($where)->count();

                // 权限点不在控制列表中的.可以任意访问
                if ($findAuthRuleCount == 0) {//没有规则时,不验证!
                    return true;
                }

                $name = [$name];
            }
        }

        $list   = []; //保存验证通过的规则名
        // 取出对应的分组权限信息
        $groups = Db::name('RoleUser')
            ->alias("a")
            ->join('__ROLE__ r', 'a.role_id = r.id')
            ->where(["a.user_id" => $uid, "r.status" => 1])
            ->column("role_id");

        if (in_array(1, $groups)) {
            return true;
        }

        if (empty($groups)) {
            return false;
        }
        // 取出分组路由表,根据分组信息和路由信息
        // 用这块来控制权限过滤
        $where=[];
        $where['a.role_id']= ["in", $groups];
        $where['b.name']= ["in", $name];
        if(!empty($app)){
            $where['a.type']=$app.'_url';
        }
        $rules = Db::name('AuthAccess')
            ->alias("a")
            ->join('__AUTH_RULE__ b ', ' a.rule_name = b.name')
//            ->where(["a.role_id" => ["in", $groups], "b.name" => ["in", $name]])
            ->where($where)
//                ->fetchSql(true)
            ->select();
//    var_dump($rules);exit;
        foreach ($rules as $rule) {
            // 附加条件,判断
            if (!empty($rule['condition'])) { //根据condition进行验证
                $user = $this->getUserInfo($uid);//获取用户信息,一维数组
                // name字段： grade2, condition字段： {score}>100 and {score}<200
                // 判断的是user的字段
                // $auth->check('grade2',uid) 判断用户积分是不是在100-200
                $command = preg_replace('/\{(\w*?)\}/', '$user[\'\\1\']', $rule['condition']);
                //dump($command);//debug
                //  注意这个地方有漏洞.
                @(eval('$condition=(' . $command . ');'));
                if ($condition) {
                    $list[] = strtolower($rule['name']);
                }
            } else {
                $list[] = strtolower($rule['name']);
            }
        }

        //满足条件之一就可以通过,但这个也是漏洞.万一有一个权限不是他的.所以这个地方,不能用分组模式,几个权限点一起检查,单权限点检查没问题.
        if ($relation == 'or' and !empty($list)) {
            return true;
        }
        // 多组检查,可以设置为and,就全部检查
        $diff = array_diff($name, $list);
        if ($relation == 'and' and empty($diff)) {
            return true;
        }
        return false;
    }

    /**
     * 获得用户资料
     * @param $uid
     * @return mixed
     */
    private function getUserInfo($uid)
    {
        static $userInfo = [];
        if (!isset($userInfo[$uid])) {
            $userInfo[$uid] = Db::name('user')->where(['id' => $uid])->find();
        }
        return $userInfo[$uid];
    }

}
