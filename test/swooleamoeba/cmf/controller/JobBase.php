<?php

namespace cmf\controller;

use think\queue\Job;

/**
 * Job基础方法
 * 
 * @author chenliming <19536238@qq.com>
 * @since     2018-04-21
 * 
 */
class JobBase {

    /**
     * 后续处理
     * @access protected
     * @param Job $job 任务名称
     * @param array $data 参数
     * @param Boolean $is_release 是否重复执行
     * @return mixed
     */
    protected function after(Job $job, $data, $is_release = false) {
        
        if ($job->attempts() > 3) {
            //通过这个方法可以检查这个任务已经重试了几次了
            if ($is_release === FALSE) {
                $job_data = object_array($job);
                $job_data = json_decode($job_data["*payload"], true);
                $log = new \services\logs\service\ApiService();
                $log->writeLog('job', $job_data, 'failure');
                $job->delete();
            }
        }
        //如果任务执行成功后 记得删除任务，不然这个任务会重复执行，直到达到最大重试次数后失败后，执行failed方法
        $job->delete();

        //特殊情况下,会需要重复执行任务.就需要多次重复
        if ($is_release === true) {
            // 重新发布这个任务
            if (empty($delay)) {
                $delay = 10 * 6;
            }

            $job->release($delay); //$delay为延迟时间
        }
    }

    /**
     * 出错后处理
     * @access public
     * @param array $data 参数
     * @return mixed
     */
    public function failed($data) {
        // ...任务达到最大重试次数后，失败了
        $log = new \services\logs\service\ApiService();
        $log->writeLog('job', $data, 'error');
    }

}
