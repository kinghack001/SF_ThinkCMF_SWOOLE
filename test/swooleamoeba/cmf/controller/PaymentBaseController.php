<?php

// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2018 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +---------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------

namespace cmf\controller;

use think\Db;
use common\home\model\ThemeModel;
use think\View;

class PaymentBaseController extends HomeBaseController {

    public function _initialize() {
        parent::_initialize();
        //必须登录了.才能跳转过来,否则不能跳
        // 检查是否已经授权过了

//        $sessionUser = session('user');
//        if (empty($sessionUser)) {
//            // 跳转登陆后,才能重回本处
//            // 判断session
//            $data = ['redirect' => urlencode($this->request->url())];
//            $data = base64_encode(json_encode($data));
//
//            $result = url('/user#index/login/index', ['data' => $data]);
//            if (IS_SWOOLE) {
//                return $this->location($result);
//            } else {
//                header("Location:" . $result);
//                exit();
//            }
//        }
    }

    public function _initializeView() {
        $cmfThemePath = config('cmf_theme_path');
        $cmfDefaultTheme = config('cmf_pay_theme');


        $themePath = "{$cmfThemePath}{$cmfDefaultTheme}";

        $root = cmf_get_root();
        $app_root = get_app_root();

        //使cdn设置生效
        $cdnSettings = cmf_get_option('cdn_settings');
        if (empty($cdnSettings['cdn_static_root'])) {
            $viewReplaceStr = [
                '__ROOT__' => $app_root,
                '__TMPL__' => "{$root}/{$themePath}",
                '__STATIC__' => "{$root}/static",
                '__WEB_ROOT__' => $root
            ];
        } else {
            $cdnStaticRoot = rtrim($cdnSettings['cdn_static_root'], '/');
            $viewReplaceStr = [
                '__ROOT__' => $root,
                '__TMPL__' => "{$cdnStaticRoot}/{$themePath}",
                '__STATIC__' => "{$cdnStaticRoot}/static",
                '__WEB_ROOT__' => $cdnStaticRoot
            ];
        }

        $viewReplaceStr = array_merge(config('view_replace_str'), $viewReplaceStr);
        config('template.view_base', "{$themePath}/");
        if (IS_SWOOLE) {
            // 为了区分模板载入路径.不同端进来后,会存在定位模板的问题.
            session('template.view_base_' . APP_NAME, "$themePath/");
        }
        config('view_replace_str', $viewReplaceStr);

        $themeErrorTmpl = "{$themePath}/error.html";
        if (file_exists_case($themeErrorTmpl)) {
            config('dispatch_error_tmpl', $themeErrorTmpl);
        }

        $themeSuccessTmpl = "{$themePath}/success.html";
        if (file_exists_case($themeSuccessTmpl)) {
            config('dispatch_success_tmpl', $themeSuccessTmpl);
        }
    }

}
