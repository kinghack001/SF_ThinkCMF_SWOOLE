<?php

namespace cmf\controller;

use think\Config;
use think\Controller;

/**
 * Job基础控制器
 * 
 * @author chenliming <19536238@qq.com>
 * @since     2018-04-21
 * 
 */
class JobBaseController extends Controller {

    public function _initialize() {
        parent::_initialize();
        // 强制为ajax响应
        $this->setAjax();
        // 在这块做鉴权认证
        // 做IP简单认证
        $ip = $this->request->ip();
        $client = config('job_client');
        if (!in_array($ip, $client)) {
            return $this->error('非授权IP');
        }
    }

}
