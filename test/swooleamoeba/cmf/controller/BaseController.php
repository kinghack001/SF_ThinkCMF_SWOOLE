<?php

// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2018 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +---------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------

namespace cmf\controller;

use think\Controller;
use think\Request;
use think\Response;
use think\View;
use think\Config;

class BaseController extends Controller {

    /**
     * 构造函数
     * @param Request $request Request对象
     * @access public
     */
    public function __construct(Request $request = null) {
        // 独立模式才需要判断是否安装.产品开发模式,直接放弃这个判断
        if (Config::get('is_sell') == true) {
            //分析是否安装
            if (!cmf_is_installed() && $request->module() != 'install') {
                if (IS_SWOOLE) {
                    return $this->location(url("install/index/index"));
                } else {
                    header('Location: ' . cmf_get_root() . '/index.php?s=install');
                    exit;
                }
            }
        }


        if (is_null($request)) {
            $request = Request::instance();
        }

        $this->request = $request;

        $this->_initializeView();
        // 这个地方有可能不是html访问.直接默认view有一定的问题
        $this->view = View::instance(Config::get('template'), Config::get('view_replace_str'));


        // 控制器初始化
        $this->_initialize();

        // 前置操作方法
        if ($this->beforeActionList) {
            foreach ($this->beforeActionList as $method => $options) {
                is_numeric($method) ?
                    $this->beforeAction($options) :
                    $this->beforeAction($method, $options);
            }
        }
    }

    // 初始化视图配置
    protected function _initializeView() {
        
    }

    /**
     *  排序 排序字段为list_orders数组 POST 排序字段为：list_order
     */
    protected function listOrders($model) {
        if (!is_object($model)) {
            return false;
        }

        $pk = $model->getPk(); //获取主键名称
        $ids = $this->request->post("list_orders/a");

        if (!empty($ids)) {
            foreach ($ids as $key => $r) {
                $data['list_order'] = $r;
                $model->where([$pk => $key])->update($data);
            }
        }

        return true;
    }
    /**
     * 加载模板输出
     * @access protected
     * @param  string $template 模板文件名
     * @param  array $vars 模板输出变量
     * @param  array $replace 模板替换
     * @param  array $config 模板参数
     * @return mixed
     */
//    protected function fetch($template = '', $vars = [], $replace = [], $config = [])
//    {
//        return Response::create($this->view->fetch($template, $vars, $replace, $config), '\\cmf\\response\\Html');
//    }

    /**
     * 渲染内容输出
     * @access protected
     * @param  string $content 模板内容
     * @param  array $vars 模板输出变量
     * @param  array $replace 替换内容
     * @param  array $config 模板参数
     * @return mixed
     */
//    protected function display($content = '', $vars = [], $replace = [], $config = [])
//    {
//        return Response::create($this->view->display($content, $vars, $replace, $config), '\\cmf\\response\\Html');
//    }
    
    // 强制设置为ajax方式
    protected function setAjax($is_not = true) {
        if ($is_not === true) {
            if (!$this->request->isAjax()) {
                $this->request->server([
                    'HTTP_X_REQUESTED_WITH' => 'xmlhttprequest',
                ]);
            }
        } else {
            $this->request->server([
                'HTTP_X_REQUESTED_WITH' => '',
            ]);
        }
    }

}
