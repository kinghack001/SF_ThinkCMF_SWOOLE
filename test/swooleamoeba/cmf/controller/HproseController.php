<?php

namespace cmf\controller;

use Hprose\Http\Server;

/**
 * hprose统一控制器
 * 
 * @author chenliming <chenliming@lacertafashion.com>
 * @since     2016-12-4
 * 
 */
class HproseController {

    protected $allowMethodList = '';
    protected $crossDomain = false;
    protected $P3P = false;
    protected $get = true;
    protected $debug = false;

    /**
     * 架构函数
     * @access public
     */
    public function __construct() {
        //控制器初始化
        if (method_exists($this, '_initialize')){
            $this->_initialize();
        }
            
        //导入类库
        Vendor('Hprose.src.Hprose');
        //Vendor('Hprose.HproseHttpServer');
        //实例化HproseHttpServer
       $server = new \HproseHttpServer();
        if ($this->allowMethodList) {
            $methods = $this->allowMethodList;
        } else {
            $methods = get_class_methods($this);
            $methods = array_diff($methods, array('__construct', '__call', '_initialize'));
        }
        $server->addMethods($methods, $this);
//        if (APP_DEBUG || $this->debug) {
            $server->setDebugEnabled(true);
//        }
        // Hprose设置
        $server->setCrossDomainEnabled($this->crossDomain);
        $server->setP3PEnabled($this->P3P);
        $server->setGetEnabled($this->get);
        $server->setSimple(true);
        // 启动server
        $server->start();
    }
    
        /**
     * 初始化操作
     * @access protected
     */
    protected function _initialize()
    {
    }

    /**
     * 魔术方法 有不存在的操作的时候执行
     * @access public
     * @param string $method 方法名
     * @param array $args 参数
     * @return mixed
     */
    public function __call($method, $args) {
        
    }
    
 

}
