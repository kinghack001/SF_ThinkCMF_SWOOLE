<?php

// +----------------------------------------------------------------------
// | 微信相关设置
// +----------------------------------------------------------------------
return [
    'Web'=>[

        'token' => '', // 填写你设定的key
        'appid' => '', // 填写高级调用功能的app id, 请在微信开发模式后台查询
        'appsecret' => '', // 填写高级调用功能的密钥
        'encodingaeskey' =>'',
				'mch_id' => '', // 微信支付，商户ID（可选）
        'partnerkey' => '', // 微信支付，密钥（可选）
        'ssl_cer' => '', // 微信支付，证书cert的路径（可选，操作退款或打款时必需）
        'ssl_key' => '', // 微信支付，证书key的路径（可选，操作退款或打款时必需）
        'cachepath' => '', // 设置SDK缓存目录（可选，默认位置在./src/Cache下，请保证写权限）


    ],
    'Official'=>[
		'token' => '', // 填写你设定的key
        'appid' => '', // 填写高级调用功能的app id, 请在微信开发模式后台查询
        'appsecret' => '', // 填写高级调用功能的密钥
        'encodingaeskey' =>'',
        'mch_id' => '', // 微信支付，商户ID（可选）
        'partnerkey' => '', // 微信支付，密钥（可选）
        'ssl_cer' => '', // 微信支付，证书cert的路径（可选，操作退款或打款时必需）
        'ssl_key' => '', // 微信支付，证书key的路径（可选，操作退款或打款时必需）
        'cachepath' => '', // 设置SDK缓存目录（可选，默认位置在./src/Cache下，请保证写权限）
    ],
    'enterprise'=>[
        'token' => '', // 填写你设定的key
        'appid' => '', // 填写高级调用功能的app id, 请在微信开发模式后台查询
        'appsecret' => '', // 填写高级调用功能的密钥
        'encodingaeskey' =>'',
        'mch_id' => '', // 微信支付，商户ID（可选）
        'partnerkey' => '', // 微信支付，密钥（可选）
        'ssl_cer' => APP_COMMON. '/cert/weixin/apiclient_cert.pem', // 微信支付，证书cert的路径（可选，操作退款或打款时必需）
        'ssl_key' => APP_COMMON . '/cert/weixin/apiclient_key.pem', // 微信支付，证书key的路径（可选，操作退款或打款时必需）
        'cachepath' => '', // 设置SDK缓存目录（可选，默认位置在./src/Cache下，请保证写权限）

        'key'=>'', //与partnerkey相同
        'pay_notify_url'=>'http://www.yunjing720.com/payment/pay_notify.php',
        'certPath'	=>	APP_APP. '/cert/weixin/apiclient_cert.pem',
        'keyPath'	=>	APP_APP . '/cert/weixin/apiclient_key.pem',
    ],
    'WechatMenuLink' => [
        ['link' => '@wx', 'title' => '微信商城首页'],
        ['link' => '@wx-demo-jsapi', 'title' => 'JSAPI支付测试']
    ],
];
