<?php

namespace common\platform\model;

use think\Model;


class RecycleBinModel extends Model
{

    public function user()
    {
        return $this->belongsTo('common\user\model\UserModel', 'user_id')->setEagerlyType(1);
    }


}