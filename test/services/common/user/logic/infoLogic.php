<?php

namespace common\user\logic;

use think\Db;
use common\wechat\model\WechatUserModel;

/**
 * 用户查询类逻辑_公用
 * 
 */
class infoLogic {

    /**
     * 获取角色组
     * @param array $where 过滤条件
     * @return mixed
     */
    public function info($oauth_info = '') {
        $UserWx = new WechatUserModel();
        $where = [];
        $where['app_id'] = $oauth_info['app_id'];
        $where['openid'] = $oauth_info['openid'];
        if (!empty($oauth_info['unionid'])) {
            $where['unionid'] = $oauth_info['unionid'];
        }

        $UserWx_info = $UserWx->where($where)->find();
        
        return $UserWx_info;
    }

}
