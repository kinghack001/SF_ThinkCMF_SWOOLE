<?php

namespace common\user\service;

use think\Db;
use common\user\model\UserModel;

/**
 * 用户接口,登录
 * 
 */
class LoginService {

    /**
     * 获取角色组
     * @param array $where 过滤条件
     * @return mixed
     */
    public function doLogin($user = []) {
       $userModel         = new UserModel();
       $log                = $userModel->doThird($user);
       switch ($log) {
           case 1:
               // 记录用户登录情况
               // 加积分
               cmf_user_action('login');
               return true;

               break;

           default:
               return FALSE;
               break;
       }
    }
    


}
