<?php

namespace common\user\service;

use think\Db;
use common\wechat\service\ApiService as wx;

/**
 * 用户接口,主查询
 * 
 */
class ApiService {

    /**
     * 获取角色组
     * @param array $where 过滤条件
     * @return mixed
     */
    public function roles($where = '') {
        if (empty($where)) {
            $where = ['status' => 1];
        } else {
            if (is_array($where)) {
                $where = array_merge(['status' => 1], $where);
            } else {
                $sql = $where;
                $where = [];
                $where['status'] = 1;
                $where['id'] = ['not in', $sql];
            }
        }

        $roles = Db::name('role')->where($where)->order("id DESC")->select();
        return $roles;
    }

    /**
     * 获取角色所在组
     * @param array $where 过滤条件
     * @return mixed
     */
    public function role_ids($id = 0, $where = '') {
        if (empty($where)) {
            $where = ["user_id" => $id];
        } else {
            if (is_array($where)) {
                $where = array_merge(["user_id" => $id], $where);
            } else {
                $where = [];
                $where['user_id'] = $id;
            }
        }

        $role_ids = DB::name('RoleUser')->where($where)->column("role_id");
        return $role_ids;
    }

    /**
     * 获取角色信息
     * @param array $where 过滤条件
     * @return mixed
     */
    public function user_info($id = 0, $where = '') {
        if (empty($where)) {
            $where = ["id" => $id];
        } else {
            if (is_array($where)) {
                $where = array_merge(["id" => $id], $where);
            } else {
                $where = [];
                $where['id'] = $id;
            }
        }

        $user = DB::name('user')->where($where)->find();
        return $user;
    }

    /**
     * 获取角色信息
     * @param array $where 过滤条件
     * @return mixed
     */
    public function user_info_all($id = 0) {
//        if (empty($where)) {
//            $where = ["id" => $id];
//        } else {
//            if (is_array($where)) {
//                $where = array_merge(["id" => $id], $where);
//            } else {
//                $where = [];
//                $where['id'] = $id;
//            }
//        }

        $where = [];
        $where['parent_id'] = $id;


        $user = DB::name('user')->where($where)->select()->toarray();
        if (empty($user)) {
            return self::user_info($id);
        }
        // 有第三方用户
        $user_id = [];
        foreach ($user as $key => $value) {
            $user_id[] = $value['id'];
        }

        $third_party_user = DB::name('third_party_user')->where('user_id', 'in', $user_id)->select()->toarray();
        // 按类型,把相同第三方的数据读出来
        $user_id = [];
        $wx = new wx;
        foreach ($third_party_user as $key => $value) {
            if ($value['type'] = 1) {
                // 
                $where = [];
                $where['app_id'] = $value['app_id'];
                $where['openid'] = $value['openid'];
                $where['unionid'] = $value['union_id'];
                $wx_access_token = DB::name('wx_access_token')->where($where)->select()->toarray();
                // 判断是否超期

                $wx_oauth= $wx->get_oauth($wx_access_token);
               // var_dump($wx_oauth);exit;
            }
        }


        return $user;
    }

}
