<?php

namespace  common\logs\job;

use think\queue\Job;


/**
 * 日志
 * 
 * @author 陈黎明 <19536238@qq.com>
 * @since     2018-4-20
 * 
 */
class Logs  {

    /**
     * 写日志任务
     * @access public
     * @param object $job 任务名. TP默认形式
     * @param array $data 提交数据
     * @return mixed
     * 调用形式:延迟调用 Queue::later(15,'services\logs\job\Job@writeLog', $data);
     * 即时调用 Queue::push('services\logs\job\Job@writeLog', $data);
     */
    public function writeLog(Job $job, $data) {
        //....这里执行具体的任务

        $logs = new \services\logs\service\ApiService();
        $logs->writeLog($data['name'], $data['message'], $data['module']);

        $job->delete();
    }

}
