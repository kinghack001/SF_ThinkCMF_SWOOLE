<?php

namespace  common\logs\service;

/**
 * 日志相关服务接口
 * 
 * @author chenliming <19536238@qq.com>
 * @since     2018-04-21
 * 
 */
class ApiService {

    //自定义初始化
    protected function initialize() {
        //需要调用`Model`的`initialize`方法
        parent::initialize();
        //TODO:自定义的初始化
    }

    function ver() {
        return "v0.0.1";
    }

    /*
     *  记录日志
     *  @param  string  $file_name  文件名
     *  @param  string  $message  日志内容 
     *  @param  string  $module  模块名
     */

    public function writeLog($file_name = '', $message = '', $module = '') {
        $path = CMF_ROOT . 'data/log/' . date('Y-m-d');

        if (!empty($module)) {
            $path = $path . '/' . $module;
        }

        $file_name = $file_name != '' ? $file_name : 'default';

        // 强制转换为string
        // 判断下类型
        //如果是对象,则输出为 json
        if (is_object($message)) {
            $data = json_encode(object_array($message));
        } elseif (is_array($message)) {
            $data = json_encode($message);
        } else {
            $data = $message;
        }

        // 检查日志插件是否存在
        if (!class_exists('SeasLog', false)) {
            $path = $path . '/' . $file_name;
            $path_and_name = $path . '/' . date('Ymd') . '.log';
            return self::filelog($path_and_name, $data, $path);
        } else {
            \SeasLog::setBasePath($path);
            \SeasLog::setLogger($file_name);
            \SeasLog::info($data . PHP_EOL);
            return true;
        }
    }

    /*
     *  文件型_记录日志
     *  @param  string  $file_name  文件名
     *  @param  string  $message  日志内容 
     *  @param  string  $module  模块名
     */

    private function filelog($file_name = '', $data = '', $path = '') {
        if (!file_exists($path)) {
            if (!mkdir($path, 0777, true)) {
                return false;
            }
        }

        if ($data) {
            if (!file_put_contents($file_name, date("Y-m-d H:i:s", time()) . " | " . $data . PHP_EOL, FILE_APPEND | LOCK_EX)) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

}
