<?php

namespace  common\baidu\service;

use think\Model;
use cmf\lib\HttpBase;
use think\Config;

/**
 * 百度相关对接服务接口
 * 
 * @author chenliming <19536238@qq.com>
 * @since     2018-04-20
 * 
 */
class ApiService extends Model {

    //自定义初始化
    protected function initialize() {
        //需要调用`Model`的`initialize`方法
        parent::initialize();
        //TODO:自定义的初始化
    }

    /**
     * 需要收录的网址
     * @param string $url_arr 需要收录的网址
     * @return mixed
     */
    public function NewPush($url_arr = '') {
        $datazz = new \thirdparty\baidu\logic\Datazz();
        $data = $datazz->NewPush($url_arr);
        if ($data === FALSE) {
            $this->error = $datazz->getError();
            return FALSE;
        } else {
            return true;
        }
    }

    /**
     * 需要收录的历史网址
     * @param string $url_arr 需要收录的历史网址
     * @return mixed
     */
    public function HistoryPush($url_arr = '') {
        $datazz = new \thirdparty\baidu\logic\Datazz();
        $data = $datazz->HistoryPush($url_arr);
        if ($data === FALSE) {
            $this->error = $datazz->getError();
            return FALSE;
        } else {
            return true;
        }
    }

}
