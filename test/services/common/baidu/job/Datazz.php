<?php

namespace  common\baidu\job;

use think\queue\Job;
//use cmf\lib\HttpBase;
//use think\Config;
use cmf\controller\JobBase;

/**
 * 百度熊掌任务对接服务
 * 
 * @author 陈黎明 <19536238@qq.com>
 * @since     2018-4-20
 * 
 */
class Datazz extends JobBase{

    /**
     * 新页面推荐
     * @access public
     * @param object $job 任务名. TP默认形式
     * @param array $data 提交数据
     * @return mixed
     * 调用形式:延迟调用 Queue::later(15,'thirdparty\baidu\job\Datazz@NewPush', $url, $queue = null);
     * 即时调用 Queue::push('thirdparty\baidu\job\Datazz@NewPush', $url, $queue = null);
     */
    public function NewPush(Job $job, $data) {
        //....这里执行具体的任务
        
        // 任务可以用的方式如下
        
        // 方式一:推荐用这种,至少可以少一次请求.加快速度
        $baidu = new \thirdparty\baidu\service\ApiService();
        $baidu->NewPush($data);
    
        // 方式二:如果涉及对外的,可以用http方式,这种方式,适应性好.容易调试
//        $Http = new HttpBase();
//        $config = Config::get('baidu');
//        $config = $config['data.zz']['job']['NewPush'];
//        $url = $config["job_url"];
//        $result = $Http->post($url, ['url' => $data]);
        
        // 本框架内,队列,如果没有特殊需求,均使用此格式.上面是具体任务处理.下面是标准后续处理.
        $this->after($job, $data);
    }

    /**
     * 历史页面推荐
     * @access public
     * @param object $job 任务名. TP默认形式
     * @param array $data 提交数据
     * @return mixed
     * 调用形式:延迟调用 Queue::later(15,'thirdparty\baidu\job\Datazz@HistoryPush', $url, $queue = null);
     * 即时调用 Queue::push('thirdparty\baidu\job\Datazz@HistoryPush', $url, $queue = null);
     */
    public function HistoryPush(Job $job, $data) {
        //....这里执行具体的任务
        $baidu = new \thirdparty\baidu\service\ApiService();
        $baidu->HistoryPush($data);
        
//        $Http = new HttpBase();
//        $config = Config::get('baidu');
//        $config = $config['data.zz']['job']['HistoryPush'];
//        $url = $config["job_url"];
//        $result = $Http->post($url, ['url' => $data]);
        
        $this->after($job, $data);
        
    }    
    

}
