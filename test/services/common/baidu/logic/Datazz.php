<?php

namespace  common\baidu\logic;

use think\Model;
use cmf\lib\HttpBase;
use think\Config;

/**
 * 百度熊掌号自动推送的逻辑层
 * 
 * @author chenliming <19536238@qq.com>
 * @since     2018-04-20
 * 
 */
class Datazz extends Model {

    //自定义初始化
    protected function initialize() {
        //需要调用`Model`的`initialize`方法
        parent::initialize();
        //TODO:自定义的初始化
    }

    /**
     * 需要收录的网址
     * @param string $url_arr 需要收录的网址
     * @return mixed
     */
    public function NewPush($url_arr = '') {
        $Http = new HttpBase();
        $config = Config::get('baidu');
        $config = $config['data.zz'];
        $data = $Http->prepare_post_body(array_merge($config['data'], $config['new']), 0);
        $url = $config["url"] . $config["prefix_api"] . $data;

        $result = $Http->post($url, $url_arr);

        return TRUE;
//        var_dump($result);exit;
//        $urls = array(
//            $url_arr
//        );
//        $api =  'http://data.zz.baidu.com/urls?appid=1553767668817562&token=cmB6NnxQnqbEtZST&type=realtime,original';
//        $ch = curl_init(); // ‘定义声明PHP curl（）
//        $options = array( //‘定义声明PHP curl（）的各项参数也就是POST的数据内容
//            CURLOPT_URL => $api, //’提交网址定义
//            CURLOPT_POST => true, // ’curl（）的方式为POST 根据不同可以有get 用于获取别人网站内容
//            CURLOPT_RETURNTRANSFER => true, //‘只返回结果
//            CURLOPT_POSTFIELDS => implode('\n',$urls),
//            CURLOPT_HTTPHEADER => array('Content-Type: text/plain')
//        );
//        curl_setopt_array($ch, $options); // ’执行curl（）
//        $result = curl_exec($ch); //‘获取执行curl（）结果
//        var_dump($result);exit;
////        //echo $result; // ’输出结果
    }

    /**
     * 需要收录的历史网址
     * @param string $url_arr 需要收录的历史网址
     * @return mixed
     */
    public function HistoryPush($url_arr = '') {
        $Http = new HttpBase();
        $config = Config::get('baidu');
        $config = $config['data.zz'];
        $data = $Http->prepare_post_body(array_merge($config['data'], $config['history']), 0);
        $url = $config["url"] . $config["prefix_api"] . $data;

        $result = $Http->post($url, $url_arr);

        return TRUE;
//        var_dump($result);exit;
//        $urls = array(
//            $url_arr
//        );
//        $api = 'http://data.zz.baidu.com/urls?appid=1553767668817562&token=cmB6NnxQnqbEtZST&type=batch';
//        $ch = curl_init();
//        $options =  array(
//            CURLOPT_URL => $api,
//            CURLOPT_POST => true,
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_POSTFIELDS => implode("\n", $urls),
//            CURLOPT_HTTPHEADER => array('Content-Type: text/plain'),
//        );
//        curl_setopt_array($ch, $options);
//        $result = curl_exec($ch);
//       // echo $result;
//        
    }

}
