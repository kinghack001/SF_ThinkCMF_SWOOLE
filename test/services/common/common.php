<?php

use think\Config;
use common\job\Job;
use think\Url;
use think\Request;
use think\Loader;
use think\Route;
use cmf\lib\Storage;
use think\Db;
use dir\Dir;
use think\Cache;
// 应用公共文件
// 本文件尽量写,标签里面可能会用到的.或者是使用频率较高的函数.避免函数载入过多.占用内存

/**
 * 获取微信操作对象
 * @staticvar array $wechat
 * @param type $type
 * @return WechatReceive
 */
//if (!function_exists('load_wechat')) {
// function & load_wechat($type = '') {
//    static $wechat = array();
//    $index = md5(strtolower($type));
//    if (!isset($wechat[$index])) {
//        if(strtolower($type)=='pay'){
//            $config = Config::get('wechat.enterprise');
//        }else{
//            $config = Config::get('wechat.Official');
//        }
//
//        $config['cachepath'] = CACHE_PATH . 'Data/';
//        $wechat[$index] = \Wechat\Loader::get($type, $config);
//    }
//    return $wechat[$index];
//}   
//}


/**
 * XML文档解析成数组，并将键值转成小写
 * @param  xml $xml
 * @return array
 */
if (!function_exists('_extractXml')) {

    function _extractXml($xml = '') {
        $data = (array) simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
        return array_change_key_case($data, CASE_LOWER);
    }

}


/**
 * 对变量进行 JSON 编码,主要针对有汉字或者特殊字符的
 * @param mixed value 待编码的 value ，除了resource 类型之外，可以为任何数据类型，该函数只能接受 UTF-8 编码的数据
 * @return string 返回 value 值的 JSON 形式
 */
if (!function_exists('json_encode_ex')) {

    function json_encode_ex($value = '') {
        if (version_compare(PHP_VERSION, '5.4.0', '<')) {
            $str = json_encode($value);
            $str = preg_replace_callback("#\\\u([0-9a-f]{4})#i", function($matchs) {
                return iconv('UCS-2BE', 'UTF-8', pack('H4', $matchs[1]));
            }, $str);
            return $str;
        } else {
            return json_encode($value, JSON_UNESCAPED_UNICODE);
        }
    }

}


/**
 * 将对象转换成数组
 * @param unknown_type $obj
 * @return unknown
 */
if (!function_exists('object_to_array')) {

    function object_to_array($obj = object) {
        $_arr = is_object($obj) ? get_object_vars($obj) : $obj;
        foreach ($_arr as $key => $val) {
            $val = (is_array($val) || is_object($val)) ? object_to_array($val) : $val;
            $arr[$key] = $val;
        }
        return $arr;
    }

}


/**
 * 将对象转换成数组/stdClass Object转array  
 * @param object $obj 要转换的对象
 * @param boolean $is_clear 是否清除key中的特殊字符
 * @return array
 */
if (!function_exists('object_array')) {

    function object_array($array, $is_clear = true) {
        if (is_object($array)) {
            $array = (array) $array;
        }
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                if ($is_clear === true) {
                    $array[clearInvisibleCharacter($key)] = object_array($value);
                    unset($array[$key]);
                } else {
                    $array[$key] = object_array($value);
                }
            }
        }
        return $array;
    }

}


/**
 * 将对象转换成数组/也可以是数组和数组的混合/小数量可用
 * @param object $object
 * @return array
 */
if (!function_exists('object2array_pre')) {

    function object2array_pre(&$object) {
        if (is_object($object)) {
            $arr = (array) ($object);
        } else {
            $arr = &$object;
        }
        if (is_array($arr)) {
            foreach ($arr as $varName => $varValue) {
                $arr[$varName] = object2array_pre($varValue);
            }
        }
        return $arr;
    }

}


/**
 * 将XML转换成数组
 * @param unknown_type $obj
 * @return unknown
 */
if (!function_exists('xml_to_array')) {

    function xml_to_array($xml = object) {
        $encoding = mb_detect_encoding($xml, "UTF-8, ISO-8859-1, GBK, gb2312");
        if ($encoding != "UTF-8") {
            $req = iconv($encoding, "utf-8//IGNORE", $xml);
        } else {
            $req = $xml;
        }
        $retxml = simplexml_load_string($req);
        $ret = object_to_array($retxml);
        return $ret;
    }

}


/**
 * 判断是否是json对象
 * @param unknown_type $string
 * @return Boolean
 */
if (!function_exists('is_json')) {

    function is_json($string = '') {
        if (empty($string)) {
            return FALSE;
        }
        if (is_string($string)) {
            json_decode($string);
            return (json_last_error() == JSON_ERROR_NONE);
        } else {
            return FALSE;
        }
    }

}


/*
 *  记录日志/直接丢队列中,不影响本身业务执行
 *  @param  string  $message  日志内容
 *   @param  string  $file_name  文件名
 *  @param  string  $module  模块名
 */
if (!function_exists('addLog')) {

    function addLog($message = '', $file_name = '', $module = '') {

        if (is_object($message)) {
            $data = json_encode(object_array($message));
        } elseif (is_array($message)) {
            $data = json_encode($message);
        } else {
            $data = $message;
        }

        $log_data = [];
        $log_data['name'] = $file_name;
        $log_data['message'] = $data;
        $log_data['module'] = $module;
        Job::push('log', $log_data);
    }

}


/*
 *  记录日志
 *  @param  string  $message  日志内容 
 *  @param  string  $file_name  文件名
 *  @param  string  $module  模块名
 */
if (!function_exists('writeLog')) {

    function writeLog($message = '', $file_name = '', $module = '') {
        $log = new \services\logs\service\ApiService();
        $log->writeLog($file_name, $message, $module);
    }

}


/**
 * 清理字符串中的部分不可见控制字符
 *
 * @param string $string 待处理字符串
 * @return string 处理后的字符串
 * @author fising(at)qq.com
 */
if (!function_exists('clearInvisibleCharacter')) {

    function clearInvisibleCharacter($string = '') {
        /* 排除 tab, \n, \r 三个字符 */
        $do_not_searches = array(chr(9), chr(10), chr(13));
        /* 需清理的字符列表 */
        $searches = array();
        for ($i = 0; $i <= 31; $i++) {
            if (!in_array(chr($i), $do_not_searches)) {
                $searches[] = chr($i);
            }
        }
        $searches[] = chr(127);
        return str_replace($searches, '', $string);
    }

}


/**
 * 挂件专用载入方法
 *
 * @param string    $name Widget名称
 * @param array     $data 传入的参数
 * @return mixed
 */
if (!function_exists('widgets')) {

    function widgets($name, $data = []) {
        // 需要定位到正确的控制器上,
        return Loader::action('/common/widget/' . $name, ['data' => $data], 'widget');
    }

}

if (!function_exists('widget_view')) {

    function widget_view($name, $data = []) {
        // 需要定位到正确的控制器上,
        return Loader::action('/common/widget/' . $name . '/view', ['data' => $data], 'widget');
    }

}


if (!function_exists('redirects')) {

    /**
     * 对redirect加强处理,方便项目处理
     * @param mixed         $url 重定向地址 支持Url::build方法的地址
     * @param array|integer $params 额外参数
     * @param array         $with 隐式传参
     * @return \think\response\Redirect
     */
    function redirects($url = [], $params = [], $with = []) {
        //针对sw情况加的特定url处理
//      $url=   url($url, '', false, true);
        $url = url($url);
        return redirect($url, $params, 302, $with, true);
    }

}

if (!function_exists('get_app_root')) {

    /**
     * 获取应用根目录
     * @return string 应用根目录
     */
    function get_app_root() {
        $request = Request::instance();
        $root = $request->root();
        $root = str_replace('/index.php', '', $root);
        if (strlen($root) > 1) {
            if (defined('APP_NAMESPACE') && '/' . APP_NAMESPACE == $root) {
                $_root = preg_replace('/\/' . APP_NAMESPACE . '$/', '', $root);
                if (empty($_root)) {
                    $root = '/' . APP_NAMESPACE;
                } else {
                    $root = rtrim($_root, '/');
                }
            }
        }
        return $root;
    }

}

if (!function_exists('cmf_get_current_user_theme')) {

    /**
     * 获取当前主题名
     * @return string
     */
    function cmf_get_current_user_theme() {
        static $_currentTheme;

        if (!empty($_currentTheme)) {
            return $_currentTheme;
        }

        // 加入t后可强制自己换模板
        $t = 't';
        $theme = config('cmf_user_default_theme');

        $cmfDetectTheme = config('cmf_detect_theme');
        if ($cmfDetectTheme) {
            if (isset($_GET[$t])) {
                $theme = $_GET[$t];
                cookie('cmf_template', $theme, 864000);
            } elseif (cookie('cmf_template')) {
                $theme = cookie('cmf_template');
            }
        }

        $hookTheme = hook_one('switch_theme');

        if ($hookTheme) {
            $theme = $hookTheme;
        }

        $_currentTheme = $theme;

        return $theme;
    }

}

if (!function_exists('sa_auth_check')) {

    /**
     * 非后台管理之外的检查权限
     * @param $userId  int        要检查权限的用户 ID
     * @param $name string|array  需要验证的规则列表,支持逗号分隔的权限规则或索引数组
     * @param $relation string    如果为 'or' 表示满足任一条规则即通过验证;如果为 'and'则表示需满足所有规则才能通过验证
     * @return boolean            通过验证返回true;失败返回false
     */
    function sa_auth_check($userId, $name = null, $relation = 'or') {
        if (strpos($name, 's=') !== FALSE) {
            $_app = Route::parseUrl($name);
            $_app = !empty($_app) ? $_app['module'][0] : '';
            $app = (!empty($_app)) ? $_app : '';
            $tmp = parse_url($name);
            $tmp = str_replace('s=', '', $tmp['query']);
            $tmp = explode('&', $tmp);
            $name = (isset($tmp[0]) && !empty($tmp[0])) ? $tmp[0] : '';
        } else {
            $app = (APP_NAME == 'www') ? '' : APP_NAME;
        }

        if (empty($userId)) {
            return false;
        }

        if ($userId == 1) {
            return true;
        }

        $authObj = new \cmf\lib\Auth();
        if (empty($name)) {
            $request = request();
            $module = $request->module();
            $controller = $request->controller();
            $action = $request->action();
            $name = strtolower($module . "/" . $controller . "/" . $action);
        }

        return $authObj->check($userId, $name, $app, $relation);
    }

}

if (!function_exists('auto_charset')) {

// 自动转换字符集 支持数组转换
    function auto_charset($fContents, $from = 'gbk', $to = 'utf-8') {
        $from = strtoupper($from) == 'UTF8' ? 'utf-8' : $from;
        $to = strtoupper($to) == 'UTF8' ? 'utf-8' : $to;
        if (strtoupper($from) === strtoupper($to) || empty($fContents) || (is_scalar($fContents) && !is_string($fContents))) {
            //如果编码相同或者非字符串标量则不转换
            return $fContents;
        }
        if (is_string($fContents)) {
            if (function_exists('mb_convert_encoding')) {
                return mb_convert_encoding($fContents, $to, $from);
            } elseif (function_exists('iconv')) {
                return iconv($from, $to, $fContents);
            } else {
                return $fContents;
            }
        } elseif (is_array($fContents)) {
            foreach ($fContents as $key => $val) {
                $_key = auto_charset($key, $from, $to);
                $fContents[$_key] = auto_charset($val, $from, $to);
                if ($key != $_key)
                    unset($fContents[$key]);
            }
            return $fContents;
        }
        else {
            return $fContents;
        }
    }

}

if (!function_exists('sa_get_image_preview_url')) {

    /**
     * 获取图片预览链接
     * @param string $file 文件路径，相对于upload
     * @param string $style 图片样式,支持各大云存储
     * @return string
     */
    function sa_get_image_preview_url($file, $style = 'watermark') {
        if (strpos($file, "http") === 0) {
            return $file;
        } else if (strpos($file, "/") === 0) {
            return $file;
        } else {
            $storage = cmf_get_option('storage');
            if (empty($storage['type'])) {
                $storage['type'] = 'Local';
            }
            if ($storage['type'] != 'Local') {
                $watermark = cmf_get_plugin_config($storage['type']);
                $style = empty($style) ? $watermark['styles_watermark'] : $style;
            }
            $storage = Storage::instance();
            // $file
            $extension = pathinfo($file, PATHINFO_EXTENSION);
            $file_thumb = str_replace('.' . $extension, '_thumb.' . $extension, $file);
            // 判断预览文件是否存在
            if (file_exists(SA_UPLOAD . $file_thumb)) {
                return $storage->getPreviewUrl($file_thumb, $style);
            } else {
                return $storage->getPreviewUrl($file, $style);
            }
        }
    }

}

if (!function_exists('sa_is_wechat')) {

    /**
     * 判断是否为微信访问
     * @return boolean
     */
    function sa_is_wechat() {
        $type = '';
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {
            if (strpos($_SERVER['HTTP_USER_AGENT'], 'WindowsWechat') !== false) {
                //windows版微信
                $type = 'windowswechat';
            } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'wechatdevtools') !== false) {
                $type = 'wechatdevtools';
            }
        }

        return empty($type) ? false : $type;
    }

}


if (!function_exists('orderNo')) {

    /**
     * 获取流水号
     * @return boolean
     */
    function orderNo() {
        //格式 时间 年月日 +时间戳+随机数
//        $_temp = date("YmdHis", time()) . time() . rand(10000, 99999);
        $_temp = date("YmdHis") . rand(time(), time()+rand(0,60)) . substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
//        return date('Ymd') . substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
        return $_temp;
    }

}

if (!function_exists('ncPriceCalculate')) {

    /**
     * PHP精确计算  主要用于货币的计算用
     * @param $n1 第一个数
     * @param $symbol 计算符号 + - * / %
     * @param $n2 第二个数
     * @param string $scale  精度 默认为小数点后两位
     * @return  string
     */
    function ncPriceCalculate($n1, $symbol, $n2, $scale = '2') {
        $res = "";
        switch ($symbol) {
            case "+"://加法
                $res = bcadd($n1, $n2, $scale);
                break;
            case "-"://减法
                $res = bcsub($n1, $n2, $scale);
                break;
            case "*"://乘法
                $res = bcmul($n1, $n2, $scale);
                break;
            case "/"://除法
                $res = bcdiv($n1, $n2, $scale);
                break;
            case "%"://求余、取模
                $res = bcmod($n1, $n2, $scale);
                break;
            default:
                $res = "";
                break;
        }
        return $res;
    }

}

if (!function_exists('ncPriceYuan2fen')) {

    /**
     * 价格由元转分
     * @param $price 金额
     * @return int
     */
    function ncPriceYuan2fen($price) {
        $price = (int) ncPriceCalculate(100, "*", ncPriceFormat($price));
        return $price;
    }

}

if (!function_exists('ncPricefen2Yuan')) {

    /**
     * 价格由分转元
     * @param $price 金额
     * @return int
     */
    function ncPricefen2Yuan($price) {
        $price = ncPriceCalculate(ncPriceFormat($price), "/", 100);
        return $price;
    }

}

if (!function_exists('ncPriceFormat')) {

    /**
     * 价格格式化
     *
     * @param int	$price
     * @return string	$price_format
     */
    function ncPriceFormat($price) {
        $price_format = number_format($price, 2, '.', '');
        return $price_format;
    }

}

if (!function_exists('arr2tree')) {

    /**
     * 一维数据数组生成数据树
     * @param array $list 数据列表
     * @param string $id 父ID Key
     * @param string $pid ID Key
     * @param string $son 定义子数据Key
     * @return array
     */
    function arr2tree($list, $id = 'id', $pid = 'pid', $son = 'sub') {
        list($tree, $map) = [[], []];
        foreach ($list as $item) {
            $map[$item[$id]] = $item;
        }
        foreach ($list as $item) {
            if (isset($item[$pid]) && isset($map[$item[$pid]])) {
                $map[$item[$pid]][$son][] = &$map[$item[$id]];
            } else {
                $tree[] = &$map[$item[$id]];
            }
        }
        unset($map);
        return $tree;
    }

}

if (!function_exists('arr2table')) {
        /**
     * 一维数据数组生成数据树
     * @param array $list 数据列表
     * @param string $id ID Key
     * @param string $pid 父ID Key
     * @param string $path
     * @param string $ppath
     * @return array
     */
 function arr2table(array $list, $id = 'id', $pid = 'pid', $path = 'path', $ppath = '')
    {
        $tree = [];
        foreach (arr2tree($list, $id, $pid) as $attr) {
            $attr[$path] = "{$ppath}-{$attr[$id]}";
            $attr['sub'] = isset($attr['sub']) ? $attr['sub'] : [];
            $attr['spt'] = substr_count($ppath, '-');
            $attr['spl'] = str_repeat("&nbsp;&nbsp;&nbsp;├&nbsp;&nbsp;", $attr['spt']);
            $sub = $attr['sub'];
            unset($attr['sub']);
            $tree[] = $attr;
            if (!empty($sub)) {
                $tree = array_merge($tree, arr2table($sub, $id, $pid, $path, $attr[$path]));
            }
        }
        return $tree;
}}


    /**
     * 获取数据树子ID
     * @param array $list 数据列表
     * @param int $id 起始ID
     * @param string $key 子Key
     * @param string $pkey 父Key
     * @return array
     */
  function getArrSubIds($list, $id = 0, $key = 'id', $pkey = 'pid')
    {
        $ids = [intval($id)];
        foreach ($list as $vo) {
            if (intval($vo[$pkey]) > 0 && intval($vo[$pkey]) === intval($id)) {
                $ids = array_merge($ids, getArrSubIds($list, intval($vo[$key]), $key, $pkey));
            }
        }
        return $ids;
    }

if (!function_exists('encode')) {

    /**
     * UTF8字符串加密
     * @param string $string
     * @return string
     */
    function encode($string) {
        list($chars, $length) = ['', strlen($string = iconv('utf-8', 'gbk', $string))];
        for ($i = 0; $i < $length; $i++) {
            $chars .= str_pad(base_convert(ord($string[$i]), 10, 36), 2, 0, 0);
        }
        return $chars;
    }

}

if (!function_exists('decode')) {

    /**
     * UTF8字符串解密
     * @param string $string
     * @return string
     */
    function decode($string) {
        $chars = '';
        foreach (str_split($string, 2) as $char) {
            $chars .= chr(intval(base_convert($char, 36, 10)));
        }
        return iconv('gbk', 'utf-8', $chars);
    }

}

function tozip() {
    //1.获取指定的源目录文件. 2.建立临时下载目录 3.copy文件到临时目录,生成对应的文件 4.打包, 5下载
    $FileUtil = new \fsys\FileUtil();
    $DirBase = new \fsys\DirBase();
    $zip = new \fsys\PclZip();

    $orderNo = orderNo();
    $source_path = CMF_ROOT . 'data/zip/';
    $zip_path = './tmp/' . $orderNo;
    $dest_path = SA_PUBLIC . 'tmp/';
    //zip
    $FileUtil->unlinkDir($zip_path);
    $FileUtil->createDir($zip_path);
    if (!file_exists($source_path)) {
        
    }
    if (!file_exists($dest_path)) {
        $FileUtil->createDir($dest_path);
    }

    $FileUtil->copyDir($source_path, $zip_path);

    $filename = $dest_path . $orderNo . '.zip';

    $zip->PclZip($filename);
    $rs = $zip->create($zip_path, PCLZIP_OPT_REMOVE_PATH, './tmp/');

    if ($rs == 0) {
        echo '异常：' . $zip->errorInfo(true);
    } else {
        echo '备份成功';
    }
    $FileUtil->unlinkDir($zip_path);

    return '/tmp/' . $orderNo . '.zip';
}

/**
 * 日期格式标准输出
 * @param string $datetime 输入日期
 * @param string $format 输出格式
 * @return false|string
 */
function format_datetime($datetime, $format = 'Y年m月d日 H:i:s') {
    return date($format, strtotime($datetime));
}

function rtftohtml($content) {
    //把一些预定义的 HTML 实体转换为字符  
    $html_string = htmlspecialchars_decode($content);
    //将空格替换成空  
    $content = str_replace(" ", "", $html_string);
    //函数剥去字符串中的 HTML、XML 以及 PHP 的标签,获取纯文本内容  
    $contents = strip_tags($content);

    return $contents;
}



if (!function_exists('as_url')) {
    /**
     * Url生成
     * @param string        $url 路由地址
     * @param string|array  $vars 变量
     * @param bool|string   $suffix 生成的URL后缀
     * @param bool|string   $domain 域名
     * @return string
     */
    function as_url($request= object, $vars = '', $suffix = true, $domain = false)
    {
        $path=[$request->module(),$request->controller(),$request->action()];
        $url= implode('/', $path);
        return Url::build($url, $vars, $suffix, $domain);
    }
}


/**
 * 获取漫游功能类的类名
 * @param string $name 插件名
 * @return string
 */
function sa_get_tour_class($name)
{
    $name      = ucwords($name);
    $tourDir = cmf_parse_name($name);
    $class     = "tours\\{$tourDir}\\{$name}Module";
    return $class;
}


/**
 * 生成访问漫游模块的url
 * @param string $url url格式：插件名://控制器名/方法
 * @param array $param 参数
 * @param bool $domain 是否显示域名 或者直接传入域名
 * @return string
 */
function cmf_tour_url($url, $param = [], $domain = false)
{
    $url              = parse_url($url);
    $case_insensitive = true;
    $krpanomodule           = $case_insensitive ? Loader::parseName($url['scheme']) : $url['scheme'];
    $controller       = $case_insensitive ? Loader::parseName($url['host']) : $url['host'];
    $action           = trim($case_insensitive ? strtolower($url['path']) : $url['path'], '/');

    /* 解析URL带的参数 */
    if (isset($url['query'])) {
        parse_str($url['query'], $query);
        $param = array_merge($query, $param);
    }

    /* 基础参数 */
    $params = [
        '_krpanomodule'     => $krpanomodule,
        '_controller' => $controller,
        '_action'     => $action,
    ];
    $params = array_merge($params, $param); //添加额外参数

    return url('\\app\\tour\\controller\\TourController@index', $params, true, $domain);
}


    /**
     * Url生成
     * @param string        $url 路由地址
     * @param string|array  $vars 变量
     * @param bool|string   $suffix 生成的URL后缀
     * @param bool|string   $domain 域名
     * @return string
     */
    function as_xml_url($url = '', $vars = '', $suffix = true, $domain = false)
    {
        Config::set('var_depr','/');
        $url=cmf_url($url, $vars, $suffix, $domain);
        $url= str_replace('=', '/', $url);
        $url= str_replace('?s/', '?s=', $url);
        return $url;
    }
    
    
    /**
 * 获取访问用户的省级
 * @return [array] [description]
 */
function cmf_get_user_province($provinceId)
{
    if ($provinceId == 0) {
        return null;
    }else{
            $province = cache('province')[$provinceId];
        return $province;
    }
}

/**
 * 获取访问用户的市级
 * @return [array] [description]
 */
function cmf_get_user_city($cityId)
{
    if ($cityId == 0) {
        return null;
    }else{
            $city = cache('city')[$cityId];
        return $city;
    }
}

/**
 * 获取访问用户的县级
 * @return [array] [description]
 */
function cmf_get_user_area($areaId)
{
    if ($areaId == 0) {
        return null;
    }else{
            $area = cache('area')[$areaId];
        return $area;
    }
}


/**
 * 清空系统缓存
 */
function sa_clear_cache($path='')
{
    $dirs     = [];
    $rootDirs = cmf_scan_dir(RUNTIME_PATH . "*");
    //$noNeedClear=array(".","..","Data");
    $noNeedClear = ['.', '..', 'log'];
    $rootDirs    = array_diff($rootDirs, $noNeedClear);
    foreach ($rootDirs as $dir) {

        if ($dir != "." && $dir != "..") {
            $dir = RUNTIME_PATH . $dir;
            if (is_dir($dir)) {
                //array_push ( $dirs, $dir );
                $tmpRootDirs = cmf_scan_dir($dir . "/*");
                foreach ($tmpRootDirs as $tDir) {
                    if ($tDir != "." && $tDir != "..") {
                        $tDir = $dir . '/' . $tDir;
                        if (is_dir($tDir)) {
                            array_push($dirs, $tDir);
                        } else {
                            @unlink($tDir);
                        }
                    }
                }
            } else {
                @unlink($dir);
            }
        }
    }
    $dirTool = new Dir("");
    foreach ($dirs as $dir) {
        $dirTool->delDir($dir);
    }
    // 循环清除缓存
    Cache::clear(); 
}


//特定处理函数
        function jsonUnicode2gbk($data){
        if(is_string($data)){
            $data = str_replace(array("\r\n", "\r", "\n"), '\r\n', $data);
            return urlencode($data);
        }
        return $data;
    }
    
    
    /**
 * 设置动态配置
 * @param array $data <br>如：["cmf_default_theme"=>'simpleboot3'];
 * @return boolean
 */
function sa_set_dynamic_config($data)
{
    if (!is_array($data)) {
        return false;
    }

    $configFile = CMF_ROOT . "data/conf/config.php";
    if (file_exists($configFile)) {
        $configs = include $configFile;
    } else {
        $configs = [];
    }

    $configs = array_merge($configs,$data);
    $result  = file_put_contents($configFile, "<?php\treturn " . var_export($configs, true) . ";");

    sa_clear_cache();
    return $result;
}
/**
 * 判断是否为机器人爬虫访问
 * @return boolean
 */
function sa_is_crawler() {
    if (isset($_SERVER['HTTP_USER_AGENT']) && preg_match("/(tencenttraveler|baiduspider+|baidugame|googlebot|msnbot|sosospider+|sogou web spider|ia_archiver|yahoo! slurp|youdaobot|yahoo slurp|msnbot|java|baiduspider|voila|yandex bot|bspider|twiceler|sogou spider|speedy spider|google adsense|heritrix|python-urllib|alexa|ask|exabot|custo|outfoxbot\/yodaobot|yacy|surveybot|legs|lwp-trivial|nutch|stackrambler|perl|mj12bot|netcraft|msiecrawler|wget tools|larbin|fish search)/i", $_SERVER['HTTP_USER_AGENT'])) {
        return true;
    } else {
        return false;
    }
}
/**
 * 获取当前主题的更新时间
 * @return string
 */
function cmf_get_current_theme_date()
{
    static $_currentThemedate;
    if (!empty($_currentThemedate)) {
        return $_currentThemedate;
    }
    $update = config('cmf_default_theme_update');
    if(empty($update)){
        $update =date("Ymd");
    }
    return $update;
}
/**
 * 判断是否为IE9低版本
 * @return boolean
 */
function sa_is_Ie9() {
     if (isset($_SERVER['HTTP_USER_AGENT']) && preg_match("/(msie 7|msie 8|msie 9)/i", $_SERVER['HTTP_USER_AGENT'])) {
        return true;
    } else {
        return false;
    }
}
