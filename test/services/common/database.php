<?php

if (file_exists(CMF_ROOT . "data/conf/database.php")) {
    $runtimeDatabase = include_once CMF_ROOT . "data/conf/database.php";
} else {
    $runtimeDatabase = [];
}

switch (APP_PREFIX) {
    case 'simpleboot3':
        $database = [// 数据库类型
            // 服务器地址
            // 'hostname' => '127.0.0.1',
            // 数据库名
            'database' => 'thinkcmf5',
            // 用户名
            'username' => 'test',
            // 密码
            'password' => '123456',
            // 端口
            'hostport' => '3306',
            // 数据库表前缀
            'prefix' => 'cmf_',
                //#COOKIE_PREFIX#];
        ];
        break;
    default:
        $app_configs = [];
        break;
}


return array_merge($runtimeDatabase, $database);
