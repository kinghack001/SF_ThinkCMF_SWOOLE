<?php

namespace common\portal\service;

use common\portal\model\PortalPostModel;

class PostService {

    public function adminArticleList($filter, $object = object) {
        return $this->adminPostList($filter, false, $object);
    }

    public function adminPageList($filter,$object = object) {
        return $this->adminPostList($filter, true,$object);
    }

    public function adminPostList($filter, $isPage = false, $object = object) {

        $where = [
            'a.create_time' => ['>=', 0],
            'a.delete_time' => 0
        ];

        $join = [
            ['__USER__ u', 'a.user_id = u.id']
        ];

        $field = 'a.*,u.user_login,u.user_nickname,u.user_email';

        $category = empty($filter['category']) ? 0 : intval($filter['category']);
        if (!empty($category)) {
            $where['b.category_id'] = ['eq', $category];
            array_push($join, [
                '__PORTAL_CATEGORY_POST__ b', 'a.id = b.post_id'
            ]);
            $field = 'a.*,b.id AS post_category_id,b.list_order,b.category_id,u.user_login,u.user_nickname,u.user_email';
        }

        $startTime = empty($filter['start_time']) ? 0 : strtotime($filter['start_time']);
        $endTime = empty($filter['end_time']) ? 0 : strtotime($filter['end_time']);
        if (!empty($startTime) && !empty($endTime)) {
            $where['a.published_time'] = [['>= time', $startTime], ['<= time', $endTime]];
        } else {
            if (!empty($startTime)) {
                $where['a.published_time'] = ['>= time', $startTime];
            }
            if (!empty($endTime)) {
                $where['a.published_time'] = ['<= time', $endTime];
            }
        }

        $keyword = empty($filter['keyword']) ? '' : $filter['keyword'];
        if (!empty($keyword)) {
            $where['a.post_title'] = ['like', "%$keyword%"];
        }

        if ($isPage) {
            $where['a.post_type'] = 2;
        } else {
            $where['a.post_type'] = 1;
        }

        $portalPostModel = new PortalPostModel();
        $articles = $portalPostModel->alias('a')->field($field)
                ->join($join)
                ->where($where)
                ->order('update_time', 'DESC')
                ->paginate(10, false, ['path' => as_url($object)]);

        return $articles;
    }

    // $categoryId = '0' 为文本型,为的是可以大批量in值
    public function publishedArticle($postId, $categoryId = '0')
    {
        $portalPostModel = new PortalPostModel();

        if (is_numeric($categoryId) && !empty($categoryId)) {
            $where = [
                'post.post_type' => 1,
                'post.published_time' => [['< time', time()], ['> time', 0]],
                'post.post_status' => 1,
                'post.delete_time' => 0,
                'relation.category_id' => $categoryId,
                'relation.post_id' => $postId
            ];

            $join = [
                ['__PORTAL_CATEGORY_POST__ relation', 'post.id = relation.post_id']
            ];
            $article = $portalPostModel->alias('post')->field('post.*')
                    ->join($join)
                    ->where($where)
//             ->fetchSql(true)
                    ->find();
        } elseif (empty($categoryId)) {
            $where = [
                'post.post_type' => 1,
                'post.published_time' => [['< time', time()], ['> time', 0]],
                'post.post_status' => 1,
                'post.delete_time' => 0,
                'post.id' => $postId
            ];

            $article = $portalPostModel->alias('post')->field('post.*')
                    ->where($where)
//                      ->fetchSql(true)
                    ->find();
        } else {
            $where = [
                'post.post_type' => 1,
                'post.published_time' => [['< time', time()], ['> time', 0]],
                'post.post_status' => 1,
                'post.delete_time' => 0,
                'relation.category_id' => ['in', $categoryId],
                'relation.post_id' => $postId
            ];

            $join = [
                ['__PORTAL_CATEGORY_POST__ relation', 'post.id = relation.post_id']
            ];
            $article = $portalPostModel->alias('post')->field('post.*')
                    ->join($join)
                    ->where($where)
//             ->fetchSql(true)
                    ->find();

        }

//        if (empty($categoryId)) {
//            
//        } else {
//            
//        }
//        var_dump($article);exit;

        return $article;
    }

    //上一篇文章
    public function publishedPrevArticle($postId, $categoryId = '0') {
        $portalPostModel = new PortalPostModel();
        if (is_numeric($categoryId) && !empty($categoryId)) {
            $where = [
                'post.post_type' => 1,
                'post.published_time' => [['< time', time()], ['> time', 0]],
                'post.post_status' => 1,
                'post.delete_time' => 0,
                'relation.category_id' => $categoryId,
                'relation.post_id' => ['<', $postId]
            ];

            $join = [
                ['__PORTAL_CATEGORY_POST__ relation', 'post.id = relation.post_id']
            ];
            $article = $portalPostModel->alias('post')->field('post.*')
                    ->join($join)
                    ->where($where)
                    ->order('id', 'DESC')
                    ->find();
        } elseif (empty($categoryId)) {
            $where = [
                'post.post_type' => 1,
                'post.published_time' => [['< time', time()], ['> time', 0]],
                'post.post_status' => 1,
                'post.delete_time' => 0,
                'post.id ' => ['<', $postId]
            ];

            $article = $portalPostModel->alias('post')->field('post.*')
                    ->where($where)
                    ->order('id', 'DESC')
                    ->find();
        } else {
            $where = [
                'post.post_type' => 1,
                'post.published_time' => [['< time', time()], ['> time', 0]],
                'post.post_status' => 1,
                'post.delete_time' => 0,
                'relation.category_id' => ['in', $categoryId],
                'relation.post_id' => ['<', $postId]
            ];

            $join = [
                ['__PORTAL_CATEGORY_POST__ relation', 'post.id = relation.post_id']
            ];
            $article = $portalPostModel->alias('post')->field('post.*')
                    ->join($join)
                    ->where($where)
                    ->order('id', 'DESC')
                    ->find();
        }


        return $article;
    }

    //下一篇文章
    public function publishedNextArticle($postId, $categoryId = '0') {
        $portalPostModel = new PortalPostModel();
        if (is_numeric($categoryId) && !empty($categoryId)) {
            $where = [
                'post.post_type' => 1,
                'post.published_time' => [['< time', time()], ['> time', 0]],
                'post.post_status' => 1,
                'post.delete_time' => 0,
                'relation.category_id' => $categoryId,
                'relation.post_id' => ['>', $postId]
            ];

            $join = [
                ['__PORTAL_CATEGORY_POST__ relation', 'post.id = relation.post_id']
            ];
            $article = $portalPostModel->alias('post')->field('post.*')
                    ->join($join)
                    ->where($where)
                    ->order('id', 'ASC')
                    ->find();
        } elseif (empty($categoryId)) {
            $where = [
                'post.post_type' => 1,
                'post.published_time' => [['< time', time()], ['> time', 0]],
                'post.post_status' => 1,
                'post.delete_time' => 0,
                'post.id' => ['>', $postId]
            ];

            $article = $portalPostModel->alias('post')->field('post.*')
                    ->where($where)
                    ->order('id', 'ASC')
                    ->find();
        } else {
            $where = [
                'post.post_type' => 1,
                'post.published_time' => [['< time', time()], ['> time', 0]],
                'post.post_status' => 1,
                'post.delete_time' => 0,
                'relation.category_id' => ['in', $categoryId],
                'relation.post_id' => ['>', $postId]
            ];

            $join = [
                ['__PORTAL_CATEGORY_POST__ relation', 'post.id = relation.post_id']
            ];
            $article = $portalPostModel->alias('post')->field('post.*')
                    ->join($join)
                    ->where($where)
                    ->order('id', 'ASC')
                    ->find();
        }

        return $article;
    }

    public function publishedPage($pageId)
    {

        $where = [
            'post_type' => 2,
            'published_time' => [['< time', time()], ['> time', 0]],
            'post_status' => 1,
            'delete_time' => 0,
            'id' => $pageId
        ];

        $portalPostModel = new PortalPostModel();
        $page = $portalPostModel
                ->where($where)
                ->find();

        return $page;
    }

}
