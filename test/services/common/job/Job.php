<?php

namespace common\job;

use think\Config;
use think\Queue;

/**
 * 任务服务
 * 
 * @author chenliming <19536238@qq.com>
 * @since     2018-04-20
 * 
 */
class Job {

    /**
     * 延迟执行任务
     * @param string $job 任务名或任务别名
     * @param mixed $data 参数
     * @param string $queue 使用哪个队列
     * @param int $delay 延迟多长时间后执行
     * @return mixed
     */
    public static function later($job = '', $data = '', $queue = null, $delay = false) {
        if ($delay === FALSE) {
            // 设置随机延迟
            $delay = rand(3, 15);
        }
        $jobs = Config::get('job');
        if (array_key_exists($job, $jobs)) {
            Queue::later($delay, $jobs[$job], $data, $queue);
        } else {
            Queue::later($delay, $job, $data, $queue);
        }
    }

    /**
     * 立即执行任务
     * @param string $job 任务名或任务别名
     * @param mixed $data 参数
     * @param string $queue 使用哪个队列
     * @return mixed
     */
    public static function push($job = '', $data = '', $queue = null) {
        $jobs = Config::get('job');
        if (array_key_exists($job, $jobs)) {
            Queue::push($jobs[$job], $data, $queue);
        } else {
            Queue::push($job, $data, $queue);
        }
    }

}
