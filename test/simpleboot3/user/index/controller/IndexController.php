<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2018 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Powerless < wzxaini9@gmail.com>
// +----------------------------------------------------------------------
namespace user\index\controller;

use common\user\model\UserModel;
use cmf\controller\UserBaseController;

class IndexController extends UserBaseController
{


    /**
     * 前台用户首页(公开)
     */
    public function index()
    {
        $this->checkUserLogin();
        // 注意有权限限制,需要分出来
        $sessionUser = session('user');
        if (!empty($sessionUser)) {
            $id = $sessionUser['id'];
        }else{
            $id = $this->request->param("id", 0, "intval");
        }
        
        $userModel = new UserModel();
        $user      = $userModel->where('id', $id)->find();
        if (empty($user)) {
            $this->error("查无此人！");
        }
        $this->assign($user->toArray());
        $this->assign('user',$user);
        return $this->fetch(":index");
    }

    /**
     * 前台ajax 判断用户登录状态接口
     */
    function isLogin()
    {
        if (cmf_is_user_login()) {
            $this->success("用户已登录",null,['user'=>cmf_get_current_user()]);
        } else {
            $this->error("此用户未登录!");
        }
    }

    /**
     * 退出登录
    */
    public function logout()
    {
        session("user", null);//只有前台用户退出
        return redirect(url('index/login/index'));
//        return redirect($this->request->root() . "/");
    }

}
