<?php

// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2018 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Powerless < wzxaini9@gmail.com>
// +----------------------------------------------------------------------

namespace user\index\controller;

use think\Validate;
use cmf\controller\UserBaseController;
use common\user\model\UserModel;
use anerg\OAuth2\OAuth;
use think\Config;
use common\wechat\service\ApiService as wx;

class LoginController extends UserBaseController {

    /**
     * 登录
     */
    public function index() {
        // 解密数据
        $redirect = '';
        $data = $this->request->param('data', 0);
        if (!empty($data)) {
            $_tmp = json_decode(base64_decode($data), true);
            if (isset($_tmp['redirect']) && !empty($_tmp['redirect'])) {
                $redirect = urldecode($_tmp['redirect']);
            }
        }
        $captcha_no = 0;
        if (empty($redirect)) {
            $redirect = $this->request->post("redirect");
            if (empty($redirect)) {
                $redirect = $this->request->server('HTTP_REFERER');
            } else {
                $redirect = base64_decode($redirect);
            }
        } else {
            $captcha_no = 1;
        }

        session('login_http_referer', $redirect);
        if (cmf_is_user_login()) {
            //已经登录时直接跳到首页
            return redirect(url('index/index/index'));
//            return redirect($this->request->root() . '/');
        } else {
            session('captcha_no', $captcha_no);
            $this->assign('captcha_no', $captcha_no);
            $this->assign('redirect', $redirect);
            return $this->fetch(":login");
        }
    }

    /**
     * 登录验证提交
     */
    public function doLogin() {
        if ($this->request->isPost()) {
            $captcha_no = session('captcha_no');
            if ($captcha_no != true) {
                $validate = new Validate([
                    'captcha' => 'require',
                    'username' => 'require',
                    'password' => 'require|min:6|max:32',
                ]);
                $validate->message([
                    'username.require' => '用户名不能为空',
                    'password.require' => '密码不能为空',
                    'password.max' => '密码不能超过32个字符',
                    'password.min' => '密码不能小于6个字符',
                    'captcha.require' => '验证码不能为空',
                ]);
                $data = $this->request->post();
                if (!$validate->check($data)) {
                    $this->error($validate->getError());
                }

                if (!cmf_captcha_check($data['captcha'])) {
                    $this->error(lang('CAPTCHA_NOT_RIGHT'));
                }
            } else {
                $validate = new Validate([
                    'username' => 'require',
                    'password' => 'require|min:6|max:32',
                ]);
                $validate->message([
                    'username.require' => '用户名不能为空',
                    'password.require' => '密码不能为空',
                    'password.max' => '密码不能超过32个字符',
                    'password.min' => '密码不能小于6个字符',
                ]);
                $data = $this->request->post();
                if (!$validate->check($data)) {
                    $this->error($validate->getError());
                }
            }


            $userModel = new UserModel();
            $user['user_pass'] = $data['password'];
            if (Validate::is($data['username'], 'email')) {
                $user['user_email'] = $data['username'];
                $log = $userModel->doEmail($user);
            } else if (cmf_check_mobile($data['username'])) {
                $user['mobile'] = $data['username'];
                $log = $userModel->doMobile($user);
            } else {
                $user['user_login'] = $data['username'];
                $log = $userModel->doName($user);
            }

            $session_login_http_referer = session('login_http_referer');

            $redirect = empty($session_login_http_referer) ? $this->request->root() : $session_login_http_referer;
//            var_dump($redirect);exit;
            switch ($log) {
                case 0:
                    cmf_user_action('login');
                    $this->success(lang('LOGIN_SUCCESS'), $redirect);
//                    $this->success(lang('LOGIN_SUCCESS'), url('index/index/index'));
                    break;
                case 1:
                    $this->error(lang('PASSWORD_NOT_RIGHT'));
                    break;
                case 2:
                    $this->error('账户不存在');
                    break;
                case 3:
                    $this->error('账号被禁止访问系统');
                    break;
                default :
                    $this->error('未受理的请求');
            }
        } else {
            $this->error("请求错误");
        }
    }

    /**
     * 找回密码
     */
    public function findPassword() {
        return $this->fetch(':find_password');
    }

    /**
     * 用户密码重置
     */
    public function passwordReset() {

        if ($this->request->isPost()) {
            $validate = new Validate([
                'captcha' => 'require',
                'verification_code' => 'require',
                'password' => 'require|min:6|max:32',
            ]);
            $validate->message([
                'verification_code.require' => '验证码不能为空',
                'password.require' => '密码不能为空',
                'password.max' => '密码不能超过32个字符',
                'password.min' => '密码不能小于6个字符',
                'captcha.require' => '验证码不能为空',
            ]);

            $data = $this->request->post();
            if (!$validate->check($data)) {
                $this->error($validate->getError());
            }

            $captchaId = empty($data['_captcha_id']) ? '' : $data['_captcha_id'];
            if (!cmf_captcha_check($data['captcha'], $captchaId)) {
                $this->error('验证码错误');
            }

            $errMsg = cmf_check_verification_code($data['username'], $data['verification_code']);
            if (!empty($errMsg)) {
                $this->error($errMsg);
            }

            $userModel = new UserModel();
            if ($validate::is($data['username'], 'email')) {

                $log = $userModel->emailPasswordReset($data['username'], $data['password']);

            } else if (cmf_check_mobile($data['username'])) {
                $user['mobile'] = $data['username'];
                $log = $userModel->mobilePasswordReset($data['username'], $data['password']);
            } else {
                $log = 2;
            }
            switch ($log) {
                case 0:
                    $this->success('密码重置成功', $this->request->root());
                    break;
                case 1:
                    $this->error("您的账户尚未注册");
                    break;
                case 2:
                    $this->error("您输入的账号格式错误");
                    break;
                default :
                    $this->error('未受理的请求');
            }
        } else {
            $this->error("请求错误");
        }
    }

    /**
     * 此处应当考虑使用空控制器来简化代码
     * 同时应当考虑对第三方渠道名称进行检查
     * $config配置参数应当放在配置文件中
     * callback对应了普通PC版的返回页面和移动版的页面
     */
    public function weixin() {
        $state = $this->request->param('state', 0);
        $config = Config::get('wechat.Web');
        $config['app_id'] = $config['appid'];
        $config['app_secret'] = $config['appsecret'];
        $config['scope'] = 'snsapi_login';

        $config['callback'] = [
            'default' => url('/user#index/Login/callback', ['channel' => 'wx_qrcode'], false, true),
            'mobile' => url('/user#index/Login/callback', ['channel' => 'wx_qrcode'], false, true),
        ];
        $OAuth = OAuth::getInstance($config, 'wx_qrcode');
        $OAuth->setDisplay('mobile'); //此处为可选,若没有设置为mobile,则跳转的授权页面可能不适合手机浏览器访问
        return redirect($OAuth->getAuthorizeURL($state));
    }

    public function callback($channel) {
        $code = $this->request->param('code');
        $state = $this->request->param('state', 0);
        if (!empty($state)) {
            $_tmp = json_decode(base64_decode($state), true);
            if (isset($_tmp['redirect']) && !empty($_tmp['redirect'])) {
                $redirect = urldecode($_tmp['redirect']);
            }
        }

        $config = Config::get('wechat.Web');

        $config['app_id'] = $config['appid'];
        $config['app_secret'] = $config['appsecret'];
        $config['scope'] = 'snsapi_userinfo';
        $config['callback'] = [
            'default' => url('/user#index/Login/callback', ['channel' => 'wx_qrcode'], false, true),
            'mobile' => url('/user#index/Login/callback', ['channel' => 'wx_qrcode'], false, true),
        ];

        $OAuth = OAuth::getInstance($config, 'wx_qrcode');

        $token = $OAuth->getAccessToken();
        $wx = new wx;


        if (is_array($token)) {
            // 更新授权信息
            $wx->up_oauth($token);
            // 主动获取用户信息
            $sns_info = $OAuth->userinfo();
            $wx_info = $wx->get_wx_info($token['openid'], $this->request, '', $sns_info);
            if ($wx_info != FALSE) {
                if (!is_array($wx_info)) {
                    $wx_info = $wx_info->toarray();
                }
                $wx_info['code'] = $code;
                $Login = new Login();
                // 执行第三方登录过程
                $user = $wx_info;

                $ret = $Login->doLogin($user);
            } else {
                $wx_info = $result;
                $wx_info['code'] = $code;
            }

            // session 中记录 user信息
            $sessionUser = session('user');
            if (!empty($sessionUser)) {
                $sessionUser['wechat'] = $wx_info;
            } else {
                $sessionUser = [];
                $sessionUser['wechat'] = $wx_info;
            }
            session('user', $sessionUser);


            if (IS_SWOOLE) {
                return $this->location($redirect);
            } else {
                header("Location:" . $redirect);
                exit();
            }
        }

        /**
         * 在获取access_token的时候可以考虑忽略你传递的state参数
         * 此参数使用cookie保存并验证
         */
//        $ignore_stat = true;
//        $OAuth->getAccessToken(true);

//        var_dump($sns_info);
//        exit;
        /**
         * 此处获取了sns提供的用户数据
         * 你可以进行其他操作
         */
    }

}
