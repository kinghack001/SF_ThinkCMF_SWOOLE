# 管理后台模块 #
----------
## 目录文件列表 ##
- command.php 命令行工具配置文件
- common.php 应用公共(函数)文件
- config.php 应用(公共)配置文件
- database.php 数据库配置文件
- debug.php 开发环境调试控制
- release.php 生产环境调试控制
- route.php 路由配置文件
- tags.php 应用行为扩展定义文件
- index 后台框架主目录,后台框架相关的都放在这里
