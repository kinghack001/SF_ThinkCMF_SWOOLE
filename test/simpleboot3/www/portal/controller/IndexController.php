<?php

// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2018 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 老猫 <thinkcmf@126.com>
// +----------------------------------------------------------------------

namespace www\portal\controller;

use cmf\controller\HomeBaseController;
use common\user\service\LoginService as Login;
use common\wechat\service\ApiService as wx;

class IndexController extends HomeBaseController {

//        public function _initialize() {
//        // 监听home_init
////        hook('home_init');
//            session('user',null);exit;
//        parent::_initialize();
//        
//        }


    public function index() {
        $sessionUser = session('user');
//        $template = $this->parseTemplate(':index');
//        var_dump($template);exit;
//       return $this->buildHtml('index',SA_PUBLIC,':index');
//        exit;
        return $this->fetch(':index');
    }

}
