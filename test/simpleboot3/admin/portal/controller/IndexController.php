<?php

namespace admin\portal\controller;

use cmf\controller\AdminBaseController;

/**
 * Class AdminIndexController
 * @package app\portal\controller
 * @adminMenuRoot(
 *     'name'   =>'门户管理',
 *     'action' =>'default',
 *     'parent' =>'',
 *     'display'=> true,
 *     'order'  => 30,
 *     'icon'   =>'th',
 *     'remark' =>'门户管理'
 * )
 */
class IndexController extends AdminBaseController
{


}
