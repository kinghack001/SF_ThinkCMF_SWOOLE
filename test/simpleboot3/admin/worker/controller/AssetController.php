<?php
namespace admin\worker\controller;

use think\Db;
use cmf\controller\AdminBaseController;

/**
 * 用户资源管理
 * 
 * @author chenliming <19536238@qq.com>
 * @since     2018-05-07
 * 
 */
class AssetController extends AdminBaseController
{
    /**
     * 资源管理列表
     * @adminMenu(
     *     'name'   => '资源管理',
     *     'parent' => '',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => 'file',
     *     'remark' => '资源管理列表',
     *     'param'  => ''
     * )
     */
    public function index()
    {
       $content = hook_one('user_admin_asset_index_view');
        if (!empty($content)) {
            return $content;
        }
        $join   = [
            ['__USER__ u', 'a.user_id = u.id']
        ];
        $result = Db::name('asset')->field('a.*,u.user_login,u.user_email,u.user_nickname')
            ->alias('a')->join($join)
            ->order('create_time', 'DESC')
            ->paginate(10,false,['path'=>as_url($this->request)]);
        $this->assign('assets', $result->items());
        $this->assign('page', $result->render());
        return $this->fetch();
    }

    /**
     * 删除文件
     * @adminMenu(
     *     'name'   => '删除文件',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '删除文件',
     *     'param'  => ''
     * )
     */
    public function delete()
    {
        $id            = $this->request->param('id');
        $file_filePath = Db::name('asset')->where('id', $id)->value('file_path');
        $file          = '../upload/' . $file_filePath;
        $res = true;

        if (file_exists($file)) {
            $res = unlink($file);
        }
        if ($res) {
            Db::name('asset')->where('id', $id)->delete();
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }

}