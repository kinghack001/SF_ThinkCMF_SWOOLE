<?php
namespace admin\worker\controller;

use cmf\controller\AdminBaseController;
use think\Db;

/**
 * Class UserController 管理员信息处理
 * @package admin\worker\controller
 * @adminMenuRoot(
 *     'name'   => '管理组',
 *     'action' => 'default',
 *     'parent' => 'user/AdminIndex/default',
 *     'display'=> true,
 *     'order'  => 10000,
 *     'icon'   => '',
 *     'remark' => '管理组'
 * )
 */
class UserController extends AdminBaseController
{

    /**
     * 管理员列表
     * @adminMenu(
     *     'name'   => '管理员',
     *     'parent' => 'default',
     *     'display'=> true,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '管理员管理',
     *     'param'  => ''
     * )
     */
    public function index()
    {
        $content = hook_one('admin_user_index_view');
        if (!empty($content)) {
            return $content;
        }
        $where = ["user_type" => 1];
        /**搜索条件**/
        $userLogin = $this->request->param('user_login');
        $userEmail = trim($this->request->param('user_email'));

        if ($userLogin) {
            $where['user_login'] = ['like', "%$userLogin%"];
        }

        if ($userEmail) {
            $where['user_email'] = ['like', "%$userEmail%"];;
        }
        $users = Db::name('user')
            ->where($where)
            ->order("id DESC")
            ->paginate(10,false,['path'=>as_url($this->request)]);
        $users->appends(['user_login' => $userLogin, 'user_email' => $userEmail]);
        // 获取分页显示
        $page = $users->render();

        $rolesSrc = Db::name('role')->select();
        $roles    = [];
        foreach ($rolesSrc as $r) {
            $roleId           = $r['id'];
            $roles["$roleId"] = $r;
        }
        $this->assign("page", $page);
        $this->assign("roles", $roles);
        $this->assign("users", $users);
        return $this->fetch();
    }

    /**
     * 管理员添加
     * @adminMenu(
     *     'name'   => '管理员添加',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '管理员添加',
     *     'param'  => ''
     * )
     */
    public function add()
    {
        $content = hook_one('admin_user_add_view');
        if (!empty($content)) {
            return $content;
        }
        $user=  new \common\user\service\ApiService();
        $roles= $user->roles();
//        $roles = Db::name('role')->where(['status' => 1])->order("id DESC")->select();
        $this->assign("roles", $roles);
        return $this->fetch();
    }

    /**
     * 管理员添加提交
     * @adminMenu(
     *     'name'   => '管理员添加提交',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '管理员添加提交',
     *     'param'  => ''
     * )
     */
    public function addPost()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $param['parent_id']=0;
            $user = new \common\user\service\PostService();
            $result = $user->add($param);
            if ($result === FALSE) {
                $this->error($user->getError());
            } else {
                $this->success("添加成功！", url("user/index"));
            }
        }
//        if ($this->request->isPost()) {            
//            if (!empty($_POST['role_id']) && is_array($_POST['role_id'])) {
//                $role_ids = $_POST['role_id'];
//                unset($_POST['role_id']);
//                $result = $this->validate($this->request->param(), 'User');
//                if ($result !== true) {
//                    $this->error($result);
//                } else {
//                    $_POST['user_pass'] = cmf_password($_POST['user_pass']);
//                    $result             = DB::name('user')->insertGetId($_POST);
//                    if ($result !== false) {
//                        //$role_user_model=M("RoleUser");
//                        foreach ($role_ids as $role_id) {
//                            if (cmf_get_current_admin_id() != 1 && $role_id == 1) {
//                                $this->error("为了网站的安全，非网站创建者不可创建超级管理员！");
//                            }
//                            Db::name('RoleUser')->insert(["role_id" => $role_id, "user_id" => $result]);
//                        }
//                        $this->success("添加成功！", url("user/index"));
//                    } else {
//                        $this->error("添加失败！");
//                    }
//                }
//            } else {
//                $this->error("请为此用户指定角色！");
//            }
//
//        }
    }

    /**
     * 管理员编辑
     * @adminMenu(
     *     'name'   => '管理员编辑',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '管理员编辑',
     *     'param'  => ''
     * )
     */
    public function edit()
    {
        $content = hook_one('admin_user_edit_view');
        if (!empty($content)) {
            return $content;
        }
        $id = $this->request->param('id', 0, 'intval');
        $user = new \common\user\service\ApiService();
        $roles = $user->roles();
//        $roles = DB::name('role')->where(['status' => 1])->order("id DESC")->select();
        $this->assign("roles", $roles);
        
        $role_ids = $user->role_ids($id);
//        $role_ids = DB::name('RoleUser')->where(["user_id" => $id])->column("role_id");
        $this->assign("role_ids", $role_ids);

        $user_info = $user->user_info($id);
//        var_dump($user_info);exit;
//        $user = DB::name('user')->where(["id" => $id])->find();
        $this->assign($user_info);
        return $this->fetch();
    }

    /**
     * 管理员编辑提交
     * @adminMenu(
     *     'name'   => '管理员编辑提交',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '管理员编辑提交',
     *     'param'  => ''
     * )
     */
    public function editPost()
    {
        if ($this->request->isPost()) {
            $user = new \common\user\service\PostService();
            $result = $user->editPost($this->request);
            if ($result === FALSE) {
                $this->error($user->getError());
            } else {
                $this->success("保存成功！");
            }
        }
        
//        if ($this->request->isPost()) {
//            if (!empty($_POST['role_id']) && is_array($_POST['role_id'])) {
//                if (empty($_POST['user_pass'])) {
//                    unset($_POST['user_pass']);
//                } else {
//                    $_POST['user_pass'] = cmf_password($_POST['user_pass']);
//                }
//                $role_ids = $this->request->param('role_id/a');
//                unset($_POST['role_id']);
//                $result = $this->validate($this->request->param(), 'User.edit');
//
//                if ($result !== true) {
//                    // 验证失败 输出错误信息
//                    $this->error($result);
//                } else {
//                    $result = DB::name('user')->update($_POST);
//                    if ($result !== false) {
//                        $uid = $this->request->param('id', 0, 'intval');
//                        DB::name("RoleUser")->where(["user_id" => $uid])->delete();
//                        foreach ($role_ids as $role_id) {
//                            if (cmf_get_current_admin_id() != 1 && $role_id == 1) {
//                                $this->error("为了网站的安全，非网站创建者不可创建超级管理员！");
//                            }
//                            DB::name("RoleUser")->insert(["role_id" => $role_id, "user_id" => $uid]);
//                        }
//                        $this->success("保存成功！");
//                    } else {
//                        $this->error("保存失败！");
//                    }
//                }
//            } else {
//                $this->error("请为此用户指定角色！");
//            }
//
//        }
    }

    /**
     * 管理员个人信息修改
     * @adminMenu(
     *     'name'   => '个人信息',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '管理员个人信息修改',
     *     'param'  => ''
     * )
     */
    public function userInfo()
    {
        $id   = cmf_get_current_admin_id();
        $user = Db::name('user')->where(["id" => $id])->find();
        $this->assign($user);
        return $this->fetch();
    }

    /**
     * 管理员个人信息修改提交
     * @adminMenu(
     *     'name'   => '管理员个人信息修改提交',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '管理员个人信息修改提交',
     *     'param'  => ''
     * )
     */
    public function userInfoPost()
    {
        if ($this->request->isPost()) {

            $data             = $this->request->post();
            $data['birthday'] = strtotime($data['birthday']);
            $data['id']       = cmf_get_current_admin_id();
            $create_result    = Db::name('user')->update($data);;
            if ($create_result !== false) {
                $this->success("保存成功！");
            } else {
                $this->error("保存失败！");
            }
        }
    }

    /**
     * 管理员删除
     * @adminMenu(
     *     'name'   => '管理员删除',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '管理员删除',
     *     'param'  => ''
     * )
     */
    public function delete()
    {
        $user = new \common\user\service\PostService();
        $result = $user->delete($this->request);
        if ($result === FALSE) {
            $this->error($user->getError());
        } else {
            $this->success("删除成功！");
        }

//        $id = $this->request->param('id', 0, 'intval');
//        if ($id == 1) {
//            $this->error("最高管理员不能删除！");
//        }
//
//        if (Db::name('user')->delete($id) !== false) {
//            Db::name("RoleUser")->where(["user_id" => $id])->delete();
//            $this->success("删除成功！");
//        } else {
//            $this->error("删除失败！");
//        }
    }

    /**
     * 停用管理员
     * @adminMenu(
     *     'name'   => '停用管理员',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '停用管理员',
     *     'param'  => ''
     * )
     */
    public function ban()
    {
        $id = $this->request->param('id', 0, 'intval');
        if (!empty($id)) {
            $result = Db::name('user')->where(["id" => $id, "user_type" => 1])->setField('user_status', '0');
            if ($result !== false) {
                $this->success("管理员停用成功！", url("user/index"));
            } else {
                $this->error('管理员停用失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    /**
     * 启用管理员
     * @adminMenu(
     *     'name'   => '启用管理员',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '启用管理员',
     *     'param'  => ''
     * )
     */
    public function cancelBan()
    {
        $id = $this->request->param('id', 0, 'intval');
        if (!empty($id)) {
            $result = Db::name('user')->where(["id" => $id, "user_type" => 1])->setField('user_status', '1');
            if ($result !== false) {
                $this->success("管理员启用成功！", url("user/index"));
            } else {
                $this->error('管理员启用失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }
    
    
    /**
     * 密码修改
     * @adminMenu(
     *     'name'   => '密码修改',
     *     'parent' => 'default',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '密码修改',
     *     'param'  => ''
     * )
     */
    public function password()
    {
        return $this->fetch();
    }

    /**
     * 密码修改提交
     * @adminMenu(
     *     'name'   => '密码修改提交',
     *     'parent' => 'password',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '密码修改提交',
     *     'param'  => ''
     * )
     */
    public function passwordPost()
    {
        if ($this->request->isPost()) {

            $data = $this->request->param();
            if (empty($data['old_password'])) {
                $this->error("原始密码不能为空！");
            }
            if (empty($data['password'])) {
                $this->error("新密码不能为空！");
            }

            $userId = cmf_get_current_admin_id();

            $admin = Db::name('user')->where(["id" => $userId])->find();

            $oldPassword = $data['old_password'];
            $password    = $data['password'];
            $rePassword  = $data['re_password'];

            if (cmf_compare_password($oldPassword, $admin['user_pass'])) {
                if ($password == $rePassword) {

                    if (cmf_compare_password($password, $admin['user_pass'])) {
                        $this->error("新密码不能和原始密码相同！");
                    } else {
                        Db::name('user')->where('id', $userId)->update(['user_pass' => cmf_password($password)]);
                        $this->success("密码修改成功！");
                    }
                } else {
                    $this->error("密码输入不一致！");
                }

            } else {
                $this->error("原始密码不正确！");
            }
        }
    }
}