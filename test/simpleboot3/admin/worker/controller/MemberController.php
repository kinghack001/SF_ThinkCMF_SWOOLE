<?php

namespace admin\worker\controller;

use cmf\controller\AdminBaseController;
use think\Db;

/**
 * Class memberController
 * @package worker\member\controller
 *
 * @adminMenuRoot(
 *     'name'   =>'用户管理',
 *     'action' =>'default',
 *     'parent' =>'',
 *     'display'=> true,
 *     'order'  => 10,
 *     'icon'   =>'group',
 *     'remark' =>'用户管理'
 * )
 *
 * @adminMenuRoot(
 *     'name'   =>'用户组',
 *     'action' =>'default1',
 *     'parent' =>'worker/member/default',
 *     'display'=> true,
 *     'order'  => 10000,
 *     'icon'   =>'',
 *     'remark' =>'用户组'
 * )
 */
class MemberController extends AdminBaseController
{

    /**
     * 后台本站用户列表
     * @adminMenu(
     *     'name'   => '本站用户',
     *     'parent' => 'default1',
     *     'display'=> true,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '本站用户',
     *     'param'  => ''
     * )
     */
    public function index()
    {
         $content = hook_one('user_admin_index_view');
        if (!empty($content)) {
            return $content;
        }
        $where   = [];
        $request = input('request.');

        if (!empty($request['uid'])) {
            $where['id'] = intval($request['uid']);
        }
        $keywordComplex = [];
        if (!empty($request['keyword'])) {
            $keyword = $request['keyword'];

            $keywordComplex['user_login|user_nickname|user_email|mobile']    = ['like', "%$keyword%"];
        }
        // 过滤管理员
        $worker=[];
        $worker['user_type']=2;
        $worker['parent_id']=0;
        
        $usersQuery = Db::name('user');

        $list = $usersQuery->whereOr($keywordComplex)->where($where)->where($worker)->order("create_time DESC")->paginate(10,false,['path'=>as_url($this->request)]);
        // 获取分页显示
        $page = $list->render();
        $this->assign('list', $list);
        $this->assign('page', $page);
        // 渲染模板输出
        return $this->fetch();
    }

    /**
     * 本站用户拉黑
     * @adminMenu(
     *     'name'   => '本站用户拉黑',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '本站用户拉黑',
     *     'param'  => ''
     * )
     */
    public function ban()
    {
        $id = input('param.id', 0, 'intval');
        if ($id) {
            $result = Db::name("user")->where(["id" => $id, "user_type" => 2])->setField('user_status', 0);
            if ($result) {
                $this->success("会员拉黑成功！");
            } else {
                $this->error('会员拉黑失败,会员不存在,或者是管理员！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    /**
     * 本站用户启用
     * @adminMenu(
     *     'name'   => '本站用户启用',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '本站用户启用',
     *     'param'  => ''
     * )
     */
    public function cancelBan()
    {
        $id = input('param.id', 0, 'intval');
        if ($id) {
            Db::name("user")->where(["id" => $id, "user_type" => 2])->setField('user_status', 1);
            $this->success("会员启用成功！", '');
        } else {
            $this->error('数据传入失败！');
        }
    }
    
    /**
     * 用户添加
     * @adminMenu(
     *     'name'   => '用户添加',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '用户添加',
     *     'param'  => ''
     * )
     */
    public function add()
    {
        $user=  new \common\user\service\ApiService();
        // 过滤掉后台用户组
        $roles= $user->roles(config('admin_roles'));
        $this->assign("roles", $roles);
        return $this->fetch();
    }
    
  /**
     * 用户添加提交
     * @adminMenu(
     *     'name'   => '用户添加提交',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '用户添加提交',
     *     'param'  => ''
     * )
     */
    public function addPost()
    {
        if ($this->request->isPost()) {
            $param = $this->request->param();
            $param['user_type']=2;
            $user = new \common\user\service\PostService();
            $result = $user->add($param);
            if ($result === FALSE) {
                $this->error($user->getError());
            } else {
                $this->success("添加成功！", url("member/index"));
            }
        }
    }
    

    /**
     * 用户排序
     * @return [type] [description]
     */
    public function listOrder()
    {
        parent::listOrders(Db::name('user'));
        $this->success("排序更新成功！", '');
    }





  /**
     * 管理员编辑
     * @adminMenu(
     *     'name'   => '管理员编辑',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '管理员编辑',
     *     'param'  => ''
     * )
     */
    public function edit()
    {
        $id = $this->request->param('id', 0, 'intval');
        $user = new \common\user\service\ApiService();
        $roles = $user->roles(config('admin_roles'));
        $this->assign("roles", $roles);
        
        $role_ids = $user->role_ids($id);
        $this->assign("role_ids", $role_ids);

        $user_info = $user->user_info($id);
        $this->assign($user_info);
        return $this->fetch();
    }
    
        /**
     * 用户编辑提交
     * @adminMenu(
     *     'name'   => '用户编辑提交',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '用户编辑提交',
     *     'param'  => ''
     * )
     */
    public function editPost() {

        if ($this->request->isPost()) {
            $user = new \common\user\service\PostService();
            $result = $user->editPost($this->request,'memberedit');
            if ($result === FALSE) {
                $this->error($user->getError());
            } else {
                $this->success("保存成功！");
            }
        }
    }
    
    
    /**
     * 用户删除
     * @adminMenu(
     *     'name'   => '管理员删除',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '管理员删除',
     *     'param'  => ''
     * )
     */
    public function delete()
    {
        $user = new \common\user\service\PostService();
        $result = $user->delete($this->request);
        if ($result === FALSE) {
            $this->error($user->getError());
        } else {
            $this->success("删除成功！");
        }
    }

}
