<?php

namespace admin\index\controller;

use cmf\controller\AdminBaseController;
use think\Db;

/**
 * 后台管理中心框架_公共处理方法
 * 
 * @author chenliming <19536238@qq.com>
 * @since     2018-05-05
 * 
 */
class PublicController extends AdminBaseController
{
    public function _initialize()
    {
        hook('admin_init');
    }

    /**
     * 后台登陆界面
     */
    public function login()
    {        
        //debug
//        session("__LOGIN_BY_CMF_ADMIN_PW__", 1);//设置后台登录加密码
        $loginAllowed = session("__LOGIN_BY_CMF_ADMIN_PW__");    
        if (empty($loginAllowed)) {
            $this->error('非法登录!');
            //$this->error('非法登录!', cmf_get_root() . '/');
//            return redirect(cmf_get_root() . "/");
        }

        $admin_id = session('ADMIN_ID');
        if (!empty($admin_id)) {//已经登录
            return redirects('index/index/index');
        } else {
            $site_admin_url_password = config("cmf_SITE_ADMIN_URL_PASSWORD");
            $upw                     = session("__CMF_UPW__");
            // 密码不一致,重新登录
            if (!empty($site_admin_url_password) && $upw != $site_admin_url_password) {
               // return redirect(cmf_get_root() . "/");
                //
                return redirect(config('boot_path'));
            } else {
                session("__SP_ADMIN_LOGIN_PAGE_SHOWED_SUCCESS__", true);
                // 加入钩子,可以在登录页做提前处理,截断
                $result = hook_one('admin_login');
                if (!empty($result)) {
                    return $result;
                }
                //var_dump(111);exit;
                return $this->fetch(":login");
            }
        }
    }

    /**
     * 登录验证
     */
    public function doLogin()
    {
        if (hook_one('admin_custom_login_open')) {
            $this->error('您已经通过插件自定义后台登录！');
        }
        $loginAllowed = session("__LOGIN_BY_CMF_ADMIN_PW__");
        if (empty($loginAllowed)) {
            //$this->error('非法登录!', cmf_get_root() . '/');
            $this->error('非法登录!', config('boot_path'));
        }
        // 核对验证码
        $captcha = $this->request->param('captcha');
        if (empty($captcha)) {
            $this->error(lang('CAPTCHA_REQUIRED'));
        }
        //验证码
        if (!cmf_captcha_check($captcha)) {
            $this->error(lang('CAPTCHA_NOT_RIGHT'));
        }

        // 核对账户密码
        $name = $this->request->param("username");
        if (empty($name)) {
            $this->error(lang('USERNAME_OR_EMAIL_EMPTY'));
        }
        $pass = $this->request->param("password");
        
        if (empty($pass)) {
            $this->error(lang('PASSWORD_REQUIRED'));
        }
       
        // 定位登录方式
        if (strpos($name, "@") > 0) {//邮箱登陆
            $where['user_email'] = $name;
        } else {
            $where['user_login'] = $name;
        }
        
        // 为保持和cmf接近,这块保持不变
        $result = Db::name('user')->where($where)->find();

        if (!empty($result) && $result['user_type'] == 1) {
            if (cmf_compare_password($pass, $result['user_pass'])) {
                $groups = Db::name('RoleUser')
                    ->alias("a")
                    ->join('__ROLE__ b', 'a.role_id =b.id')
                    ->where(["user_id" => $result["id"], "status" => 1])
//                    ->fetchSql(true)
                    ->value("role_id");

                // 非超级管理员账号,都可以被禁用
                // 没有分组,状态禁用,都不能登录
                if ($result["id"] != 1 && (empty($groups) || empty($result['user_status']))) {
                    $this->error(lang('USE_DISABLED'));
                }
                // 需要记录下.是从哪登录的.登录白名单,后期加
                // 登入成功页面跳转,这个是识别登录的关键点,注意这个地方需要特殊区分不然session会穿透
                session('ADMIN_ID', $result["id"]);
                session('name', $result["user_login"]);
                $result['last_login_ip']   = get_client_ip(0, true);
                $result['last_login_time'] = time();
                $token                     = cmf_generate_user_token($result["id"], 'web');
                if (!empty($token)) {
                    session('token', $token);
                }
                Db::name('user')->update($result);
                // 保存30天登录记录
                cookie("admin_username", $name, 3600 * 24 * 30);
                // 再次登录时,强制登录.
                session("__LOGIN_BY_CMF_ADMIN_PW__", null);
                $this->success(lang('LOGIN_SUCCESS'), url("index/index/index"));
            } else {
                $this->error(lang('PASSWORD_NOT_RIGHT'));
            }
        } else {
            $this->error(lang('USERNAME_NOT_EXIST'));
        }
    }

    /**
     * 后台管理员退出
     */
    public function logout()
    {
        session('ADMIN_ID', null);
//        return redirect(url('/', [], false, true));
        return redirects('index/index/index');
    }
}