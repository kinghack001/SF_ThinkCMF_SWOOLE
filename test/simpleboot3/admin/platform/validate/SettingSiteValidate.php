<?php

namespace admin\platform\validate;

use common\platform\model\RouteModel;
use think\Validate;

/**
 * 设置页对应的验证规则
 * 
 * @author chenliming <19536238@qq.com>
 * @since     2018-05-05
 * 
 */
class SettingSiteValidate extends Validate {

    protected $rule = [
        'options.site_name' => 'require',
        'admin_settings.admin_password' => 'alphaNum|checkAlias'
    ];
    protected $message = [
        'options.site_name.require' => '网站名称不能为空',
        'admin_settings.admin_password.alphaNum' => '后台加密码只能是英文字母和数字',
        'admin_settings.admin_password.checkAlias' => '此加密码不能使用!',
    ];

    /**
     * 自定义验证规则
     * @param string $value 输入参数
     * @param string $rule 检测路由(格式数据)
     * @param string $data 检测数据(格式数据)
     * @return mixed
     */
    protected function checkAlias($value, $rule, $data) {
        if (empty($value)) {
            return true;
        }

        if (preg_match('/^\d+$/', $value)) {
            return "加密码不能是纯数字！";
        }

        $routeModel = new RouteModel();
        $fullUrl = $routeModel->buildFullUrl('index/Index/index', []);
        if (!$routeModel->exists($value . '$', $fullUrl)) {
            return true;
        } else {
            return "URL规则已经存在,无法设置此加密码!";
        }
    }

}
