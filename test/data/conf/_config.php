<?php

// 对不同类型的项目进行缓存策略选择
switch (CACHE_TYPE) {
    case 'complex':
        if (!extension_loaded('redis')) {
            if (IS_SWOOLE) {
                return 'not redis!';
            } else {
                die('not redis!');
            }
        }
        // +----------------------------------------------------------------------
        // | 缓存设置
        // +----------------------------------------------------------------------
        $configs_cache = [
            'cache' => [
                // 使用复合缓存类型
                'type' => 'complex',
                // 默认使用的缓存
                'default' => [
                    // 驱动方式
                    'type' => 'redis',
                    // 缓存保存目录
                    'path' => CACHE_PATH,
                ],
                // 文件缓存
                'file' => [
                    // 驱动方式
                    'type' => 'file',
                    // 设置不同的缓存保存目录
                    'path' => RUNTIME_PATH . 'file/',
                    // 缓存前缀
                    'prefix' => APP_PREFIX . '_',
                    // 缓存有效期 0表示永久缓存
                    'expire' => 0,
                ],
                // redis缓存
                'redis' => [
                    // 驱动方式
                    'type' => 'redis',
                    // 服务器地址
                    'host' => '127.0.0.1',
                    // 缓存前缀
                    'prefix' => APP_PREFIX . '_',
                ],
                // memcache缓存
                'memcache' => [
                    // 驱动方式
                    'type' => 'memcache',
                    // 服务器地址
                    'host' => '127.0.0.1',
                    // 缓存前缀
                    'prefix' => APP_PREFIX . '_',
                ],
            ],
            // +----------------------------------------------------------------------
            // | 会话设置
            // +----------------------------------------------------------------------
            'session' => [
                'id' => '',
                // SESSION_ID的提交变量,解决flash上传跨域
                'var_session_id' => '',
                // SESSION 前缀
                'prefix' => APP_PREFIX . '_',
                // 是否自动开启 SESSION
                'auto_start' => true,
                // 驱动方式 支持redis memcache memcached
                'type' => 'redis',
                'auto_start' => true,
                // redis主机
                'host' => '127.0.0.1',
            ],
        ];

        break;

    default:
        $configs_cache = [
            // +----------------------------------------------------------------------
            // | 缓存设置
            // +----------------------------------------------------------------------
            'cache' => [
                // 驱动方式
                'type' => 'File',
                // 缓存保存目录
                'path' => CACHE_PATH,
                // 缓存前缀
                'prefix' => APP_PREFIX . '_',
                // 缓存有效期 0表示永久缓存
                'expire' => 0,
            ],
            // +----------------------------------------------------------------------
            // | 会话设置
            // +----------------------------------------------------------------------
            'session' => [
                'id' => '',
                // SESSION_ID的提交变量,解决flash上传跨域
                'var_session_id' => '',
                // SESSION 前缀
                'prefix' => APP_PREFIX . '_',
                // 驱动方式 支持redis memcache memcached
                'type' => '',
                // 是否自动开启 SESSION
                'auto_start' => true,
            ],
        ];
        break;
}

$configs = [
    // +----------------------------------------------------------------------
    // | 应用设置
    // +----------------------------------------------------------------------
    // 应用模式状态
    'app_status' => APP_DEBUG ? 'debug' : 'release',
    // 是否支持多模块
    'app_multi_module' => true,
    // 入口自动绑定模块
    'auto_bind_module' => false,
    // 注册的根命名空间
    // 利用根命名空间可以把功能解耦,在使用的时候,可以减少代码重复率
    'root_namespace' => [
        'cmf' => CMF_PATH,
        'plugins' => PLUGINS_PATH,
        'themes' => SA_PUBLIC . 'themes/',
        // 通用业务/不同项目都会用到的
        'common' => APP_COMMON,
        // 公共专属业务
        'app' =>APP_APP,
    ],
    // 扩展函数文件
    'extra_file_list' => [CMF_PATH . 'common' . EXT, THINK_PATH . 'helper' . EXT],
    // 默认输出类型
    'default_return_type' => 'html',
    // 默认AJAX 数据返回格式,可选json xml ...
    'default_ajax_return' => 'json',
    // 默认JSONP格式返回的处理方法
    'default_jsonp_handler' => 'jsonpReturn',
    // 默认JSONP处理方法
    'var_jsonp_handler' => 'callback',
    // 默认时区
    'default_timezone' => 'PRC',
    // 是否开启多语言
    'lang_switch_on' => false,
    // 默认全局过滤方法 用逗号分隔多个
    'default_filter' => 'htmlspecialchars',
    // 默认语言
    'default_lang' => 'zh-cn',
    // 应用类库后缀 强制区分
    'class_suffix' => true,
    // 控制器类后缀 强制区分
    'controller_suffix' => true,
    // 默认访问方式
    'protocol_http' =>true, // 需要启用多协议方式设置为false
    // +----------------------------------------------------------------------
    // | 模块设置
    // +----------------------------------------------------------------------
    // 默认模块名
    'default_module' => 'index',
    // 禁止访问模块
    'deny_module_list' => ['common'],
    // 默认控制器名
    'default_controller' => 'Index',
    // 默认操作名
    'default_action' => 'index',
    // 默认验证器
    'default_validate' => '',
    // 默认的空控制器名
    'empty_controller' => 'Error',
    // 操作方法后缀
    'action_suffix' => '',
    // 自动搜索控制器
    'controller_auto_search' => false,
    // +----------------------------------------------------------------------
    // | URL设置
    // +----------------------------------------------------------------------
    // PATHINFO变量名 用于兼容模式
    'var_pathinfo' => 's',
    // 兼容PATH_INFO获取
    'pathinfo_fetch' => ['ORIG_PATH_INFO', 'REDIRECT_PATH_INFO', 'REDIRECT_URL'],
    // pathinfo分隔符
    'pathinfo_depr' => '/',
    // URL伪静态后缀
    'url_html_suffix' => false,
    // URL普通方式参数 用于自动生成
    'url_common_param' => true,
    // URL参数方式 0 按名称成对解析 1 按顺序解析
    'url_param_type' => 0,
    // 是否开启路由
    'url_route_on' => true,
    // 路由使用完整匹配
    'route_complete_match' => false,
    // 路由配置文件（支持配置多个）
    'route_config_file' => ['route'],
    // 是否强制使用路由
    'url_route_must' => false,
    // 域名部署
    'url_domain_deploy' => false,
    // 域名根，如thinkphp.cn
    'url_domain_root' => '',
    // 是否自动转换URL中的控制器和操作名
    'url_convert' => true,
    // 默认的访问控制器层
    'url_controller_layer' => 'controller',
    // 表单请求类型伪装变量
    'var_method' => '_method',
    // 表单ajax伪装变量
    'var_ajax' => '_ajax',
    // 表单pjax伪装变量
    'var_pjax' => '_pjax',
    // 是否开启请求缓存 true自动缓存 支持设置请求缓存规则
    'request_cache' => false,
    // 请求缓存有效期
    'request_cache_expire' => null,
    // 全局请求缓存排除规则
    'request_cache_except' => [],
    'var_depr' => '&', //TP默认是?
    // +----------------------------------------------------------------------
    // | 模板设置
    // +----------------------------------------------------------------------
    'template' => [
        // 模板引擎类型 支持 php think 支持扩展
        'type' => 'Think',
        // 默认模板渲染规则 1 解析为小写+下划线 2 全部转换小写
        'auto_rule'    => 1,
        // 视图根目录
        'view_base' => '',
        // 模板路径
        'view_path' => '',
        // 模板后缀
        'view_suffix' => 'html',
        // 模板文件名分隔符
        'view_depr' => DS,
        // 模板引擎普通标签开始标记
        'tpl_begin' => '{',
        // 模板引擎普通标签结束标记
        'tpl_end' => '}',
        // 标签库标签开始标记
        'taglib_begin' => '<',
        // 标签库标签结束标记
        'taglib_end' => '>',
        'taglib_build_in' => 'cmf\lib\taglib\Cmf,cx',
        'tpl_cache' => APP_DEBUG ? false : true,
        'tpl_deny_php' => false,
    ],
    // 视图输出字符串内容替换
    'view_replace_str' => [],
    // 默认跳转页面对应的模板文件
    'dispatch_success_tmpl' => THINK_PATH . 'tpl' . DS . 'dispatch_jump.tpl',
    'dispatch_error_tmpl' => THINK_PATH . 'tpl' . DS . 'dispatch_jump.tpl',
    // +----------------------------------------------------------------------
    // | 异常及错误设置
    // +----------------------------------------------------------------------
    // 异常页面的模板文件
    'exception_tmpl' => THINK_PATH . 'tpl' . DS . 'think_exception.tpl',
    // 错误显示信息,非调试模式有效
    'error_message' => '页面错误！请稍后再试～',
    // 显示错误信息
    'show_error_msg' => false,
    // 异常处理handle类 留空使用 \think\exception\Handle
    'exception_handle' => '',
    'http_exception_template' => [
        // 定义404错误的重定向页面地址
        // 404 =>  APP_PATH.'404.html',
        404 => 'index/index/_empty',
    ],
    // +----------------------------------------------------------------------
    // | 日志设置
    // +----------------------------------------------------------------------
    'log' => [
        // 日志记录方式，内置 file socket 支持扩展
        'type' => 'File',
        // 日志保存目录
        'path' => LOG_PATH,
        // 日志记录级别
        'level' => [],
    ],
    // +----------------------------------------------------------------------
    // | Trace设置 开启 app_trace 后 有效
    // +----------------------------------------------------------------------
    'trace' => [
        // 内置Html Console 支持扩展
        'type' => 'Html',
    ],
    // +----------------------------------------------------------------------
    // | Cookie设置
    // +----------------------------------------------------------------------
    'cookie' => [
        // cookie 名称前缀
        'prefix' => '',
        // cookie 保存时间
        'expire' => 0,
        // cookie 保存路径
        'path' => '/',
        // cookie 有效域名
        'domain' => '',
        //  cookie 启用安全传输
        'secure' => false,
        // httponly设置
        'httponly' => '',
        // 是否使用 setcookie
        'setcookie' => true,
    ],
    // +----------------------------------------------------------------------
    // | 数据库设置
    // +----------------------------------------------------------------------
    'database' => [
        // 数据库调试模式
        'debug' => true,
        // 数据集返回类型
        'resultset_type' => 'collection',
        // 自动写入时间戳字段
        'auto_timestamp' => true,
        // 时间字段取出后的默认时间格式
        'datetime_format' => true,
        // 是否需要进行SQL性能分析
        'sql_explain' => false,
    ],
    //分页配置
    'paginate' => [
        'type' => '\cmf\paginator\Bootstrap',
        'var_page' => 'page',
        'list_rows' => 15,
    ],
        // 'queue'                   => [
        //     'connector' => '\cmf\queue\connector\Database'
        // ],
        // +----------------------------------------------------------------------
        // | CMF 设置
        // +----------------------------------------------------------------------
        // +----------------------------------------------------------------------
        // | 自定义 设置
        // +----------------------------------------------------------------------
];
return array_merge($configs_cache, $configs);
