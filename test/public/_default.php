<?php
//定义swoole模式
defined('IS_SWOOLE') or define("IS_SWOOLE", FALSE);
if (IS_SWOOLE) {
    if (version_compare(PHP_VERSION, '7.1.0', '<'))
        return 'require PHP > 7.1.0 !';
}else {
    if (version_compare(PHP_VERSION, '7.1.0', '<'))
        die('require PHP > 7.1.0 !');
}
if (!defined('CMF_ROOT')) {
    if (IS_SWOOLE) {
        return 'hacking attempt!';
    } else {
        die('hacking attempt!');
    }
}
// 环境常量
if (IS_SWOOLE) {
    defined('IS_CLI') or define('IS_CLI', false);
} else {
    defined('IS_CLI') or define('IS_CLI', PHP_SAPI == 'cli' ? true : false);
}

// 调试模式开关
defined('APP_DEBUG') or define("APP_DEBUG", true);

// 上传目录
defined('SA_UPLOAD') or define('SA_UPLOAD', SA_PUBLIC . '/upload/');
defined('SA_STATIC') or define('SA_STATIC', SA_PUBLIC . 'static/');

// 定义应用目录
// 定义一个应用存放目录,所有应用都存在此目录下.
// 婚纱hunsha thinkcmf官方版simpleboot3 主开发版本
switch (APP_PREFIX) {
    case 'simpleboot3':
        defined('SA_APP_PATH') or define('SA_APP_PATH', 'simpleboot3/');
        break;
    default:
        break;
}
defined('APP_PATH') or define('APP_PATH', CMF_ROOT . SA_APP_PATH . APP_NAME . '/');


//必须强制初始命名空间,否在不会载入自定义的目录
defined('APP_NAMESPACE') or define('APP_NAMESPACE', APP_NAME);
// 定义入口文件
defined('APP_ROOT') or define('APP_ROOT', 'index.php');
// 二级域名部署
defined('IS_SLD') or define('IS_SLD', false);

// 定义系统业务公共目录,业务层内容均放在此处.
defined('SA_SERVICES_PATH') or define('SA_SERVICES_PATH', 'services/');
// 框架公共功能模块 框架级别的底层公共功能
defined('APP_COMMON') or define('APP_COMMON', CMF_ROOT . SA_SERVICES_PATH. 'common/');
// 项目公共功能模块 不涉及控制层的公共功能,注意这模块可以随着不同项目改动
switch (APP_PREFIX) {
    case 'simpleboot3':
        defined('APP_APP') or define('APP_APP', 'simpleboot3/');
        break;
    default:
        defined('APP_APP') or define('APP_APP', CMF_ROOT . SA_SERVICES_PATH. 'app/');
        break;
}


//定义框架名称
defined('FRAMEWORK_NAME') or define('FRAMEWORK_NAME', 'swooleamoeba');
// 定义CMF核心包目录
defined('CMF_PATH') or define('CMF_PATH', CMF_ROOT . FRAMEWORK_NAME . '/cmf/');


// 定义插件目录
defined('PLUGINS_PATH') or define('PLUGINS_PATH', SA_PUBLIC . '/plugins/');

// 定义扩展目录
defined('EXTEND_PATH') or define('EXTEND_PATH', CMF_ROOT . FRAMEWORK_NAME . '/extend/');
defined('VENDOR_PATH') or define('VENDOR_PATH', CMF_ROOT . FRAMEWORK_NAME . '/vendor/');

// 定义应用的运行时目录
if (IS_SWOOLE) {
    defined('RUNTIME_PATH') or define('RUNTIME_PATH', CMF_ROOT . 'data/runtime_swoole/' . APP_NAME . '/');
} elseif (IS_CLI) {
    defined('RUNTIME_PATH') or define('RUNTIME_PATH', CMF_ROOT . 'data/runtime_cli/' . APP_NAME . '/');
} else {
    defined('RUNTIME_PATH') or define('RUNTIME_PATH', CMF_ROOT . 'data/runtime/' . APP_NAME . '/');
}

// 定义CMF 版本号
defined('THINKCMF_VERSION') or define('THINKCMF_VERSION', '5.0.180901');

// 定义SwooleAmoeba 版本号
defined('FRAMEWORK_VERSION') or define('FRAMEWORK_VERSION', '0.2.1800906');

// 定义缓存方式 为空时为文件方式
defined('CACHE_TYPE') or define("CACHE_TYPE", 'complex');

