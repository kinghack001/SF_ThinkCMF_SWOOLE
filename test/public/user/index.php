<?php
// [ 入口文件 ]
// 需要判断是二级目录还是根目录
defined('IS_ROOT') or define('IS_ROOT', '../');
if (!empty(IS_ROOT)) {
    // 定义CMF根目录,可更改此目录
    defined('CMF_ROOT') or define('CMF_ROOT', __DIR__ . '/../' . IS_ROOT);
    // 定义静态文件所在目录,同时也是网站的根目录
    defined('SA_PUBLIC') or define('SA_PUBLIC', __DIR__ . '/' . IS_ROOT);
} else {
    // 定义CMF根目录,可更改此目录
    defined('CMF_ROOT') or define('CMF_ROOT', __DIR__ .  '/../');
    // 定义静态文件所在目录,同时也是网站的根目录
    defined('SA_PUBLIC') or define('SA_PUBLIC', __DIR__ . '/');
}

// 定义应用目录
// 如何判断应该载入哪个应用目录 默认app
defined('APP_NAME') or define('APP_NAME', 'user');
// 不同项目此处需要改动 yunjing720  hunsha
defined('APP_PREFIX') or define('APP_PREFIX', 'yunjing720');
// 定位网站根目录用,web访问时用
defined('SA_WEB_ROOT') or define('SA_WEB_ROOT', '');



//加载公共配置文件
require_once SA_PUBLIC . '/_default.php';
require_once SA_PUBLIC . '/_app_default.php';


// 加载框架基础文件
require_once CMF_ROOT . FRAMEWORK_NAME . '/thinkphp/base.php';

// 执行应用
if (IS_SWOOLE) {
    \think\App::run(null, $response)->send();
} else {
    \think\App::run()->send();
}

