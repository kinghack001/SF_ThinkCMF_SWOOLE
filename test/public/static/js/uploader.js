function upolader(data,callback){

	var uploader = WebUploader.create({
	    auto: true, // 选完文件后，是否自动上传 
	    swf: '/themes/user_yunjing720/public/assets/js/Uploader.swf', // swf文件路径 
	    server:data.successUrl, // 文件接收服务端 
	    duplicate :true,
	     pick:{
	    	id:data.id,// 选择文件的按钮。可选
	    	multiple:false
	    }, 
	    formData:{
	    	userId:data.userId,
	    	_ajax:1,
	    	app:data['app'],
			filetype:data.type
	    },
	    method:"POST",
	    // 只允许选择图片文件。 
	    accept: {
	       title: 'Images',
	       extensions: data.upoladerType,
	    },
	    thumb: {
	       // type: 'image/jpg,jpeg,png'
	    },
	});

	uploader.on('uploadSuccess',function(file,res){
	 // $(data.setElement).attr("src","{:cmf_get_user_avatar_url('"+res.data.preview_url+"')}");
	  if(res.code){

	  	if(data.type ==='image'){

            $(".preview_url_img").remove();

		  	$(data.id).after('<div class="preview_url_img"><span style="display:block;">'+file.name+'</span><img  src="'+res.data.preview_url+'" /></div>');

		  	$(".preview_url_img img").css({"height":"200px"});

	  	}

	  	if(data.type ==='audio'){

            $(".preview_url_audio").remove();

		  	$(data.id).after('<div class="preview_url_audio"><span style="display:block;">'+file.name+'</span><audio controls  src="'+res.data.preview_url+'" /></audio></div>');

		  	$(".preview_url_audio").css({});

	  	}

	  	if(data.type ==='video'){

            $(".preview_url_video").remove();

		  	$(data.id).after('<div class="preview_url_video"><span style="display:block;">'+file.name+'</span><video controls  src="'+res.data.preview_url+'" ></video></div>');

		  	$(".preview_url_video").css({});

	  	}

	  	

	  	// $(data.setElement).attr("src",res.data.preview_url);
        var uploadSrc = res.data.preview_url;
			// .split("/");
         
        // uploadSrc = uploadSrc.slice(1).join("/");

        callback({
        	url:uploadSrc,
        	name:file.name
        });

	  }

	});


	uploader.on( 'fileQueued', function( file ) {

    uploader.md5File( file )

        .progress(function(percentage) {
            // console.log();
        })

        // 完成
        .then(function(val) {
           
               $.get(data.mad5Url,{
               	_ajax:1,
				md5:val
               },function(data){

               })

        });

	})
}
