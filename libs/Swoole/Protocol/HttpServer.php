<?php
namespace Swoole\Protocol;

use Swoole;
use Swoole\Sessions;

/**
 * HTTP Server
 * @author Tianfeng.Han
 * @link http://www.swoole.com/
 * @package Swoole
 * @subpackage net.protocol
 */
class HttpServer extends Swoole\Protocol\WebServer implements  Swoole\IFace\Protocol
{
    protected $swoole_server;
    protected $buffer_header = array();
    protected $buffer_maxlen = 65535; //最大POST尺寸，超过将写文件

    const DATE_FORMAT_HTTP = 'D, d-M-Y H:i:s T';

    const HTTP_EOF = "\r\n\r\n";
    const HTTP_HEAD_MAXLEN = 8192; //http头最大长度不得超过2k

    const ST_FINISH = 1; //完成，进入处理流程
    const ST_WAIT   = 2; //等待数据
    const ST_ERROR  = 3; //错误，丢弃此包

    function __construct($config = array())
    {
        parent::__construct($config);
        // 文件响应类型
        $mimes = require(LIBPATH . '/data/mimes.php');
        $this->mime_types = array_flip($mimes);
        $this->config = $config;
        // http解析类
        $this->parser = new Swoole\Http\Parser;
    }

    function onStart($serv, $worker_id = 0)
    {
        if (!defined('WEBROOT'))
        {
            define('WEBROOT', $this->config['server']['webroot']);
        }

        if (isset($this->config['server']['user']))
        {
            Swoole\Console::changeUser($this->config['server']['user']);
        }

        Swoole\Error::$echo_html = true;
        $this->swoole_server = $serv;
        Swoole::$php->server = $this;
        // 这个地方会回显到命令行,想好看,就需要这块改.
        $this->log(self::SOFTWARE . "[#{$worker_id}]. running. on {$this->server->host}:{$this->server->port}");
        set_error_handler(array($this, 'onErrorHandle'), E_USER_ERROR);
        register_shutdown_function(array($this, 'onErrorShutDown'));
    }

    /**
     * @return \swoole_server
     */
    function getSwooleServer()
    {
        return $this->swoole_server;
    }

    function onShutdown($serv)
    {
        $this->log(self::SOFTWARE . " shutdown");
    }

    function onConnect($serv, $client_id, $from_id)
    {
        $this->log("Event: client[#$client_id@$from_id] connect");
    }


    function onClose($serv, $client_id, $from_id)
    {
        $this->log("Event: client[#$client_id@$from_id] close");
        $this->cleanBuffer($client_id);
    }

    function cleanBuffer($fd)
    {
        unset($this->requests[$fd], $this->buffer_header[$fd]);
    }

    /**
     * 检测请求头
     * @param $client_id
     * @param $http_data
     * @return bool|Swoole\Request
     */
    function checkHeader($client_id, $http_data)
    {
        //新的连接
        //注意post,get第一包都是一样的.但post会存在多包的情况.当后面的包出现时,可以认为是body数据.这部分数据需要合并
        if (!isset($this->requests[$client_id]))
        {
            if (!empty($this->buffer_header[$client_id]))
            {
                $http_data = $this->buffer_header[$client_id].$http_data;
            }
            //主要处理post方式,判断当前请求包是否完整
            //HTTP结束符
            $ret = strpos($http_data, self::HTTP_EOF);
            //没有找到EOF，继续等待数据
            if ($ret === false)
            {
                return false;
            }
            else
            {
                $this->buffer_header[$client_id] = '';
                $request = new Swoole\Request;
                //GET没有body
                //分解请求头.
                list($header, $request->body) = explode(self::HTTP_EOF, $http_data, 2);
                $request->header = $this->parser->parseHeader($header);
                //使用head[0]保存额外的信息
                $request->meta = $request->header[0];
                unset($request->header[0]);
                //保存请求
                $this->requests[$client_id] = $request;
                //解析失败
                if ($request->header == false)
                {
                    $this->log("parseHeader failed. header=".$header);
                    return false;
                }
            }
        }
        //POST请求需要合并数据
        //利用数据检查的异步模式,把封包合并起来.
        else
        {
            $request = $this->requests[$client_id];
            $request->body .= $http_data;
        }
        return $request;
    }

    /**
     * POST请求需要检测body是否完整
     * @param Swoole\Request $request
     * @return int
     */
    function checkPost(Swoole\Request $request)
    {
        if (isset($request->header['Content-Length']))
        {
            //超过最大尺寸
            if (intval($request->header['Content-Length']) > $this->config['server']['post_maxsize'])
            {
                $this->log("checkPost failed. post_data is too long.");
                return self::ST_ERROR;
            }
            //不完整，继续等待数据
            if (intval($request->header['Content-Length']) > strlen($request->body))
            {
                return self::ST_WAIT;
            }
            //长度正确
            else
            {
                return self::ST_FINISH;
            }
        }
        $this->log("checkPost fail. Not have Content-Length.");
        //POST请求没有Content-Length，丢弃此请求
        return self::ST_ERROR;
    }

    /**
     * 检测request data完整性,这个地方要区分,请求进来的当前客户端,这个地方是数组
     */
    function checkData($client_id, $http_data)
    {
        //不是很明白这段,好像是要强行构造一个数据包,用于区分,貌似是多包的时候,会用到.
        if (isset($this->buffer_header[$client_id]))
        {
            $http_data = $this->buffer_header[$client_id].$http_data;
        }
        //检测头
        //用这部分,强行把数据包进行合并,注意返回的是个数组
        $request = $this->checkHeader($client_id, $http_data);
        //错误的http头
        if ($request === false)
        {
            $this->buffer_header[$client_id] = $http_data;
            //超过最大HTTP头限制了
            if (strlen($http_data) > self::HTTP_HEAD_MAXLEN)
            {
                $this->log("http header is too long.");
                return self::ST_ERROR;
            }
            else
            {
                $this->log("wait request data. fd={$client_id}");
                return self::ST_WAIT;
            }
        }
        //POST请求需要检测body是否完整
        if ($request->meta['method'] == 'POST')
        {
            return $this->checkPost($request);
        }
        //GET请求直接进入处理流程
        else
        {
            return self::ST_FINISH;
        }
    }

    /**
     * 接收到数据,重要处理,关键位置,调用的内置方法,从接口声明处继承来的
     * @param $serv \swoole_server
     * @param $client_id
     * @param $from_id
     * @param $data
     * @return null
     */
    function onReceive($serv, $client_id, $from_id, $data)
    {
        //检测request data完整性,接受请求第一步需要处理的事情
        $ret = $this->checkData($client_id, $data);
        switch($ret)
        {
            //错误的请求
            case self::ST_ERROR;
                $this->server->close($client_id);
                return;
            //请求不完整，继续等待
            case self::ST_WAIT:
                return;
            default:
                break;
        }
        //完整的请求
        //开始处理

        /**
         * @var $request Swoole\Request
         */
        $request = $this->requests[$client_id];

        $request->fd = $client_id;
        $request->time = time();

        /**
         * Socket连接信息
         */
        $info = $serv->connection_info($client_id);
        $request->server['SWOOLE_CONNECTION_INFO'] = $info;
        $request->remote_ip = $info['remote_ip'];
        $request->remote_port = $info['remote_port'];
        /**
         * Server变量
         */
        $request->server['REQUEST_URI'] = $request->meta['uri'];
        $request->server['REMOTE_ADDR'] = $request->remote_ip;
        $request->server['REMOTE_PORT'] = $request->remote_port;
        $request->server['REQUEST_METHOD'] = $request->meta['method'];
        $request->server['REQUEST_TIME'] = $request->time;
        $request->server['SERVER_PROTOCOL'] = $request->meta['protocol'];
		$request->server['SERVER_SOFTWARE'] = SERVER_NAME;
        if (!empty($request->meta['query']))
        {
            $_SERVER['QUERY_STRING'] = $request->meta['query'];
        }


        //解析请求
        $this->parseRequest($request);
        $this->currentRequest = $request;

        /**
         * 补充Server变量
         */
        // 默认访问时,这个地方 path 会没有路径
        $request->server['SCRIPT_FILENAME']=$this->document_root.$request->meta['path'];
        $request->server['SCRIPT_NAME']=$request->meta['path'];
        
        if ($request->meta['path'][strlen($request->meta['path']) - 1] == '/')
        {
            $request->meta['path'] .= $this->config['request']['default_page'];
            $request->server['SCRIPT_FILENAME']=$this->document_root.$request->meta['path'];
            $request->server['SCRIPT_NAME']=$request->meta['path'];           
        }


        //注意这个地方,需要先把全局变量读出来,再设置,否则参数不全

        //故意放下面的,方便获取变量
        $request->setGlobal();
//        var_dump($request);

        //处理请求，产生response对象
        //此步骤为处理请求的关键点
        $response = $this->onRequest($request);

        if ($response and $response instanceof Swoole\Response)
        {

            //发送response
            $this->response($request, $response);
        }
    }

    /**
     * 发送完后系统处理.
     */
    function afterResponse(Swoole\Request $request, Swoole\Response $response)
    {
        if (!$this->keepalive or $response->head['Connection'] == 'close')
        {
            $this->server->close($request->fd);
        }
        $request->unsetGlobal();
        //清空request缓存区
        unset($this->requests[$request->fd]);
        unset($request);
        unset($response);
    }

    /**
     * 解析请求,对所有请求结构中的参数进行拆解
     * @param $request Swoole\Request
     * @return null
     */
    function parseRequest($request)
    {
        //解析请求的文件或者方法
        $url_info = parse_url($request->meta['uri']);
        //20171107下面这条语句取消了,获取响应的时候,就设置时间了
	//$request->time = time();
         if (isset($url_info['path'])) {
            $request->meta['path'] = $url_info['path'];
        } else {
            $this->httpError(404, $response, "服务器拒绝了您的访问:" . $request->meta['uri']);
            return true;
        }
        $request->meta['path'] = $url_info['path'];
        if (isset($url_info['fragment'])) $request->meta['fragment'] = $url_info['fragment'];
        if (isset($url_info['query']))
        {
            parse_str($url_info['query'], $request->get);
            //var_dump($request->get);
        }
        //POST请求,有http body
        if ($request->meta['method'] === 'POST')
        {
            $this->parser->parseBody($request);
        }
        //解析Cookies

        if (!empty($request->header['Cookie']))
        {
            $this->parser->parseCookie($request);
        }
		
    }

    /**
     * 发送响应
     * @param $request Swoole\Request
     * @param $response Swoole\Response
     * @return bool
     */
    function response(Swoole\Request $request, Swoole\Response $response)
    {

//        $response->head['Server']='abc';

        if (!isset($response->head['Date']))
        {
            $response->head['Date'] = gmdate("D, d M Y H:i:s T");
        }
        if (!isset($response->head['Connection']))
        {
            //keepalive
            if ($this->keepalive and (isset($request->header['Connection']) and strtolower($request->header['Connection']) == 'keep-alive'))
            {
                $response->head['KeepAlive'] = 'on';
                $response->head['Connection'] = 'keep-alive';
            }
            else
            {
                $response->head['KeepAlive'] = 'off';
                $response->head['Connection'] = 'close';
            }
        }
        //过期命中
        if ($this->expire and $response->http_status == 304)
        {
            //加入必要的头部设置
            if (defined('SERVER_NAME')) {
                $response->setHeader('Server', SERVER_NAME);
            }
            $out = $response->getHeader();
           
        $ret = $this->server->send($request->fd, $out);
        //返回完,清除必要数据,但其实文件并没有销毁
        $this->afterResponse($request, $response);
        return $ret;
        }
        //压缩
        if ($this->gzip)
        {
            if (!empty($request->header['Accept-Encoding']))
            {
                //gzip
                if (strpos($request->header['Accept-Encoding'], 'gzip') !== false)
                {
                    $response->head['Content-Encoding'] = 'gzip';
                    $response->body = gzencode($response->body, $this->config['server']['gzip_level']);
                }
                //deflate
                elseif (strpos($request->header['Accept-Encoding'], 'deflate') !== false)
                {
                    $response->head['Content-Encoding'] = 'deflate';
                    $response->body = gzdeflate($response->body, $this->config['server']['gzip_level']);
                }
                else
                {
                    $this->log("Unsupported compression type : {$request->header['Accept-Encoding']}.");
                }
            }
        }

        //加入必要的头部设置
        if(defined('SERVER_NAME')){
//            $response->addHeaders(array(
//                'Server' => SERVER_NAME
//            ));
            $response->setHeader('Server',SERVER_NAME);
        }


        $out = $response->getHeader().$response->body;
        $ret = $this->server->send($request->fd, $out);
        //返回完,清除必要数据,但其实文件并没有销毁
        $this->afterResponse($request, $response);
        return $ret;
    }

    /**
     * 发生了http错误
     * @param                 $code
     * @param Swoole\Response $response
     * @param string          $content
     */
    function httpError($code, Swoole\Response $response, $content = '')
    {
        $response->setHttpStatus($code);
        $response->head['Content-Type'] = 'text/html';
        $response->body = Swoole\Error::info(Swoole\Response::$HTTP_HEADERS[$code],
            "<p>$content</p><hr><address>" . self::SOFTWARE . " at {$this->server->host}" .
            " Port {$this->server->port}</address>");
    }

    /**
     * 捕获register_shutdown_function错误
     */
    function onErrorShutDown()
    {
        $error = error_get_last();
        if (!isset($error['type'])) return;
        switch ($error['type'])
        {
            case E_ERROR :
            case E_PARSE :
            case E_USER_ERROR:
            case E_CORE_ERROR :
            case E_COMPILE_ERROR :
                break;
            default:
                return;
        }
        $this->errorResponse($error);
    }

    /**
     * 捕获set_error_handle错误
     */
    function onErrorHandle($errno, $errstr, $errfile, $errline)
    {
        $error = array(
            'message' => $errstr,
            'file' => $errfile,
            'line' => $errline,
        );
        $this->errorResponse($error);
    }

    /**
     * 错误显示
     * @param $error
     */
    private function errorResponse($error)
    {
        $errorMsg = "{$error['message']} ({$error['file']}:{$error['line']})";
        $message = Swoole\Error::info(self::SOFTWARE." Application Error", $errorMsg);
        if (empty($this->currentResponse))
        {
            $this->currentResponse = new Swoole\Response();
        }
        $this->currentResponse->setHttpStatus(500);
        $this->currentResponse->body = $message;
        $this->currentResponse->body = (defined('DEBUG') && DEBUG == 'on') ? $message : '';
        $this->response($this->currentRequest, $this->currentResponse);
    }

    /**
     * 处理请求,最重要的处理过程,tp在此处加载
     * @param $request
     * @return Swoole\Response
     */
    function onRequest(Swoole\Request $request)
    {
        //为什么要申请返回对象?
        $response = new Swoole\Response;
        $this->currentResponse = $response;
        \Swoole::$php->request = $request;
        \Swoole::$php->response = $response;

//        var_dump($this->config);

        $Sessions= new Swoole\Sessions($this->config['session']);
        $Sessions->start();
//        $a->getId();
//        $_SESSION['a']='123';
       // var_dump('getId:',$a->getId());

        //请求路径         // 

        //判断是否是默认请求文件
        if ($request->meta['path'][strlen($request->meta['path']) - 1] == '/')
        {
            $request->meta['path'] .= $this->config['request']['default_page'];
            $request->server['SCRIPT_FILENAME']=$this->document_root.$request->meta['path'];
            $request->server['SCRIPT_NAME']=$request->meta['path'];           
        }

        //         if($request->meta['path']=='/'){

        // }

        //过滤请求，阻止禁止访问的目录，处理静态文件
        if ($this->doStaticRequest($request, $response))
        {
             //pass
        }
        /* 动态脚本 */
        elseif (isset($this->dynamic_ext[$request->ext_name]) or empty($ext_name))
        {	
            //真正处理应用载入
            $this->processDynamic($request, $response);
//            var_dump($_SESSION);
            $Sessions->save();
           
            //var_dump($a->load($a->getId()));

//           var_dump(Swoole\Session::getId(),session_id());
//            var_dump(1234,$_SESSION);
//            session_write_close();
//            session_commit();
        }
        else
        {
            $this->httpError(404, $response, "Http Not Found({($request->meta['path']})");
        }

        return $response;
    }

    /**
     * 过滤请求，阻止禁止访问的目录，处理静态文件
     * @param Swoole\Request $request
     * @param Swoole\Response $response
     * @return bool
     */
    function doStaticRequest(Swoole\Request $request, Swoole\Response $response)
    {
        $path = explode('/', trim($request->meta['path'], '/'));
        //扩展名
        $request->ext_name = $ext_name = Swoole\Upload::getFileExt($request->meta['path']);
        /* 检测是否拒绝访问 */
        if (isset($this->deny_dir[$path[0]]))
        {
            $this->httpError(403, $response, "服务器拒绝了您的访问({$request->meta['path']})");
            return true;
        }
        /* 是否静态目录 */
        elseif (isset($this->static_dir[$path[0]]) or isset($this->static_ext[$ext_name]))
        {
            
			return $this->processStatic($request, $response);
        }
        return false;
    }

    /**
     * 处理静态请求
     * @param Swoole\Request $request
     * @param Swoole\Response $response
     * @return bool
     */
    function processStatic(Swoole\Request $request, Swoole\Response $response)
    {
        //这个地方负责实际载入项目
        $path = $this->document_root . '/' . $request->meta['path'];
        if (is_file($path))
        {
            $read_file = true;
            if ($this->expire)
            {
                $expire = intval($this->config['server']['expire_time']);
                $fstat = stat($path);
                //过期控制信息
                if (isset($request->header['If-Modified-Since']))
                {
                    $lastModifiedSince = strtotime($request->header['If-Modified-Since']);
                    if ($lastModifiedSince and $fstat['mtime'] <= $lastModifiedSince)
                    {
                        //不需要读文件了
                        $read_file = false;
                        $response->setHttpStatus(304);
                    }
                }
                else
                {
                    $response->head['Cache-Control'] = "max-age={$expire}";
                    $response->head['Pragma'] = "max-age={$expire}";
                    $response->head['Last-Modified'] = date(self::DATE_FORMAT_HTTP, $fstat['mtime']);
                    $response->head['Expires'] = "max-age={$expire}";
                }
            }
            $ext_name = Swoole\Upload::getFileExt($request->meta['path']);
            if($read_file)
            {
                if(isset($this->mime_types[$ext_name])){
                    $response->head['Content-Type'] = $this->mime_types[$ext_name];
                    $response->body = file_get_contents($path);
                }else{
                    $this->httpError(403, $response, "服务器拒绝了您的访问($ext_name)");
                    return true;
                }

            }
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * 处理动态请求
     * @param Swoole\Request $request
     * @param Swoole\Response $response
     */
    function processDynamic(Swoole\Request $request, Swoole\Response $response)
    {
        //构造指定访问文件路径
        $path = $this->document_root . '/' . $request->meta['path'];
        if (is_file($path))
        {
            //var_dump($response);
			//$response->head['Content-Type'] = 'text/html';
            ob_start();
            try
            {
                //此处为载入入口.项目的.最核心的部分
                include $path;
//                var_dump($_SESSION);exit;
                $response->body = ob_get_contents();
                //有head头数据需要处理
//                $response->setHeader('S-D','2345');
//              $a=  $response->getHeader();
//              var_dump($a);

//              exit;
            }
            catch (\Exception $e)
            {
                $response->setHttpStatus(500);
                $response->body = $e->getMessage() . '!<br /><h1>' . self::SOFTWARE . '</h1>';
            }
            ob_end_clean();

        }
        else
        {
            $this->httpError(404, $response, "页面不存在({$request->meta['path']})！");
        }
    }
}
