<?php

namespace Swoole;

/**
 * Swoole库加载器
 * @author Tianfeng.Han
 * @package SwooleSystem
 * @subpackage base
 * 系统初始第一步，就是加载文件。加载文件需要定义好路径
 * 主要作用是定位加载的文件路径,必须第一步载入
 *
 */
class Loader {

    /**
     * 命名空间的路径
     */
    protected static $namespaces;
    static $swoole;
    static $_objects;

    function __construct($swoole) {
        self::$swoole = $swoole;
        self::$_objects = array(
            'model' => new \ArrayObject,
            'object' => new \ArrayObject
        );
    }

    /**
     * for composer,载入扩展库
     */
    static function vendorInit() {
        require __DIR__ . '/../lib_config.php';
    }

    /**
     * 加载一个模型对象
     * @param $model_name string 模型名称
     * @return $model_object 模型对象
     */
    static function loadModel($model_name) {
        if (isset(self::$_objects['model'][$model_name])) {
            return self::$_objects['model'][$model_name];
        } else {
//            // 内置的mvc.因为mvc走了tp.这部分应该放弃不用
//            $model_file = \Swoole::$app_path . '/models/' . $model_name . '.model.php';
//            if (!file_exists($model_file)) {
//                Error::info('MVC错误', "不存在的模型, <b>$model_name</b>");
//            }
//            require($model_file);
//            self::$_objects['model'][$model_name] = new $model_name(self::$swoole);
//            return self::$_objects['model'][$model_name];
        }
    }

    /**
     * 自动载入类,自动构造指定路径的PHP文件,这个地方以指定顶级命名空间的root为根.进行文件相对定位
     * @param $class
     */
    static function autoload($class) {
        $root = explode('\\', trim($class, '\\'), 2);
        if (count($root) > 1 and isset(self::$namespaces[$root[0]])) {
            include_once self::$namespaces[$root[0]] . '/' . str_replace('\\', '/', $root[1]) . '.php';
        }
    }

    /**
     * 设置根命名空间,其实就是注册指定类的寻找路径
     * @param $root
     * @param $path
     */
    static function addNameSpace($root, $path) {
        self::$namespaces[$root] = $path;
    }

}
