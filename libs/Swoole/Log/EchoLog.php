<?php
namespace Swoole\Log;
use Swoole;

/**
 * 日志处理,这个地方以后需要和APP内的日志服务整合,原因是这个地方的日志记录的太简单.
 *
 */
class EchoLog extends Swoole\Log implements Swoole\IFace\Log
{
    protected $display = true;

    function __construct($config)
    {
        if (isset($config['display']) and $config['display'] == false)
        {
            $this->display = false;
        }
        parent::__construct($config);
    }

    function put($msg, $level = self::INFO)
    {
        if ($this->display)
        {
            $log = $this->format($msg, $level);
            if ($log) echo $log;
        }
    }
}