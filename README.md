# SF-ThinkCMF_SWOOLE 
----------


**ThinKCMFv5.0.180901 for swoole** 
示例,探讨一种平滑过渡thinkphp5.0X开发的项目到swoole下运行的思路.
- 支撑框架经过调整,暂时未验证swoole下是否能正常运行,直接从现行项目里剥离出来的.如有问题请与我联系QQ群:302604682
- 主要解决的问题
1. 多端 PC+APP+M站+微网站+小程序 代码复用问题,避免功能实现重复冗余,在架构层面遵循功能单一原则.避免开发多次迭代后代码随意性问题和开发规范遵循问题.
2. for swoole兼容,提高了多协议通信兼容和性能的提升.
3. 遵循架构的水平和垂直伸缩原则,支持系统分布集群热部署.
4. 多项目开发时,共用底层框架和服务问题

### 架构示意
![](https://gitee.com/kinghack001/SF_ThinkCMF_SWOOLE/raw/master/%E4%BA%A7%E5%93%81%E6%9E%B6%E6%9E%84v0.02.png)


### 登陆信息
1. 后台
- 用户名:admin
- 密码:123456

2. 用户中心
- 用户名:test
- 密码:123456

## 内容相关信息
1. 数据库文件 thinkcmf5.sql 自己导.
2. 启动方式:gateway下
- php admin.php start
- php user.php start
- php www.php start


### 开发环境
> php 7.1.15

> swoole 2.1.1

> mysql 5.6+

## 其他
我做的工作,只是调整了已有部分内容加入了swoole支持.同时扩展了跨应用调用方式,可以根据需求,使用不同方式,调用类.new class,rpc,RESTFul api.可以让tp项目在不修改已有代码的情况下,在swoole下运行.其它见提交日志

## nginx 的配置，直接上的 服务器 配置 ：
```shell
server {
  listen 6666;
  server_name _;
  access_log /data/wwwlogs/www.test.com_nginx.log combined;
  index index.html index.htm;
  root /home/test/www.test.com/test/public;



  location ~* ^/admin/.*$ {
        proxy_http_version 1.1;
        proxy_set_header Connection "keep-alive";
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_cache off;
		proxy_pass http://127.0.0.1:8888;
    }

  location ~* ^/user/.*$ {
        proxy_http_version 1.1;
        proxy_set_header Connection "keep-alive";
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://127.0.0.1:8891;
    }

  location  ~ [^/]\.php(/|$) {
        proxy_http_version 1.1;
        proxy_set_header Connection "keep-alive";
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://127.0.0.1:8890;
    }


  location ~ .*\.(gif|jpg|jpeg|png|bmp|swf|flv|mp4|ico)$ {
    expires 30d;
    access_log off;
  }
  location ~ .*\.(js|css)?$ {
    expires 7d;
    access_log off;
  }
  location ~ /\.ht {
    deny all;
  }
}
```
