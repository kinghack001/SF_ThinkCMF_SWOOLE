HttpServer的使用方法
----
http服务器跟fpm和apache很像，只是包含documentRoot中的php文件，没有带有任何额外功能。
主要为TP类的web框架,提供一个类原生的WebServer容器.为这类框架应用提供一种无缝过渡的选择.
效率在相同环境下,httpserver是普通AP类效率的2倍.
使用转发模式.貌似还有加速的作用,效率会进一步提升.大致30%左右.
```shell
php cmf_server.php start
```


