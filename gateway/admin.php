<?php
define('DEBUG', 'on');
define("WEBPATH", realpath(__DIR__ . '/../'));
define('IS_SWOOLE', true);
define('SERVER_NAME', 'kinghack/0.1');
date_default_timezone_set('Asia/Shanghai'); 

//不释放,一直是一个需要更新
session_start();

// 载入链接库,完成顶级命名空间注册等工作
require dirname(__DIR__) . '/libs/lib_config.php';

// 设置出错后的提示方式
Swoole\Config::$debug = true;

//设置PID文件的存储路径
Swoole\Network\Server::setPidFile(__DIR__ . '/admin.pid');
/**
 * 显示Usage界面
 * php admin.php start|stop|reload
 * 这么写的目的是为了.有个完整的命令行显示,同时输出初始过程结果,前面都是壳操作
 */
Swoole\Network\Server::start(function ()
{
    // 调用httpserver对象,启用
    $AppSvr = new Swoole\Protocol\HttpServer();
    $AppSvr->loadSetting(__DIR__.'/admin.ini'); //加载配置文件
    // 指定APP目录的初始位置,放置其它PHP框架
	$AppSvr->setDocumentRoot(realpath(__DIR__ . '/../').'/test/public');
	// 设置log
    $AppSvr->setLogger(new Swoole\Log\EchoLog(true)); //Logger

    // 设置出错显示方式
    Swoole\Error::$echo_html = false;

    // 设置监听的IP和端口
    // Swoole\Network\Server::$useSwooleHttpServer = true;
    $server = Swoole\Network\Server::autoCreate('0.0.0.0', 8888);
    // $server::$useSwooleHttpServer=true;
    // 设置监听的协议
    $server->setProtocol($AppSvr);
    //$server->daemonize(); //作为守护进程
    $server->run(array('worker_num' => 0, 'max_request' => 5000, 'log_file' => '/tmp/admin.log'));
});
